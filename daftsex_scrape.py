#!/usr/bin/python3


import urllib3
import os
import json
import certifi
import re           # for regular expression
from pudb import set_trace

http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())

# Light Green Car 240/360/480/1080/720/
id = r'https?://(?:www\.)?daftsex\.com/watch/(?P<id>-?\d+_\d+)'

# GET VIDEO ID (-2000072782_95072782)
# 2012 X-Games 18 motocross best trick 720/480/360/240 28:50min
url = 'https://daftsex.com/watch/-53425992_165397466'
id = url[26:46]
print("ID: ",id)

request = http.request('GET',url)

# FIRST REQUEST - GET PAGE SOURCE AND GET PLAYER HASH
# GET TITLE  AND PLAYER HASH
# GET CONTENT-TYPE HEADERS
content = request.headers["Content-Type"]

# GET WEBPAGE CONTENT
webpage = request.data.decode('utf-8')

# SAVE TO FILE
file = "scraping_DS1.md"
with open(file ,'a') as file_obj:
    file_obj.write(webpage)

# GET TITLE
# title = webpage.find("title")
title = re.findall(r'<title>(.*?)</title>',str(webpage))
print("Title: ", title[0],"\n")

duration = re.findall(r'Duration: ((?:[0-9]{2}:)[0-9]{2})',str(webpage))
print("Duration: " , duration[0],"\n")

player_hash = re.findall('DaxabPlayer\\.Init\\({[\\s\\S]*hash:\\s*"([0-9a-zA-Z_\\-]+)"[\\s\\S]*}',str(webpage))
#        'DaxabPlayer\\.Init\\({[\\s\\S]*hash:\\s*"([0-9a-zA-Z_\\-]+)"[\\s\\S]*}')
print("Hash:", player_hash[0])

player_color = re.findall('DaxabPlayer\\.Init\\({[\\s\\S]*color:\\s*"([0-9a-z]+)"[\\s\\S]*}',str(webpage))
print("Color:" ,player_color[0])

## WEB URL2 https://daxab.com/player/x5pXeQrsRHeyDj-3whOndJ71YGADVX5_rEHeBGYDYR_Lh85zQRlTxH8aXRifIAZuDspQ77tWQyIchjffqesbqHswXHxa2ingFBdHZjHqtmY?color=f12b24

url2 = 'https://daxab.com/player/' + player_hash[0] +'?color=' + player_color[0]
print("URL2: ", url2)

request2 = http.request('POST',url2)
content = request2.headers["Content-Type"]
webpage2 = request2.data.decode('utf-8')
file2 = "scraping_DS2.md"

print(request2.data.decode('utf-8'))

if(os.path.exists(file2)):
    os.remove("scraping_DS2.md")

with open(file2,'a') as file2_obj:
    file2_obj.write(webpage2)
