<!DOCTYPE html>
<html>
<head data-extra="aIX2DL3JIGnO7i8k6HvdT4">
<meta charset="UTF-8">
<title>2012 X-Games 18 motocross best trick — DaftSex</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="/index.js?6" defer></script>
<link rel="manifest" href="/manifest.webmanifest">
<meta name="robots" content="all" />
<meta name="robots" content="noarchive" />
<meta name="description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality">
<meta name="keywords" content="Watch, video, daftsex, motocross, best, trick, daftsex">
<meta property="og:url" content="https://daftsex.com/watch/-53425992_165397466" />
<meta property="og:type" content="video.movie" />
<meta property="og:title" content="2012 X-Games 18 motocross best trick" />
<meta property="og:description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality" />
<meta property="og:image" content="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg" />
<meta property="og:video:type" content="text/html" />
<link href="/css/robotocondensed.v4b6944ca5bbf3c8b.css" rel="stylesheet" type="text/css">
<link rel="icon" type="image/png" sizes="196x196" href="/img/daftlogo196x196.png?2">
<link rel="shortcut icon" href="/img/favicon.ico?5" />
<meta name="theme-color" content="#C00">
<link rel="stylesheet" href="/css/common.v112268244c82433e.css" />
<link rel="stylesheet" href="/css/icons.v38e1854b6d41caec.css" />
<link rel="stylesheet" href="/css/dark.ve32b1eba318e418e.css" />
<script type="text/javascript">
window.globEmbedUrl = 'https://daxab.com/player/';
window.timeNow = 1658939168;
setInterval(function() {
  window.timeNow++;
}, 1000);
window.liteopen = false;
window.is_logged = false;
</script>
<script type="text/javascript" src="/js/jquery-2.1.1.min.v18b7e87c91d98481.js"></script>
<script type="text/javascript" src="/js/history.v955089448af5a0c8.js"></script>
<script type="text/javascript" src="/js/nprogress.v3410974b8841b4f3.js"></script>
<script type="text/javascript" src="/js/nouislider.v2192f61dc764023a.js"></script>
<script type="text/javascript" src="/js/select.ve363dc0076d2c78b.js"></script>
<script type="text/javascript" src="/js/common.vb1f31c4b59a9e4d1.js"></script>
<script type="text/javascript" src="/js/auth.vfa3c32a15fba2304.js"></script>
<script type="text/javascript" src="/js/jquery.mutations.min.v4b147b138a5b1019.js"></script>
<script type="text/javascript" src="/js/fav.v764365b62392eb58.js"></script>
<script type="text/javascript" src="/js/share.vdf8ddf291dc2f417.js"></script>
<script type="text/javascript" src="/js/likes.v5e342c5feda70804.js"></script>
<script type="text/javascript">
window._stv = '67c5c441c9e2';
window.log_version = 'fbcb78248ac55029';
</script>
</head>
<body>
<a class="up" onclick="toTop();">&uarr;</a>
<div class="page_wrapper">
<div class="header">
<div class="width">
<div class="header_inner">
<a onclick="window.history.go(-1); return false;" title="Back" class="back-logo"><i class="icon-left"></i></a>
<a href="/" title="DaftSex" class="logo">
<svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="55.000000pt" height="50.000000pt" viewBox="0 0 311.000000 127.000000" preserveAspectRatio="xMidYMid meet">
<g transform="translate(0.000000,180.000000) scale(0.100000,-0.100000)" fill="#dd3333" stroke="none">
<path d="M895 1713 c-142 -78 -362 -148 -559 -178 -54 -9 -102 -18 -107 -21
-14 -8 -10 -228 5 -354 36 -284 110 -501 235 -692 110 -167 280 -320 442 -398
l46 -23 54 27 c149 75 311 216 408 356 59 84 131 221 168 320 35 92 79 287 93
410 12 101 18 346 8 355 -2 2 -49 11 -104 19 -215 33 -424 99 -578 184 l-51
29 -60 -34z m15 -818 l0 -636 -45 31 c-128 88 -246 229 -327 390 -86 171 -148
427 -148 612 0 72 2 78 23 83 209 46 339 85 447 134 25 11 46 21 48 21 1 0 2
-286 2 -635z m197 592 c99 -40 258 -87 343 -102 79 -14 74 -4 67 -133 -4 -63
-11 -120 -16 -125 -5 -5 -38 -6 -83 -1 l-73 9 3 46 3 45 -63 13 c-35 6 -75 14
-90 18 l-28 5 0 -95 c0 -81 3 -96 18 -102 9 -3 81 -9 160 -12 78 -3 142 -10
142 -15 0 -14 -39 -155 -61 -221 -35 -101 -118 -253 -188 -340 -65 -79 -226
-221 -242 -212 -8 5 -12 501 -5 514 3 4 43 2 90 -3 l85 -9 3 -41 3 -41 37 65
c59 105 64 96 -61 104 -61 3 -122 11 -136 16 l-25 10 0 325 c0 179 2 325 4
325 2 0 53 -20 113 -43z" />
<path d="M628 1242 c-79 -19 -78 -15 -53 -153 18 -102 51 -201 98 -294 69
-137 66 -145 67 178 l0 287 -22 -1 c-13 -1 -53 -9 -90 -17z" />
</g>
</svg>
</a>
<div class="clear-form"><i class="icon-cancel"></i></div>
<form class="search-form" onsubmit="return search(true);">
<input type="text" id="q" autocomplete="off" value="" placeholder="Search millions of videos...">
<a title="RandomTV" href="/video/Cheating Wife" class="randomQuery"><span class="deskrandom"></span></a>
<button type="submit" title="Search button">Search</button>
<div class="head-menu"></div>
</form>
<div class="head-menu module-browse"><a href="/browse">Browse</a></div>
<div class="head-menu module-hottest"><a href="/hottest">Hottest</a></div>
<div class="head-menu module-categories"><a href="/categories">Category</a></div>
<div class="head-menu module-pornstars"><a href="/pornstars">Pornstars</a></div>
<div class="head-menu"><a href="https://theporndude.com/" target="_blank" rel="nofollow" style="color: #000;">Best Porn Sites</a></div>
<button class="btn-search" type="button"><i class="icon-left"></i></button>
<span class="hide-item">

<a href="/pornstars"><span class="btn-pornstars"></span></a>
<a href="/categories"><span class="btn-catt"></span></a>
<a href="/hottest"><span class="btn-hottest"></span></a>
<a href="/browse"><span class="btn-browse"></span></a>
</span>
<span class="auth login hide-item" title="Login" onclick="Auth.Login();"></span>
</div>
</div>
<div id="progress_content"></div>
<div class="suggest_search"><div class="suggest_inner width"><ul class="suggest_items"></ul></div></div>
</div>
<div class="bg_width">
<div class="width">

<div class="aboveVideo standalonead" style="text-align: center;padding-top: 75px;margin-bottom: -70px;">

<span style="
    text-align: center;
"> <a style="
    text-align: left;
    color: #fff;
    font-size: 15px;
    text-decoration: none;
    padding: 5px;
    top: 61px;
    margin: 5px;
    border-radius: 25px;
    display: block;
    background-color: #5e62ff;
    " href="https://artsporn.com" target="_blank">
ℹ: ARTSPORN.COM - Another new Alternative, faster website without ads. 👊⚡️⚡️⚡️</a></span>


<iframe width="300" height="150" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10009021?time=1575323689465" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10012984"></iframe>




</div>
<div class="aboveVideoPC standalonead" style="text-align: center;padding-top: 75px;margin-bottom: -70px;">








</div>
<div class="content-wrapper" id="content">
<div itemscope itemtype="http://schema.org/VideoObject">
<link itemprop="thumbnailUrl" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
<span itemprop="thumbnail" itemscope itemtype="http://schema.org/ImageObject">
<link itemprop="url" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
<link itemprop="contentUrl" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
</span>
<meta itemprop="name" content="2012 X-Games 18 motocross best trick" />
<meta itemprop="uploadDate" content="2013-07-17T16:03:39+04:00">
<meta itemprop="description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality" />
<meta itemprop="duration" content="T28M50S" />
</div>

<div class="video">
<a class="standalonead" style="font-size: 14px; font-weight: bold;color:#0a0a0a; text-align: center;" href="https://landing.brazzersnetwork.com/?ats=eyJhIjoyOTIyOTcsImMiOjU3NzAzNTQ4LCJuIjoxNCwicyI6OTAsImUiOjg4MDMsInAiOjU3fQ==" target="_blank" rel="nofollow"><div class="message-container" style="background-color:#ffd001;padding: 7px;font-size: 17px;font-weight: 700;"><span class="nomobile">EXCLUSIVE DaftSex OFFER - Join BRAZZERS Only 1$ Today ! [PROMO]</span> <span class="nomobile"><span style="color:rgba(255, 208, 1)"></span><span style="color:rgba(255, 208, 1)"><span></span></span></span></div></a>
<div style="background:#000000;outline: 0px solid #fff;" class="frame_wrapper">
<script id="data-embed-video-QpU0YmgKK2OE1kNnS52_zSvF5qacVu_omXWIC7k0bWkcHeNBDLOct6zi8Twep6rOtkyURjpVWpuvR3lYdLGGTXlXzAp3Zdck0LF9CrclwFg">
if (window.DaxabPlayer && window.DaxabPlayer.Init) {
  DaxabPlayer.Init({
    id: 'video-53425992_165397466',
    origin: window.globEmbedUrl.replace('/player/', ''),
    embedUrl: window.globEmbedUrl,
    hash: "QpU0YmgKK2OE1kNnS52_zSvF5qacVu_omXWIC7k0bWkcHeNBDLOct6zi8Twep6rOtkyURjpVWpuvR3lYdLGGTXlXzAp3Zdck0LF9CrclwFg",
    color: "f12b24",
    styles: {
      border: 0,
      overflow: 'hidden',
      marginBottom: '-5px'
    },
    attrs: {
      frameborder: 0,
      allowFullScreen: ''
    },
    events: function(data) {
      if (data.event == 'player_show') {
        dt();

        HistoryWatch.set("-53425992_165397466", "WyIyMDEyIFgtR2FtZXMgMTggbW90b2Nyb3NzIGJlc3QgdHJpY2siLDE3MzAsNjIsImh0dHBzOlwvXC9zdW45LTQ2LnVzZXJhcGkuY29tXC9jNTM1MzA0XC91NjI1MjQ2NVwvdmlkZW9cL2xfZGQ5MzBlOGYuanBnIiwiODc4OGE3MDgzNDAzZWNkOTg0ZWNiOGExNThhMmJjN2IiXQ==");
      }
    }
  });
}
</script>
</div>
<div class="video_info_wrapper">
<h1 class="heading">2012 X-Games 18 motocross best trick</h1>
<div class="voting-module video-watch">
<span class="icon-container">
<span class="icon icon1"></span>
<span class="icon icon2 hidden-vote"></span>
</span>
<span class="text-container">
<span class="text text1"></span>
<span class="text text2"></span>
</span>
</div>
<script>
var videoLikes = 0;

_likes(videoLikes, "-53425992_165397466", "f277e7e3");

logVideoId("-53425992_165397466", "f277e7e3");
</script>
<div class="video_fav" alt="To favorites" title="To favorites" onclick="Fav.Toggle(this, '-53425992_165397466', '6adab556e398e4602ff6ca8224d02364');"></div>
<div class="video_embed" title="Share" onclick="Embed.show(-53425992, 165397466);"></div>
<div class="main_share_panel">
<div id="addthis_sharing_toolbox">
<div class="share_btn share_btn_re"><span class="re_icon" onclick="return Share.reddit();"></span></div>
<div class="share_btn share_btn_fb"><span class="fb_icon" onclick="return Share.facebook();"></span></div>
<div class="share_btn share_btn_tw"><span class="tw_icon" onclick="return Share.twitter();"></span></div>
<div class="share_btn share_btn_vk"><span class="vk_icon" onclick="return Share.vkontakte();"></span></div>
</div>
</div>
<div class="clear"></div>
<div class="bordertop">
<br>Views: 62 <br>Published: 17 Jul at 04:03 PM </div></div>
<div>Duration: 28:50</div>
</div>
<div class="tabs_wrapper">
<div class="tabs">
<style>.tabs > div.tab-btn{width: 50%;}</style>
<div class="tab-btn tab-show" id="tab_similar">Related</div>
<div class="tab-btn" id="tab_comments">Comments</div>
</div>
</div>
<div class="rightbar tab_similar tab-result tab-show">
<span style="margin: 0px 13px;padding-bottom: 15px;" class="standalonead">
<iframe width="300" height="250" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10002729?time=1555363895445" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10002729"></iframe>
</span>
<div class="video-item video5817_163924401" data-video="WyJXaW50ZXIgWCBHYW1lcyAyMDEyIFtBc3Blbl0gLUZpbmFsIE1lbnMgU3VwZXJQaXBlIiw1MjQ0LDQzLCJodHRwczpcL1wvc3VuOS03My51c2VyYXBpLmNvbVwvYzUxOTYxMVwvdTA1ODE3XC92aWRlb1wvbF83YzRiNjI4Zi5qcGciLCJiNDI1NzY4NmJlYjIxYmE5MjU3NmM5M2NkY2E0YzQxYSJd" data-embedUrl="https://daxab.com/player/A6bLWOQ8M7fVXg4t7-9VMWAnxbccrDv69n9X3ASSaQ-xzDbTKsTIU-2VijvVPiczsfAWFo-G8ArJyDtLzjTBkg" data-playerHash="A6bLWOQ8M7fVXg4t7-9VMWAnxbccrDv69n9X3ASSaQ-xzDbTKsTIU-2VijvVPiczsfAWFo-G8ArJyDtLzjTBkg" data-favHash="24659d3a3191ece1b857645e1fc8a685">
<a href="/watch/5817_163924401" data-id="5817_163924401">
<div class="video-show"></div>
<div class="video-fav" data-hash="24659d3a3191ece1b857645e1fc8a685"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-73.userapi.com/c519611/u05817/video/l_7c4b628f.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">43</span>
<span class="video-time">01:27:24</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Winter X Games 2012 [Aspen] -Final Mens SuperPipe</div>
</a>
</div>
<div class="video-item video-4296410_167364583" data-video="WyJYLWdhbWVzIDIwMTIgYm14IHBhcmsgZmluYWwgKNC90LAg0YDRg9GB0YHQutC+0LwpIiw1MjkzLDM1LCJodHRwczpcL1wvc3VuOS01NS51c2VyYXBpLmNvbVwvYzUzNTUyMlwvdTk3MDA0ODdcL3ZpZGVvXC9sXzgwYzVjOThmLmpwZyIsIjc5YWY1NjFmYzFiYjQ5NmFhZjM5MzEzNzBkNjBkZTQ4Il0=" data-embedUrl="https://daxab.com/player/wPwvFrGNGX-jXGh-3g5iCgwa42SXP8IXeEZmQKI0HTgmejjr0nZ7jt7sKRru_xXDgeE1nzR8AQ2CD1Gc4YIoIw" data-playerHash="wPwvFrGNGX-jXGh-3g5iCgwa42SXP8IXeEZmQKI0HTgmejjr0nZ7jt7sKRru_xXDgeE1nzR8AQ2CD1Gc4YIoIw" data-favHash="24c1b3a842c081128ac6b11cf664db03">
<a href="/watch/-4296410_167364583" data-id="-4296410_167364583">
<div class="video-show"></div>
<div class="video-fav" data-hash="24c1b3a842c081128ac6b11cf664db03"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-55.userapi.com/c535522/u9700487/video/l_80c5c98f.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">8 years ago</span>
<span class="video-view">35</span>
<span class="video-time">01:28:13</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012 bmx park final (на русском)</div>
</a>
</div>
<div class="video-item video-20774214_163747625" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxKSBIRCIsNjY5MiwxNzUsImh0dHBzOlwvXC9zdW45LTgwLnVzZXJhcGkuY29tXC9jNTE0MjE4XC91MTE0MDIzMTJcL3ZpZGVvXC9sXzE3NWVhYzgxLmpwZyIsIjFmNTlmYmM5NTc0MjY4N2JhNmEwOGQ5ZWY3ZjUzMmZlIl0=" data-embedUrl="https://daxab.com/player/PCTVcGGI4JUDlEbw4SRhwNUKY3Bl-4QXCRMB58rgXsTzoceWxuk4V8mDMYO_jlnXWcyFds5vfrNoO6fRdCL5Qw" data-playerHash="PCTVcGGI4JUDlEbw4SRhwNUKY3Bl-4QXCRMB58rgXsTzoceWxuk4V8mDMYO_jlnXWcyFds5vfrNoO6fRdCL5Qw" data-favHash="50bb3585ce2c07e4f89225006b652011">
<a href="/watch/-20774214_163747625" data-id="-20774214_163747625">
<div class="video-show"></div>
<div class="video-fav" data-hash="50bb3585ce2c07e4f89225006b652011"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c514218/u11402312/video/l_175eac81.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">175</span>
<span class="video-time">01:51:32</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1) HD</div>
</a>
</div>
<div class="video-item video-127946280_456239036" data-video="WyJFcm9nZSEgSCBtbyBHYW1lIG1vIEthaWhhdHN1IFphbm1haSDQrdGA0L7Qs9C1ISAwMlwv0JfQsNCx0YvRgtGM0YHRjyDQsiDRgdC+0LfQtNCw0L3QuNC4INGF0LXQvdGC0LDQuSDQuNCz0YDRiyAoKzE4IEhlbnRhaSBIRCkiLDE4NDYsNDA0NSwiaHR0cHM6XC9cL3N1bjYtMjIudXNlcmFwaS5jb21cL2M2MjY2MTlcL3Y2MjY2MTkyODBcLzI2MTExXC9TSmhKdExNNDBYNC5qcGciLCJhNmI1NmM0M2NkNmQxNTJiMzljYWExMzNhZmQzNGUwZSJd" data-embedUrl="https://daxab.com/player/h5pnDJ4cjnFjnQSLe5hjCMTQJgJ4JAiNF4WEHEGUUglJsR2XElLiWeZ7ZjdDGoPazoM7HjWuBqIk039S7BH75ySbxwcdnh63dQiruQeMm-w" data-playerHash="h5pnDJ4cjnFjnQSLe5hjCMTQJgJ4JAiNF4WEHEGUUglJsR2XElLiWeZ7ZjdDGoPazoM7HjWuBqIk039S7BH75ySbxwcdnh63dQiruQeMm-w" data-favHash="3a7c012b7e3a9ade5274c30b1f34afa5">
<a href="/watch/-127946280_456239036" data-id="-127946280_456239036">
<div class="video-show"></div>
<div class="video-fav" data-hash="3a7c012b7e3a9ade5274c30b1f34afa5"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c626619/v626619280/26111/SJhJtLM40X4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">5 years ago</span>
<span class="video-view">4k</span>
<span class="video-time">30:46</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Eroge! H mo Game mo Kaihatsu Zanmai Эроге! 02/Забыться в создании хентай игры (+18 Hentai HD)</div>
</a>
</div>
<div class="video-item video-20774214_163586566" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxKSIsNjc4NCwyMDQsImh0dHBzOlwvXC9zdW45LTg2LnVzZXJhcGkuY29tXC9jNTMyMzE1XC91OTcyODAxMVwvdmlkZW9cL2xfNTYzNzMyMmMuanBnIiwiYWQxZjRiYzE1ODQ0M2VlODE2MDVlZjcyNTE0YzczMDIiXQ==" data-embedUrl="https://daxab.com/player/5OPx1pwPffqkjuzRSlQBxCCN0tWFnIttwfvRKCVAHeLZfWnTsxeXyuu3vT0GP77buZhFByUaGpY7wKdbPkiRVg" data-playerHash="5OPx1pwPffqkjuzRSlQBxCCN0tWFnIttwfvRKCVAHeLZfWnTsxeXyuu3vT0GP77buZhFByUaGpY7wKdbPkiRVg" data-favHash="255bdad6a740955e40ef7e3cc4be92d1">
<a href="/watch/-20774214_163586566" data-id="-20774214_163586566">
<div class="video-show"></div>
<div class="video-fav" data-hash="255bdad6a740955e40ef7e3cc4be92d1"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-86.userapi.com/c532315/u9728011/video/l_5637322c.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">204</span>
<span class="video-time">01:53:04</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1)</div>
</a>
</div>
<div class="video-item video-147765253_456239519" data-video="WyJHeW1uYXN0aWNzIC0gQXJ0aXN0aWMgLSBNZW4mIzM5O3MgVGVhbSBGaW5hbCB8IExvbmRvbiAyMDEyIE9seW1waWMgR2FtZXMiLDExNjQwLDEsImh0dHBzOlwvXC9zdW42LTIwLnVzZXJhcGkuY29tXC93Y2t5eHgwLTdCQjlFa05BRDJYbVpfeDN1ZjZVemUtcnVxVl94d1wvYXZpSDc4Vk1OZEkuanBnIiwiN2FmNjY0MDdiNDQwMGM3NTc2NWRmYTYzZGJmZWEwMTQiXQ==" data-embedUrl="https://daxab.com/player/P_iN3tdVmuh9v0RHGRZnDGmlKhpxo64C3GJ2yHxDe_bM5hRyaF1YJqDNAKFkbT1-yb8DXdhG8gVPb-Hx8ADOCJabJABDuqAWSrDA8nn8HYQ" data-playerHash="P_iN3tdVmuh9v0RHGRZnDGmlKhpxo64C3GJ2yHxDe_bM5hRyaF1YJqDNAKFkbT1-yb8DXdhG8gVPb-Hx8ADOCJabJABDuqAWSrDA8nn8HYQ" data-favHash="21df7febeb200b63d0e7d35d0cdd5e05">
<a href="/watch/-147765253_456239519" data-id="-147765253_456239519">
<div class="video-show"></div>
<div class="video-fav" data-hash="21df7febeb200b63d0e7d35d0cdd5e05"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/wckyxx0-7BB9EkNAD2XmZ_x3uf6Uze-ruqV_xw/aviH78VMNdI.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 months ago</span>
<span class="video-view">1</span>
<span class="video-time">03:14:00</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Gymnastics - Artistic - Men&amp;#39;s Team Final | London 2012 Olympic Games</div>
</a>
</div>
<div class="video-item video-188576299_456239468" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAxIFs3MjBwXSIsMTg2OSwxOTEyLCJodHRwczpcL1wvc3VuNi0yMC51c2VyYXBpLmNvbVwvYzg1NzQyMFwvdjg1NzQyMDM5NVwvMjJhZTBmXC9xV0dMVjlycDgyRS5qcGciLCI2NzM2ZGM1NzgzOWRhYTFjMGI3M2YwN2UxZDhhM2ZkNSJd" data-embedUrl="https://daxab.com/player/MVSwezKChci1-Q9WoeMCMoHNT824hGMX0gCofCBhRtmOZ5pcH3zYNdHWbveTyfqlO2tkxBtNjkEa4yJXniNn4ZaOyYOCZezMwxH8GOsBnsQ" data-playerHash="MVSwezKChci1-Q9WoeMCMoHNT824hGMX0gCofCBhRtmOZ5pcH3zYNdHWbveTyfqlO2tkxBtNjkEa4yJXniNn4ZaOyYOCZezMwxH8GOsBnsQ" data-favHash="cc2ea25fe6368387cd0d23e5d82299d1">
<a href="/watch/-188576299_456239468" data-id="-188576299_456239468">
<div class="video-show"></div>
<div class="video-fav" data-hash="cc2ea25fe6368387cd0d23e5d82299d1"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/c857420/v857420395/22ae0f/qWGLV9rp82E.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,9k</span>
<span class="video-time">31:09</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 01 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239466" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAzIFs3MjBwXSIsMTgwNiwxNzMwLCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NTcyOFwvdjg1NTcyODM5NVwvMjRmMjY0XC9lcHl0TURrMFJkSS5qcGciLCI3Y2RiMDhjOTFhNmMwOWZiNjQ3YzBkNDhmZTllMzZlMyJd" data-embedUrl="https://daxab.com/player/wbk51KStcWv8onqwxNXYm5TaYEJTPtExRoY0cuUnOEOUbzzclYNKKOMnrJgC6VP4jQ_9viKwpWsJO2JgFy3Jp1UeC758Y-dZNaj-MQL2K9A" data-playerHash="wbk51KStcWv8onqwxNXYm5TaYEJTPtExRoY0cuUnOEOUbzzclYNKKOMnrJgC6VP4jQ_9viKwpWsJO2JgFy3Jp1UeC758Y-dZNaj-MQL2K9A" data-favHash="ac89a8b41ee3f0f4315343b2c6f6a9fa">
<a href="/watch/-188576299_456239466" data-id="-188576299_456239466">
<div class="video-show"></div>
<div class="video-fav" data-hash="ac89a8b41ee3f0f4315343b2c6f6a9fa"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c855728/v855728395/24f264/epytMDk0RdI.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,7k</span>
<span class="video-time">30:06</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 03 [720p]</div>
</a>
</div>
<div class="video-item video-20774214_163763516" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTQgKExpdmUgU2hvdyAyKSBIRCIsNjAzNCwxMzYsImh0dHBzOlwvXC9zdW45LTEzLnVzZXJhcGkuY29tXC9jNTI3NjEwXC91MTE0MDIzMTJcL3ZpZGVvXC9sXzg1MzBmODBiLmpwZyIsIjNjNjFjYzYyMDY2YmI5ZWJhM2RhNjZkNTU3Y2M5MWMyIl0=" data-embedUrl="https://daxab.com/player/AvLZpu7aBkm0QSdstI7n1nWc0KTnCXQdpaqCMTM_nSSXXziN9BIbiLH38DNnwaJjxqK_4hw9GMFKd3RvyBR_Vg" data-playerHash="AvLZpu7aBkm0QSdstI7n1nWc0KTnCXQdpaqCMTM_nSSXXziN9BIbiLH38DNnwaJjxqK_4hw9GMFKd3RvyBR_Vg" data-favHash="0938a89746b3a3d4fa400d6265b36a06">
<a href="/watch/-20774214_163763516" data-id="-20774214_163763516">
<div class="video-show"></div>
<div class="video-fav" data-hash="0938a89746b3a3d4fa400d6265b36a06"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-13.userapi.com/c527610/u11402312/video/l_8530f80b.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">136</span>
<span class="video-time">01:40:34</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x14 (Live Show 2) HD</div>
</a>
</div>
<div class="video-item video-901607_162943067" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIE1hbmBzIE1vdG8gWCBFbmR1cm8gWCIsNDc5OSw4MCwiaHR0cHM6XC9cL3N1bjktNDkudXNlcmFwaS5jb21cL2M1Mjc1MDdcL3UzMDY5OTMxN1wvdmlkZW9cL2xfYTZmNDJkMDkuanBnIiwiYmNmN2I5NjA1ZmNmZDI3OTFhNzUzY2FkYjNjZDViYTAiXQ==" data-embedUrl="https://daxab.com/player/h5Rwocrz4fKqcBG5O-9T-d-h9zw9aob0mU6pOQ59pepITiC9NF-i2u6X6a9JC_oU4ax3YGy_tr1ow4E1JmhUiw" data-playerHash="h5Rwocrz4fKqcBG5O-9T-d-h9zw9aob0mU6pOQ59pepITiC9NF-i2u6X6a9JC_oU4ax3YGy_tr1ow4E1JmhUiw" data-favHash="c3778c5de8bcf23b7d197e025de7fd81">
<a href="/watch/-901607_162943067" data-id="-901607_162943067">
<div class="video-show"></div>
<div class="video-fav" data-hash="c3778c5de8bcf23b7d197e025de7fd81"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-49.userapi.com/c527507/u30699317/video/l_a6f42d09.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">80</span>
<span class="video-time">01:19:59</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Man`s Moto X Enduro X</div>
</a>
</div>
<div class="video-item video-188576299_456239467" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDA0IFs3MjBwXSIsMTgwNSwxMzgwLCJodHRwczpcL1wvc3VuNi0yMS51c2VyYXBpLmNvbVwvYzg1NTIzNlwvdjg1NTIzNjM5NVwvMjU0ODc1XC9VYUFDbnNmTUNGWS5qcGciLCI0ZTg2ZDE5M2I1ZWExZmQ0ODhkNGZlNWQ0ZGMzZTkzNCJd" data-embedUrl="https://daxab.com/player/BCb66aHeWcmNkfwXTHtmUZaERI8K6Zmbh3W0SVOujB_2yS4SLbM8jha7l51YtuEZr-BBwvXkt9I__qdze7zsrOtGTaxWysKo4TkJp22_Iss" data-playerHash="BCb66aHeWcmNkfwXTHtmUZaERI8K6Zmbh3W0SVOujB_2yS4SLbM8jha7l51YtuEZr-BBwvXkt9I__qdze7zsrOtGTaxWysKo4TkJp22_Iss" data-favHash="78bdcfd180ecfe4a5ad2e93a0d744515">
<a href="/watch/-188576299_456239467" data-id="-188576299_456239467">
<div class="video-show"></div>
<div class="video-fav" data-hash="78bdcfd180ecfe4a5ad2e93a0d744515"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-21.userapi.com/c855236/v855236395/254875/UaACnsfMCFY.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,4k</span>
<span class="video-time">30:05</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 04 [720p]</div>
</a>
</div>
<div class="video-item video-200135128_456239373" data-video="WyLQkNC80LXRgNC40LrQsNC90YHQutC40Lkg0LrQvtGI0LzQsNGAIC0gQWxhbiBXYWtlJiMzOTtzIEFtZXJpY2FuIE5pZ2h0bWFyZSAjMSIsMjUwMSwxLCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvVHp4NTZSQnR4YXpuUV81X2lrVFpmZmUzcmhmSWgwZWVhQ2dra1FcLy0tZWFOSkdrTnhnLmpwZyIsIjMzMzM4NzZkMTczMjkzMzU4NmVhYjQxYzBmOWZlNGJjIl0=" data-embedUrl="https://daxab.com/player/a801hmsH8oarA4FSz7h_SdRsHohTlyClUYyjL2NO28sS4HQgU-jk1NKazzppfp0kpY1CIy4jKuqu0lf9grXx-Bc3r7z8f_6eZs3zWHGGhms" data-playerHash="a801hmsH8oarA4FSz7h_SdRsHohTlyClUYyjL2NO28sS4HQgU-jk1NKazzppfp0kpY1CIy4jKuqu0lf9grXx-Bc3r7z8f_6eZs3zWHGGhms" data-favHash="268336d8383e223acd548ce94a4c1928">
<a href="/watch/-200135128_456239373" data-id="-200135128_456239373">
<div class="video-show"></div>
<div class="video-fav" data-hash="268336d8383e223acd548ce94a4c1928"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/Tzx56RBtxaznQ_5_ikTZffe3rhfIh0eeaCgkkQ/--eaNJGkNxg.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 months ago</span>
<span class="video-view">1</span>
<span class="video-time">41:41</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Американский кошмар - Alan Wake&amp;#39;s American Nightmare #1</div>
</a>
</div>
<div class="video-item video195091338_456239074" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIEJlc3QgVHJpY2siLDI0NTcsMiwiaHR0cHM6XC9cL3N1bjYtMjEudXNlcmFwaS5jb21cL2M2MzY5MjVcL3Y2MzY5MjUzMzhcLzJmMWRcLzlKbm9iQ1R0ZjlnLmpwZyIsImUwNWQwZmYwMWJiNDE5MWFhYzRkOWQwNDNkNjk1OWMyIl0=" data-embedUrl="https://daxab.com/player/wwwAg6ffy7c-2PQSj42mcSSY7UAcbXhKKMEdo4vgVIMAHK6OzOIrU1LbvFnLYfMCSXl-4pjMns-N0TvhRWYRtw" data-playerHash="wwwAg6ffy7c-2PQSj42mcSSY7UAcbXhKKMEdo4vgVIMAHK6OzOIrU1LbvFnLYfMCSXl-4pjMns-N0TvhRWYRtw" data-favHash="1393143e615e35dcd69cbdbe2d1e56f3">
<a href="/watch/195091338_456239074" data-id="195091338_456239074">
<div class="video-show"></div>
<div class="video-fav" data-hash="1393143e615e35dcd69cbdbe2d1e56f3"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-21.userapi.com/c636925/v636925338/2f1d/9JnobCTtf9g.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">6 years ago</span>
<span class="video-view">2</span>
<span class="video-time">40:57</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Best Trick</div>
</a>
</div>
<div class="video-item video-188576299_456239333" data-video="WyLQl9Cw0YHRgtC10L3Rh9C40LLQvi3QoNCw0YHQv9GD0YLQvdCw0Y8g0LTQtdCy0YPRiNC60LAg0KHRg9C60YPQvNC4IFRzdW5kZXJlIElucmFuIFNob3VqbyBTdWt1bWkgLSAwMiBbNzIwcF0iLDExNTEsNTY2LCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NzIxNlwvdjg1NzIxNjE1NFwvMWE5Yzc5XC9pemdpTm9pTmZLby5qcGciLCIxMTg1ZjdjMWFlNDQyZjc5OGJjM2RlMGUzODQ5NjM1OCJd" data-embedUrl="https://daxab.com/player/2TImd1CeAzKxYbRBNHj852iDomLMIz9fYlCXo4DYXPnQZz3o2FjTP-aaRvQmNgfyCSbgAwicSUGkb_dYfBWPLPgQnrsFE6ULVeXSmdwR3jk" data-playerHash="2TImd1CeAzKxYbRBNHj852iDomLMIz9fYlCXo4DYXPnQZz3o2FjTP-aaRvQmNgfyCSbgAwicSUGkb_dYfBWPLPgQnrsFE6ULVeXSmdwR3jk" data-favHash="1059f8f2c8602da44c308a4c7842b98c">
<a href="/watch/-188576299_456239333" data-id="-188576299_456239333">
 <div class="video-show"></div>
<div class="video-fav" data-hash="1059f8f2c8602da44c308a4c7842b98c"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c857216/v857216154/1a9c79/izgiNoiNfKo.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">566</span>
<span class="video-time">19:11</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Застенчиво-Распутная девушка Сукуми Tsundere Inran Shoujo Sukumi - 02 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239465" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAyIFs3MjBwXSIsMTgwNiwxMjAyLCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NTYyMFwvdjg1NTYyMDM5NVwvMjQ5ZDg3XC9CcGI5Q2tBUWFBZy5qcGciLCIzNjYyZDFjMWE5NjQ5OTkzZGExMGI3NWYyOTc0MGRjNyJd" data-embedUrl="https://daxab.com/player/kPCggQXddKtHzLhPA-2TV23sV9qD0z0snea-EDlowwrQhPv-3C_HxBDMpRul0JkDTQWQ2dPuFzZcepXs_qjUoetZO4AEZ9l58tIHRz-rQoc" data-playerHash="kPCggQXddKtHzLhPA-2TV23sV9qD0z0snea-EDlowwrQhPv-3C_HxBDMpRul0JkDTQWQ2dPuFzZcepXs_qjUoetZO4AEZ9l58tIHRz-rQoc" data-favHash="ff491e850a72875ffb536f7e4fe9c880">
<a href="/watch/-188576299_456239465" data-id="-188576299_456239465">
<div class="video-show"></div>
<div class="video-fav" data-hash="ff491e850a72875ffb536f7e4fe9c880"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c855620/v855620395/249d87/Bpb9CkAQaAg.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,2k</span>
<span class="video-time">30:06</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 02 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239332" data-video="WyLQl9Cw0YHRgtC10L3Rh9C40LLQvi3QoNCw0YHQv9GD0YLQvdCw0Y8g0LTQtdCy0YPRiNC60LAg0KHRg9C60YPQvNC4IFRzdW5kZXJlIElucmFuIFNob3VqbyBTdWt1bWkgLSAwMSBbNzIwcF0iLDEwODIsOTE4LCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvVVJDWFp3aHhjT285TVR2QWtzWjZpUTd6TXFZXzR5alZiWFByandcL3ZIQnB6RFRmLVNjLmpwZyIsIjY1MTEyODU3MmUzNTkzMGRhYjEyYmQ2MWRkNWNiMWMyIl0=" data-embedUrl="https://daxab.com/player/Pw0egsuEqN_82WHMh5gDE6GnUczxRcL26X87ai1MAIIf3sCviSDTZeNhF1GCMrZOdoY-bieFMMnLhs2gW2MpdYaHbX7VgeDBZAybs7BjdqI" data-playerHash="Pw0egsuEqN_82WHMh5gDE6GnUczxRcL26X87ai1MAIIf3sCviSDTZeNhF1GCMrZOdoY-bieFMMnLhs2gW2MpdYaHbX7VgeDBZAybs7BjdqI" data-favHash="a3947601bc8cd944b48dba7d4c2d585a">
<a href="/watch/-188576299_456239332" data-id="-188576299_456239332">
<div class="video-show"></div>
<div class="video-fav" data-hash="a3947601bc8cd944b48dba7d4c2d585a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/URCXZwhxcOo9MTvAksZ6iQ7zMqY_4yjVbXPrjw/vHBpzDTf-Sc.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">918</span>
<span class="video-time">18:02</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Застенчиво-Распутная девушка Сукуми Tsundere Inran Shoujo Sukumi - 01 [720p]</div>
</a>
</div>
<div class="video-item video-901607_162904584" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIFN0ZXAgVXAiLDMyMzAsNzgsImh0dHBzOlwvXC9zdW45LTY3LnVzZXJhcGkuY29tXC9jNTI2NjE2XC91MzA2OTkzMTdcL3ZpZGVvXC9sXzI0NTEyZWQ0LmpwZyIsIjVhYjZhYjY2MzllMGFiYjI4Y2FkMTA1NzlkNGU0Y2Y4Il0=" data-embedUrl="https://daxab.com/player/dyMmB7iKWdwpnLFo2K-d12_EW_uKEL7e76lgDluR5TKJ1Ll-bzJXtYWInN_pSGly6uvz5juxh6iU376eLnb_WA" data-playerHash="dyMmB7iKWdwpnLFo2K-d12_EW_uKEL7e76lgDluR5TKJ1Ll-bzJXtYWInN_pSGly6uvz5juxh6iU376eLnb_WA" data-favHash="7c5221b338e8f47aef78cdfd04b38762">
<a href="/watch/-901607_162904584" data-id="-901607_162904584">
<div class="video-show"></div>
<div class="video-fav" data-hash="7c5221b338e8f47aef78cdfd04b38762"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-67.userapi.com/c526616/u30699317/video/l_24512ed4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">78</span>
<span class="video-time">53:50</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Step Up</div>
</a>
</div>
<div class="video-item video-150082602_456239164" data-video="WyLQn9GD0YLQtdGI0LXRgdGC0LLQuNC1INCa0LDRgdGB0LDQvdC00YDRiyAyLiDQmtC+0L3QtdGGINGB0LLQtdGC0LAgMjAxMi4gIyAxIC0g0J/QvtC40YHQutC4INC90LDRh9C40L3QsNGO0YLRgdGPIiwxODY3LDEsImh0dHBzOlwvXC9zdW42LTIwLnVzZXJhcGkuY29tXC9jODMwMzA4XC92ODMwMzA4MTI1XC8xN2E1OWRcL1JUV1Rtb2QxOEJ3LmpwZyIsImFkNDI4MTQyOGMzMmEzZTdkMjRmZTczNjhlOTc5YzExIl0=" data-embedUrl="https://daxab.com/player/oeSM_io0_EiR63KaNjyNBHBfX84_6OZN64n3n88KU4UDVVuLn9wNpT4nXxga00zKvAagq5o4grzZjHYuN2FeOGNz8vgXxwkVkOMFRJlyO7I" data-playerHash="oeSM_io0_EiR63KaNjyNBHBfX84_6OZN64n3n88KU4UDVVuLn9wNpT4nXxga00zKvAagq5o4grzZjHYuN2FeOGNz8vgXxwkVkOMFRJlyO7I" data-favHash="f3e0f335d61cf72c6f3c24bb83dcaaaf">
<a href="/watch/-150082602_456239164" data-id="-150082602_456239164">
<div class="video-show"></div>
<div class="video-fav" data-hash="f3e0f335d61cf72c6f3c24bb83dcaaaf"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/c830308/v830308125/17a59d/RTWTmod18Bw.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">3 years ago</span>
<span class="video-view">1</span>
<span class="video-time">31:07</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Путешествие Кассандры 2. Конец света 2012. # 1 - Поиски начинаются</div>
</a>
</div>
<div class="video-item video-20774214_163637013" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTQgKExpdmUgU2hvdyAyKSIsNjA1OCwxMzcsImh0dHBzOlwvXC9zdW45LTgwLnVzZXJhcGkuY29tXC9jNTI3MzE3XC91OTcyODAxMVwvdmlkZW9cL2xfMjZlOWZhMmEuanBnIiwiNzM1NDA1ZjA5YjcwMTcyMDI3M2U3MDRiNDU5NjJhZTAiXQ==" data-embedUrl="https://daxab.com/player/BJ_PResBS-lQQlk4Qu6-olQYiDCuQqPvfHSi7l2Tyk1EEv8CTzY-8jpaHAbovxN4akgxa6zRXCkcnTRV4O9TlQ" data-playerHash="BJ_PResBS-lQQlk4Qu6-olQYiDCuQqPvfHSi7l2Tyk1EEv8CTzY-8jpaHAbovxN4akgxa6zRXCkcnTRV4O9TlQ" data-favHash="d462fb2019aed8ca25f27225aac6c177">
<a href="/watch/-20774214_163637013" data-id="-20774214_163637013">
<div class="video-show"></div>
<div class="video-fav" data-hash="d462fb2019aed8ca25f27225aac6c177"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c527317/u9728011/video/l_26e9fa2a.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">137</span>
<span class="video-time">01:40:58</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x14 (Live Show 2)</div>
</a>
</div>
<div class="video-item video-19767527_165512903" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gRm91cnRoIGRheS4gQk1YIEJpZyBBaXIiLDI5NTUsNDcsImh0dHBzOlwvXC9zdW45LTEwLnVzZXJhcGkuY29tXC9jNTA2MTE4XC91ODE5ODkyMDhcL3ZpZGVvXC9sX2VkZmYxMGU4LmpwZyIsIjQ1ZWExY2M0YWQ1MDNkNTQ4ZDI1ZWY3ODk4MTVkODJkIl0=" data-embedUrl="https://daxab.com/player/wAdvb8F3QuNiUJDeDkEJxaSNGZBNeO7HKNuGCBZid5muWfrcZ6pvlJqq-BxudB6w-vHDk_3ji5H85EgDdUyL9g" data-playerHash="wAdvb8F3QuNiUJDeDkEJxaSNGZBNeO7HKNuGCBZid5muWfrcZ6pvlJqq-BxudB6w-vHDk_3ji5H85EgDdUyL9g" data-favHash="dde051d566857522cb8860d118466d73">
<a href="/watch/-19767527_165512903" data-id="-19767527_165512903">
 <div class="video-show"></div>
<div class="video-fav" data-hash="dde051d566857522cb8860d118466d73"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-10.userapi.com/c506118/u81989208/video/l_edff10e8.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">47</span>
<span class="video-time">49:15</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Fourth day. BMX Big Air</div>
</a>
</div>
<div class="video-item video-53908459_456239661" data-video="WyJYLdC40LPRgNCwIDIgXC8gWCBHYW1lIDIiLDYxOTUsODEsImh0dHBzOlwvXC9zdW42LTIyLnVzZXJhcGkuY29tXC9jNjM2MjE4XC92NjM2MjE4NDU5XC80ODY0M1wvazFoWEpFWk1SNzQuanBnIiwiYjYzN2IzNzFmNjIxNTUxMThlMzYxZWI2NTE1OGNmMTMiXQ==" data-embedUrl="https://daxab.com/player/eNV3mpEjr83CdwyVYj-R8_6q56LduWMzKgTbLyvfnU4SPew1lXV-VtJyKrOuifJINOmomQwPckE90dW8FlQMsQ" data-playerHash="eNV3mpEjr83CdwyVYj-R8_6q56LduWMzKgTbLyvfnU4SPew1lXV-VtJyKrOuifJINOmomQwPckE90dW8FlQMsQ" data-favHash="fcc330c464b617c167c9bfb94242a787">
<a href="/watch/-53908459_456239661" data-id="-53908459_456239661">
<div class="video-show"></div>
<div class="video-fav" data-hash="fcc330c464b617c167c9bfb94242a787"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c636218/v636218459/48643/k1hXJEZMR74.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">5 years ago</span>
<span class="video-view">81</span>
<span class="video-time">01:43:15</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-игра 2 / X Game 2</div>
</a>
</div>
<div class="video-item video-901607_162940653" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIE1vdG8gRnJlZXN0eWxlIiwzNzg1LDcwLCJodHRwczpcL1wvc3VuOS00NS51c2VyYXBpLmNvbVwvYzUxODYxNlwvdTMwNjk5MzE3XC92aWRlb1wvbF82MmE2MTVmOS5qcGciLCI4NWYzNjI0MjMxYjJkOTVkZTE1OGQ4MjM5Yzg5OWUxYiJd" data-embedUrl="https://daxab.com/player/NesojuGCIsOzFAz4Yd_15ekyrQ_YyIdn6ZybzsUGhT1orxTUvRis--UzneO_ex6BTcTaeLTQdDdL63ZzYaYRVw" data-playerHash="NesojuGCIsOzFAz4Yd_15ekyrQ_YyIdn6ZybzsUGhT1orxTUvRis--UzneO_ex6BTcTaeLTQdDdL63ZzYaYRVw" data-favHash="fccdce282eb9ecdf8bd54f3045922e3c">
<a href="/watch/-901607_162940653" data-id="-901607_162940653">
<div class="video-show"></div>
<div class="video-fav" data-hash="fccdce282eb9ecdf8bd54f3045922e3c"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-45.userapi.com/c518616/u30699317/video/l_62a615f9.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">70</span>
<span class="video-time">01:03:05</span>
</div>
</div>
 <div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Moto Freestyle</div>
</a>
</div>
<div class="video-item video-20774214_163592952" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxXC8gUGFydCAxKSBIRCIsMzM3OSw3MywiaHR0cHM6XC9cL3N1bjktNTQudXNlcmFwaS5jb21cL2M1MjkyMDJcL3UzMDg0OTU1XC92aWRlb1wvbF85Mjc0NDRkMC5qcGciLCJjM2ZmZGZhY2RhNDU5OGJiOTc1MTYwNzc3YTU4ZWRmMSJd" data-embedUrl="https://daxab.com/player/Yh0TOYPQ-MK-HZJAPd6K_jitxOCzfUyzyoqtmLrW0U0_NTZBohuVfDKrLYjmAx3jWwCUHw8e7491A7JXxOByZA" data-playerHash="Yh0TOYPQ-MK-HZJAPd6K_jitxOCzfUyzyoqtmLrW0U0_NTZBohuVfDKrLYjmAx3jWwCUHw8e7491A7JXxOByZA" data-favHash="3576c9c62c9f70156112ddb8ea7f498e">
<a href="/watch/-20774214_163592952" data-id="-20774214_163592952">
<div class="video-show"></div>
<div class="video-fav" data-hash="3576c9c62c9f70156112ddb8ea7f498e"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-54.userapi.com/c529202/u3084955/video/l_927444d0.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">73</span>
<span class="video-time">56:19</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1/ Part 1) HD</div>
</a>
</div>
<div class="video-item video-190464408_456239270" data-video="WyJTbGVlcGluZyBEb2dzIOKeqCDQn9GA0L7RhdC+0LbQtNC10L3QuNC1IDEyIOKeqCDQntC00L7Qu9C20LXQvdC40Y8g4oSWIDIgI1NsZWVwaW5nRG9ncyAjU3RhcnBlcml5YSIsMjU3NiwxLCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvNzBJZ0FONW1tclFjajFjR0FEZ2RaZkNPSkhVejJPeHFDdVJmUWdcL2w4Y0JfTXpwcVJFLmpwZyIsIjg5M2Y3YmJlMWQ3MjBmNDdjNzRmODg0NGQ0ZjI1NDhlIl0=" data-embedUrl="https://daxab.com/player/SGBrrJ7ZE0sIkkQ2FrqwdB1wfYqp8qBcUGnrJU6QimVJtqutyaO4dEn0qTBpaoC6ehf-wnuOS94wiEoewFnzsQ7PGIhkfC0sKz6-a-VCTUI" data-playerHash="SGBrrJ7ZE0sIkkQ2FrqwdB1wfYqp8qBcUGnrJU6QimVJtqutyaO4dEn0qTBpaoC6ehf-wnuOS94wiEoewFnzsQ7PGIhkfC0sKz6-a-VCTUI" data-favHash="dfaa105378c66240f1bf25aa8494588d">
<a href="/watch/-190464408_456239270" data-id="-190464408_456239270">
<div class="video-show"></div>
<div class="video-fav" data-hash="dfaa105378c66240f1bf25aa8494588d"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/70IgAN5mmrQcj1cGADgdZfCOJHUz2OxqCuRfQg/l8cB_MzpqRE.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1</span>
<span class="video-time">42:56</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Sleeping Dogs ➨ Прохождение 12 ➨ Одолжения № 2 #SleepingDogs #Starperiya</div>
</a>
</div>
<div class="video-item video384329_164980719" data-video="WyJYLWdhbWVzLCBBc2lhLCBWZXJ0LCAyMDEyIHllYXIiLDI4MzQsMTUsImh0dHBzOlwvXC9zdW45LTIyLnVzZXJhcGkuY29tXC9jNTE4MjEzXC91Mzg0MzI5XC92aWRlb1wvbF82MjEyODI1ZC5qcGciLCJlNzMzZGY4NTdhMGE3ZTczN2FmN2Y1ZDAwMDA4MGI2MCJd" data-embedUrl="https://daxab.com/player/YAUOlZja7C-63HjGWIHgMn3av-jMGSi29XW7iR02iFzPYA-J5fSVhpcaGDeZ9ec4UvYODTIDHO471dGqCwa7YQ" data-playerHash="YAUOlZja7C-63HjGWIHgMn3av-jMGSi29XW7iR02iFzPYA-J5fSVhpcaGDeZ9ec4UvYODTIDHO471dGqCwa7YQ" data-favHash="e5a881faca6da5f106ae9f0095b97d3a">
<a href="/watch/384329_164980719" data-id="384329_164980719">
<div class="video-show"></div>
<div class="video-fav" data-hash="e5a881faca6da5f106ae9f0095b97d3a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-22.userapi.com/c518213/u384329/video/l_6212825d.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">15</span>
<span class="video-time">47:14</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games, Asia, Vert, 2012 year</div>
</a>
</div>
<div class="video-item video-12783167_170084732" data-video="WyJYLUdhbWVzIDIwMTIgUmFsbHktQ3Jvc3MiLDU3MDAsMzYsImh0dHBzOlwvXC9zdW45LTM1LnVzZXJhcGkuY29tXC9jNTEzMzEyXC91MTQzOTcyOTQ5XC92aWRlb1wvbF8xMmM1MzFmNi5qcGciLCI5MDQ1MDMyMmY4MjA1OWQzMmNhNjFmNWFkMDUzNzMyYiJd" data-embedUrl="https://daxab.com/player/J9zeAHBuroXfM5us-fQjHk_JhGepJP5IA4OVGC_XhpiuLKAGh35ydT3tw6bTVhJA7xdlpwWMe37waB3qWT2vEQ" data-playerHash="J9zeAHBuroXfM5us-fQjHk_JhGepJP5IA4OVGC_XhpiuLKAGh35ydT3tw6bTVhJA7xdlpwWMe37waB3qWT2vEQ" data-favHash="a6401941b889bf3e78bc1df3df07fa72">
<a href="/watch/-12783167_170084732" data-id="-12783167_170084732">
<div class="video-show"></div>
<div class="video-fav" data-hash="a6401941b889bf3e78bc1df3df07fa72"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-35.userapi.com/c513312/u143972949/video/l_12c531f6.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 years ago</span>
<span class="video-view">36</span>
<span class="video-time">01:35:00</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games 2012 Rally-Cross</div>
</a>
</div>
<div class="video-item video-19767527_165206109" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gRmlyc3QiLDMxNDYsMjgsImh0dHBzOlwvXC9zdW45LTgyLnVzZXJhcGkuY29tXC9jNTMyMzA0XC91ODE5ODkyMDhcL3ZpZGVvXC9sXzYwNWY0ZmQ0LmpwZyIsIjdkZmU1MDE3MjJhNzMzZGFiZGZmZjMyM2M2Njk3NDUwIl0=" data-embedUrl="https://daxab.com/player/NTF_2oPE5t4SnAwHiNQ_JmcXwjrDNHao0xAhxY8ECjYROXc0F8evdOWdCKz1UVfSR75LRwFQwGr9RzdahjRfRQ" data-playerHash="NTF_2oPE5t4SnAwHiNQ_JmcXwjrDNHao0xAhxY8ECjYROXc0F8evdOWdCKz1UVfSR75LRwFQwGr9RzdahjRfRQ" data-favHash="cb12f1c853d023943344fa576e1d169a">
<a href="/watch/-19767527_165206109" data-id="-19767527_165206109">
<div class="video-show"></div>
<div class="video-fav" data-hash="cb12f1c853d023943344fa576e1d169a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-82.userapi.com/c532304/u81989208/video/l_605f4fd4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">28</span>
<span class="video-time">52:26</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. First</div>
</a>
</div>
<div class="video-item video-19767527_165214922" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gVGhyaWQgZGF5LiBCTVggU3RyZWV0IEZpbmFsIiwxMjcwLDExLCJodHRwczpcL1wvc3VuOS00Ni51c2VyYXBpLmNvbVwvYzUzNTUxMlwvdTgxOTg5MjA4XC92aWRlb1wvbF82NmRkZGZmYy5qcGciLCI5MGU1YjNiZGU4MjNhMjhkNTFlMzRiOGU3NmU2MzYzNiJd" data-embedUrl="https://daxab.com/player/AZd6c7RldvHNufk1vhg2KCD8iY2Q62rT-FL6X9zGnfyFLJS2IVui6gIwkQ28kV3-dGwKKwo5CqqpDu7ToSdwWw" data-playerHash="AZd6c7RldvHNufk1vhg2KCD8iY2Q62rT-FL6X9zGnfyFLJS2IVui6gIwkQ28kV3-dGwKKwo5CqqpDu7ToSdwWw" data-favHash="a89d88f3a3ec1b0d933a35869c8a565a">
<a href="/watch/-19767527_165214922" data-id="-19767527_165214922">
<div class="video-show"></div>
<div class="video-fav" data-hash="a89d88f3a3ec1b0d933a35869c8a565a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-46.userapi.com/c535512/u81989208/video/l_66dddffc.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">11</span>
<span class="video-time">21:10</span>
</div>
</div>
 <div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Thrid day. BMX Street Final</div>
</a>
</div>
<div class="video-item video-901607_162943115" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIFJldmlldyIsNTM4MSwxNiwiaHR0cHM6XC9cL3N1bjktODcudXNlcmFwaS5jb21cL2M1MjcyMTZcL3UzMDY5OTMxN1wvdmlkZW9cL2xfZDhiOTA2ZjQuanBnIiwiM2MyY2QzNDFhMTM4ODlkNjcxNjJmNmFlMTU5ZDBlMTIiXQ==" data-embedUrl="https://daxab.com/player/vb2uDI3fnLvWVHOytWxjXmcWsoM8nlGhGeoOuJB8tLEsdBbz7oaCakl8YqOmce3z4O_cDAKL0S_kkTvxG-NM8w" data-playerHash="vb2uDI3fnLvWVHOytWxjXmcWsoM8nlGhGeoOuJB8tLEsdBbz7oaCakl8YqOmce3z4O_cDAKL0S_kkTvxG-NM8w" data-favHash="f9d2d9c63926cc6aedd852f38b190488">
<a href="/watch/-901607_162943115" data-id="-901607_162943115">
<div class="video-show"></div>
<div class="video-fav" data-hash="f9d2d9c63926cc6aedd852f38b190488"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-87.userapi.com/c527216/u30699317/video/l_d8b906f4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">16</span>
<span class="video-time">01:29:41</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Review</div>
</a>
</div>
<div class="video-item video-19767527_165214946" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gVGhyaWQgZGF5LiBCTVggVmVydCIsOTkzLDg0LCJodHRwczpcL1wvc3VuOS03OS51c2VyYXBpLmNvbVwvYzUxNDUwNFwvdTgxOTg5MjA4XC92aWRlb1wvbF9jZDk1NjI3Ny5qcGciLCIwZjVhMDQ4NzkwYTM2NWYxYzNlNzUyNWI3NGIxYjQ1ZCJd" data-embedUrl="https://daxab.com/player/pii3rK6HgXLCiQyFNmNFa1zz8HNIh65dyg_YRn6TOokZkh1RDoG2Hq81D2IT0GqX81FFeewQTV5bdxlz0aU5Kw" data-playerHash="pii3rK6HgXLCiQyFNmNFa1zz8HNIh65dyg_YRn6TOokZkh1RDoG2Hq81D2IT0GqX81FFeewQTV5bdxlz0aU5Kw" data-favHash="8af51ac89ba5920efcbea5001540648a">
<a href="/watch/-19767527_165214946" data-id="-19767527_165214946">
<div class="video-show"></div>
<div class="video-fav" data-hash="8af51ac89ba5920efcbea5001540648a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-79.userapi.com/c514504/u81989208/video/l_cd956277.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">84</span>
<span class="video-time">16:33</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Thrid day. BMX Vert</div>
</a>
</div>
<div class="video-item video581608605_456239155" data-video="WyJSb2NrIHRoZSBWb3RlISAtIC0gUHJvcGFnYW5kYVdhdGNoICggMTQ0IFggMTQ0ICkubXA0IiwxMzE1LDE4NCwiaHR0cHM6XC9cL3N1bjYtMjMudXNlcmFwaS5jb21cLzFwdVZmMjRaVlMyd0VvNldVb3lJdV9EM2hQY1I1S1BoMWd0Uk93XC9WY2txVzB6WEVlUS5qcGciLCI3OGJlNzFlOTdmZWU0ODgwZmUxNzE5NWM1YTkxMGRjYyJd" data-embedUrl="https://daxab.com/player/0rHcPQLwj2Cn8pQYLD_btBKnmZG898uYxr7bH9ynXY4zyvgX3eWHTn1fvrh1i6Zr7hD8hWajZs4NyUhGzsh_PA" data-playerHash="0rHcPQLwj2Cn8pQYLD_btBKnmZG898uYxr7bH9ynXY4zyvgX3eWHTn1fvrh1i6Zr7hD8hWajZs4NyUhGzsh_PA" data-favHash="77b9ed060e5f68bd61219753639c34cf">
<a href="/watch/581608605_456239155" data-id="581608605_456239155">
<div class="video-show"></div>
<div class="video-fav" data-hash="77b9ed060e5f68bd61219753639c34cf"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/1puVf24ZVS2wEo6WUoyIu_D3hPcR5KPh1gtROw/VckqW0zXEeQ.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">184</span>
<span class="video-time">21:55</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Rock the Vote! - - PropagandaWatch ( 144 X 144 ).mp4</div>
</a>
</div>
<div class="video-item video-20774214_163592955" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxXC8gUGFydCAyKSBIRCIsMzMxMyw0MCwiaHR0cHM6XC9cL3N1bjktNjEudXNlcmFwaS5jb21cL2M1MTg2MDJcL3UzMDg0OTU1XC92aWRlb1wvbF83MmE0MTdiMi5qcGciLCI2MTM1NjNhMzdkZTY3NThjY2FiYzk2Njc5NzljZDg0OSJd" data-embedUrl="https://daxab.com/player/qWdR1JPcwga4hVxP-uLMqfMYjIEl1RqWh7_DAQwAykTykgrQiXV2vRCIdsycGmBrQh7XZJYDWww6XIpbf6_HnA" data-playerHash="qWdR1JPcwga4hVxP-uLMqfMYjIEl1RqWh7_DAQwAykTykgrQiXV2vRCIdsycGmBrQh7XZJYDWww6XIpbf6_HnA" data-favHash="d8daed6fac8e218db9f4e78de3e73b29">
<a href="/watch/-20774214_163592955" data-id="-20774214_163592955">
<div class="video-show"></div>
<div class="video-fav" data-hash="d8daed6fac8e218db9f4e78de3e73b29"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-61.userapi.com/c518602/u3084955/video/l_72a417b2.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">40</span>
<span class="video-time">55:13</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1/ Part 2) HD</div>
</a>
</div>
<div class="video-item video-4296410_167364578" data-video="WyJYLWdhbWVzIDIwMTIgYm14IHZlcnQgZmluYWwgKCDQvdCwINGA0YPRgdGB0LrQvtC8KSIsOTkzLDYyLCJodHRwczpcL1wvc3VuOS0zMy51c2VyYXBpLmNvbVwvYzUyMzYwMFwvdTk3MDA0ODdcL3ZpZGVvXC9sX2NkYmZhMWUyLmpwZyIsIjE5NmVhYWE3NzE2MDhiYmJkYzc4ZjE5YTg3MDk5NDVjIl0=" data-embedUrl="https://daxab.com/player/gW9yPh7iMDPd9zz9LQNpqKil9YGkF1eNeA9E1lG9YcZJ6JS28QS5dr3IgTWekG7DnFE44sVPkqj7yi7Z3sXJQQ" data-playerHash="gW9yPh7iMDPd9zz9LQNpqKil9YGkF1eNeA9E1lG9YcZJ6JS28QS5dr3IgTWekG7DnFE44sVPkqj7yi7Z3sXJQQ" data-favHash="ed545ebb89984148892cfa8ee7c45ef8">
<a href="/watch/-4296410_167364578" data-id="-4296410_167364578">
<div class="video-show"></div>
<div class="video-fav" data-hash="ed545ebb89984148892cfa8ee7c45ef8"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-33.userapi.com/c523600/u9700487/video/l_cdbfa1e2.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">8 years ago</span>
<span class="video-view">62</span>
<span class="video-time">16:33</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012 bmx vert final ( на русском)</div>
</a>
</div>
<div class="video-item video-145081708_456242402" data-video="WyLQuNC1IEJvcmRlcmxhbmRzIEdPVFkgWzIwMDldIFBDXC9QUzNcL1hib3gzNjAuINCf0YDQvtGF0L7QttC00LXQvdC40LUg0LfQsCDQm9C40LvQuNGCINGHLjggKzE4IFtkYXIiLDI2NDcsMTE4LCJodHRwczpcL1wvaS5teWNkbi5tZVwvZ2V0VmlkZW9QcmV2aWV3P2lkPTEzMjIzNzY4MjU1NjMmaWR4PTEmdHlwZT0zOSZ0a249U3VPTUVURDh2R1dRMTNIaHdhdzI0WmhoX0xRJmZuPXZpZF9sIiwiZmRkNjMwM2Y3YTczNzUxYjlkZGQzZGFiYjE5NGEwNzYiXQ==" data-embedUrl="https://daxab.com/player/iwj0b36jz9kWoz8JEzX1w0cC9ZJpN0LvkIIaA7ByEPcZ3-wNe9os76T3cmYmj0ksGtsRoqAsrXyanYiQfzlCG3DhLjR2jB4A1rOg--sCVVs" data-playerHash="iwj0b36jz9kWoz8JEzX1w0cC9ZJpN0LvkIIaA7ByEPcZ3-wNe9os76T3cmYmj0ksGtsRoqAsrXyanYiQfzlCG3DhLjR2jB4A1rOg--sCVVs" data-favHash="697a4941bacc445326c3e7e84ee48753">
<a href="/watch/-145081708_456242402" data-id="-145081708_456242402">
<div class="video-show"></div>
<div class="video-fav" data-hash="697a4941bacc445326c3e7e84ee48753"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://i.mycdn.me/getVideoPreview?id=1322376825563&idx=1&type=39&tkn=SuOMETD8vGWQ13Hhwaw24Zhh_LQ&fn=vid_l"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">118</span>
<span class="video-time">44:07</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">ие Borderlands GOTY [2009] PC/PS3/Xbox360. Прохождение за Лилит ч.8 +18 [dar</div>
</a>
</div>
<div class="video-item video104849236_456277453" data-video="WyJNQVggUEFZTkUgMyBOUEMgV2FycyAyMyAoVUZFIHZzIENyYWNoYSBQcmV0bykiLDE4ODMsMSwiaHR0cHM6XC9cL3N1bjYtMjIudXNlcmFwaS5jb21cL1d4d08yUXhIVUk4Qm5qaks1R3U2TGQxMGlLVWZDYkFpQTVMRjlnXC9zTGdBSEQ4U0lwOC5qcGciLCI5ZTJkMjU5Mzk5YmZhYzBhNzU1YWVmOGMxNDE1ZjhhZSJd" data-embedUrl="https://daxab.com/player/afFRHJ6DVED9yrk6KnQhClFOiLTSjCvYskOT1h5oY2c43DSILUFX3z7pAyFzZfT74XUqihfCDecHReTYYQbarA" data-playerHash="afFRHJ6DVED9yrk6KnQhClFOiLTSjCvYskOT1h5oY2c43DSILUFX3z7pAyFzZfT74XUqihfCDecHReTYYQbarA" data-favHash="daff3e6ec27aa95eabcde1e74d2bdced">
<a href="/watch/104849236_456277453" data-id="104849236_456277453">
<div class="video-show"></div>
<div class="video-fav" data-hash="daff3e6ec27aa95eabcde1e74d2bdced"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/WxwO2QxHUI8BnjjK5Gu6Ld10iKUfCbAiA5LF9g/sLgAHD8SIp8.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 months ago</span>
<span class="video-view">1</span>
<span class="video-time">31:23</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">MAX PAYNE 3 NPC Wars 23 (UFE vs Cracha Preto)</div>
</a>
</div>
<div class="video-item video-901607_162902722" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuINCe0LHQt9C+0YAg0L/QtdGA0LLQvtCz0L4g0LTQvdGPIiwyNjkyLDE0LCJodHRwczpcL1wvc3VuOS0zMy51c2VyYXBpLmNvbVwvYzUyNjUwN1wvdTMwNjk5MzE3XC92aWRlb1wvbF8xYWFlYTc5ZC5qcGciLCJjNjU2ODIyYzQ3YzY3MTEyM2U5Y2QxYzYxMWZhOTQzMyJd" data-embedUrl="https://daxab.com/player/Q0EW6rBUddWSePblCn6wWwCzB2KPYGboMuM1JfUKsICp84f2XapAY8Yj2BTK0ormPRN3bMhis582c5AgFqL-gQ" data-playerHash="Q0EW6rBUddWSePblCn6wWwCzB2KPYGboMuM1JfUKsICp84f2XapAY8Yj2BTK0ormPRN3bMhis582c5AgFqL-gQ" data-favHash="ad3b2031e76f2b45329c183030b3b5e2">
<a href="/watch/-901607_162902722" data-id="-901607_162902722">
<div class="video-show"></div>
<div class="video-fav" data-hash="ad3b2031e76f2b45329c183030b3b5e2"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-33.userapi.com/c526507/u30699317/video/l_1aaea79d.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">14</span>
<span class="video-time">44:52</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Обзор первого дня</div>
</a>
</div>
<div class="video-item video593543_162951285" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIEJlc3QgVHJpY2siLDI0NTcsMjUsImh0dHBzOlwvXC9zdW45LTY5LnVzZXJhcGkuY29tXC9jNTI3MTA3XC91MzA2OTkzMTdcL3ZpZGVvXC9sXzk5ZGM1MjljLmpwZyIsIjBmODBkOWU2YWYzNTQ2NDY0YTdmZTE3OTRkYjk1YjVmIl0=" data-embedUrl="https://daxab.com/player/KCBz1glUT5Xps-5qJNquwZIwjl44AFMqHPKrguD2gZvy1SOS7RnzpfDEDytZhEOIE7e-M-SWnb6z8RUXCsH3iQ" data-playerHash="KCBz1glUT5Xps-5qJNquwZIwjl44AFMqHPKrguD2gZvy1SOS7RnzpfDEDytZhEOIE7e-M-SWnb6z8RUXCsH3iQ" data-favHash="d6ab0848c96f33c96ad3032c4a0d0637">
<a href="/watch/593543_162951285" data-id="593543_162951285">
<div class="video-show"></div>
<div class="video-fav" data-hash="d6ab0848c96f33c96ad3032c4a0d0637"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-69.userapi.com/c527107/u30699317/video/l_99dc529c.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">25</span>
<span class="video-time">40:57</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Best Trick</div>
</a>
</div>
<div class="video-item video-197946026_456239614" data-video="WyLQn9GA0L7RhdC+0LbQtNC10L3QuNC1IFNsZWVwaW5nIERvZ3MgKDIwMTMpICMyIC0g0JrQu9GD0LEgXCLQkdCw0Lwt0JHQsNC8XCIiLDczMDgsNDMsImh0dHBzOlwvXC9zdW42LTIzLnVzZXJhcGkuY29tXC9xMGxBdlpKd21wZE9samtzd3BZYW5MeFpKUl9aTDF6YjB1RXdfZ1wvYllXZE50M05xSDAuanBnIiwiYmU2NjkwN2I3ZGJjNzY1YjVkNzE5MjU5ZWQ0NTZmMDEiXQ==" data-embedUrl="https://daxab.com/player/aaeMDv6HAhtHGfFfSmsQ-V3Jz8uZbs-0h1Yqbc7xaoQ2D5PvzAdMZcE6KsCgyuCVPL4HklThVIiCzF7lXWkqKAHrBEQTRQFxUE_MBJgxsAk" data-playerHash="aaeMDv6HAhtHGfFfSmsQ-V3Jz8uZbs-0h1Yqbc7xaoQ2D5PvzAdMZcE6KsCgyuCVPL4HklThVIiCzF7lXWkqKAHrBEQTRQFxUE_MBJgxsAk" data-favHash="004476a96ec3d868483e77418cf244f2">
<a href="/watch/-197946026_456239614" data-id="-197946026_456239614">
<div class="video-show"></div>
<div class="video-fav" data-hash="004476a96ec3d868483e77418cf244f2"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/q0lAvZJwmpdOljkswpYanLxZJR_ZL1zb0uEw_g/bYWdNt3NqH0.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">4 months ago</span>
<span class="video-view">43</span>
<span class="video-time">02:01:48</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Прохождение Sleeping Dogs (2013) #2 - Клуб &quot;Бам-Бам&quot;</div>
</a>
</div>
<div class="video-item video-27575945_163557091" data-video="WyJYLWdhbWVzIDIwMTIs0KTRgNC40YHRgtCw0LnQuyzQn9C+0LvQvdC+0YHRgtGM0Y4hIiwzNzg1LDEzLCJodHRwczpcL1wvc3VuOS04MC51c2VyYXBpLmNvbVwvYzUyNjIxNVwvdTU5NDM1OTcxXC92aWRlb1wvbF84YThkNGI0NS5qcGciLCI3OTMwYzNkY2Y5NzdkZDhiYWM1ZWQ1NDJiYTkwZGIyOCJd" data-embedUrl="https://daxab.com/player/DsZu4aOqTh94Zrl9YRl2owUKZa28_-bhX-766JHz3EfuNm4WBTMAtvpKRMsO4V77TiIUxbfn8BsiYnJWDra7KQ" data-playerHash="DsZu4aOqTh94Zrl9YRl2owUKZa28_-bhX-766JHz3EfuNm4WBTMAtvpKRMsO4V77TiIUxbfn8BsiYnJWDra7KQ" data-favHash="9795118eba65377777021ab858d2594b">
<a href="/watch/-27575945_163557091" data-id="-27575945_163557091">
<div class="video-show"></div>
<div class="video-fav" data-hash="9795118eba65377777021ab858d2594b"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c526215/u59435971/video/l_8a8d4b45.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">13</span>
<span class="video-time">01:03:05</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012,Фристайл,Полностью!</div>
</a>
</div>
<span class="standalonead" style="margin: 0px 13px;padding-bottom: 15px;">
<iframe width="300" height="250" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10002729?time=1555363895445" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10002729"></iframe>
</span>
<div class="more-similar" onclick="event.cancelBubble = true; loadMoreSimilar(this);" data-page="1" data-video="-53425992_165397466" data-hash="84d987ceaf814a97d050f983234f2315d5157387">Show more</div>
</div>

<script>
$('.tabs > div.tab-btn').on('click', function() {
  if ($(this).hasClass('tab-show')) {
    return;
  }

  $(this).parent().children('div').removeClass('tab-show');
  $(this).addClass('tab-show');

  $('.tab-result').removeClass('tab-show');
  $('.tab-result.' + this.id).addClass('tab-show');

  // if ($('.width').width() < 1020 && this.id == 'tab_comments') {
  //   loadComments($('#video_hash').val().trim());
  // }
});
</script>
</div>
</div>
</div>
<div class="footer">
<div class="width">
<div class="now">
<b>You might be interested</b>
<span><a href="/video/Michele James">Michele James</a> <a href="/video/Mackenzie Mace">Mackenzie Mace</a> <a href="/video/Jessica Robbins">Jessica Robbins</a> <a href="/video/Mendinggo">Mendinggo</a> <a href="/video/Lela Star">Lela Star</a> <a href="/video/Kenzie Reeves">Kenzie Reeves</a> <a href="/video/Cory Chase">Cory Chase</a> <a href="/video/Luna Okko">Luna Okko</a> <a href="/video/Luxury Girl">Luxury Girl</a> <a href="/video/Uber Compilation">Uber Compilation</a> <a href="/video/مترجم">مترجم</a> <a href="/video/Jasmine James Danny D">Jasmine James Danny D</a> <a href="/video/Tifa">Tifa</a> <a href="/video/Ryan Conner">Ryan Conner</a> <a href="/video/Lana Rhoades">Lana Rhoades</a> <a href="/video/Brandi Love">Brandi Love</a> <a href="/video/Russian Schoolgirls">Russian Schoolgirls</a> <a href="/video/Alicia Trece Fist">Alicia Trece Fist</a> <a href="/video/Savannah Bond">Savannah Bond</a> <a href="/video/Initiations">Initiations</a></span>
</div>
<div class="clear"></div>
<div class="copy">
<a href="/">DaftSex</a>
<a href="/browse">Browse</a>
<a href="/hottest">Hottest</a>
<a href="/categories">Category</a>
<a href="/pornstars">Pornstars</a>
<a href="/holders">DMCA</a>
<a href="/privacy">Privacy</a>
<a href="/faq">FAQ</a>
<a href="/contact">Contact Us</a>
<a href="https://daft.sex/" target="_blank" title="Start search Page for DaftSex">DaftSex Search</a>
<a href="https://theporndude.com/" target="_blank" rel="nofollow" style="font-weight: bold;">Best Porn Sites</a>
<a href="https://twitter.com/DaftPost" target="_blank" class="twitter font-weight-bold" style="background: #01abf1 url(/img/twitter.png) no-repeat scroll center center;border-radius: 25px;color: transparent;background-size: 27px;"><i class="icon-twistter"></i>Twitter</a>
</div>
</div>
</div>
</div>
<div class="bg_layer"></div>

<script>
onPageLoaded();
$(window).load(onPageReady);
previewEvents();
</script>
<script type='text/javascript' src='//sufficientretiredbunker.com/a5/96/32/a59632dda777535e591fa2e7dde66a93.js'></script>
<script type="text/javascript" async src="/js/app.ve1c0c4626c025980.js"></script>
<script type="text/javascript">
!function(){var w,n=window.outerWidth-window.innerWidth,o=window.outerHeight-window.innerHeight,t=$(window).width(),e=$(window).height();function i(i){w&&clearTimeout(w),w=setTimeout(function(){var i=window.outerWidth-window.innerWidth,w=window.outerHeight-window.innerHeight;n!==i&&o!==w||n==i&&o==w||t==$(window).width()&&e==$(window).height()||t!==$(window).width()&&e!==$(window).height()||!winFullscreen&&(400<i&&n<i&&400<Math.abs(i-n)||200<w&&o<w&&200<Math.abs(w-o))&&(setCookie("_dt",1,{path:"/",expires:1800,domain:"."+location.hostname}),dt()),n=window.outerWidth-window.innerWidth,o=window.outerHeight-window.innerHeight,t=$(window).width(),e=$(window).height()},500)}browser.mobile||$(window).off("resize",i).on("resize",i)}();
</script>

<div style="display:none;">
<script type="text/javascript">
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
</script>
</div>


</body>
</html>
<!DOCTYPE html>
<html>
<head data-extra="aIX2DL3JIGnO7i8k6HvdT4">
<meta charset="UTF-8">
<title>2012 X-Games 18 motocross best trick — DaftSex</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="/index.js?6" defer></script>
<link rel="manifest" href="/manifest.webmanifest">
<meta name="robots" content="all" />
<meta name="robots" content="noarchive" />
<meta name="description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality">
<meta name="keywords" content="Watch, video, daftsex, motocross, best, trick, daftsex">
<meta property="og:url" content="https://daftsex.com/watch/-53425992_165397466" />
<meta property="og:type" content="video.movie" />
<meta property="og:title" content="2012 X-Games 18 motocross best trick" />
<meta property="og:description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality" />
<meta property="og:image" content="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg" />
<meta property="og:video:type" content="text/html" />
<link href="/css/robotocondensed.v4b6944ca5bbf3c8b.css" rel="stylesheet" type="text/css">
<link rel="icon" type="image/png" sizes="196x196" href="/img/daftlogo196x196.png?2">
<link rel="shortcut icon" href="/img/favicon.ico?5" />
<meta name="theme-color" content="#C00">
<link rel="stylesheet" href="/css/common.v112268244c82433e.css" />
<link rel="stylesheet" href="/css/icons.v38e1854b6d41caec.css" />
<link rel="stylesheet" href="/css/dark.ve32b1eba318e418e.css" />
<script type="text/javascript">
window.globEmbedUrl = 'https://daxab.com/player/';
window.timeNow = 1658939817;
setInterval(function() {
  window.timeNow++;
}, 1000);
window.liteopen = false;
window.is_logged = false;
</script>
<script type="text/javascript" src="/js/jquery-2.1.1.min.v18b7e87c91d98481.js"></script>
<script type="text/javascript" src="/js/history.v955089448af5a0c8.js"></script>
<script type="text/javascript" src="/js/nprogress.v3410974b8841b4f3.js"></script>
<script type="text/javascript" src="/js/nouislider.v2192f61dc764023a.js"></script>
<script type="text/javascript" src="/js/select.ve363dc0076d2c78b.js"></script>
<script type="text/javascript" src="/js/common.vb1f31c4b59a9e4d1.js"></script>
<script type="text/javascript" src="/js/auth.vfa3c32a15fba2304.js"></script>
<script type="text/javascript" src="/js/jquery.mutations.min.v4b147b138a5b1019.js"></script>
<script type="text/javascript" src="/js/fav.v764365b62392eb58.js"></script>
<script type="text/javascript" src="/js/share.vdf8ddf291dc2f417.js"></script>
<script type="text/javascript" src="/js/likes.v5e342c5feda70804.js"></script>
<script type="text/javascript">
window._stv = '67c5c441c9e2';
window.log_version = 'fbcb78248ac55029';
</script>
</head>
<body>
<a class="up" onclick="toTop();">&uarr;</a>
<div class="page_wrapper">
<div class="header">
<div class="width">
<div class="header_inner">
<a onclick="window.history.go(-1); return false;" title="Back" class="back-logo"><i class="icon-left"></i></a>
<a href="/" title="DaftSex" class="logo">
<svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="55.000000pt" height="50.000000pt" viewBox="0 0 311.000000 127.000000" preserveAspectRatio="xMidYMid meet">
<g transform="translate(0.000000,180.000000) scale(0.100000,-0.100000)" fill="#dd3333" stroke="none">
<path d="M895 1713 c-142 -78 -362 -148 -559 -178 -54 -9 -102 -18 -107 -21
-14 -8 -10 -228 5 -354 36 -284 110 -501 235 -692 110 -167 280 -320 442 -398
l46 -23 54 27 c149 75 311 216 408 356 59 84 131 221 168 320 35 92 79 287 93
410 12 101 18 346 8 355 -2 2 -49 11 -104 19 -215 33 -424 99 -578 184 l-51
29 -60 -34z m15 -818 l0 -636 -45 31 c-128 88 -246 229 -327 390 -86 171 -148
427 -148 612 0 72 2 78 23 83 209 46 339 85 447 134 25 11 46 21 48 21 1 0 2
-286 2 -635z m197 592 c99 -40 258 -87 343 -102 79 -14 74 -4 67 -133 -4 -63
-11 -120 -16 -125 -5 -5 -38 -6 -83 -1 l-73 9 3 46 3 45 -63 13 c-35 6 -75 14
-90 18 l-28 5 0 -95 c0 -81 3 -96 18 -102 9 -3 81 -9 160 -12 78 -3 142 -10
142 -15 0 -14 -39 -155 -61 -221 -35 -101 -118 -253 -188 -340 -65 -79 -226
-221 -242 -212 -8 5 -12 501 -5 514 3 4 43 2 90 -3 l85 -9 3 -41 3 -41 37 65
c59 105 64 96 -61 104 -61 3 -122 11 -136 16 l-25 10 0 325 c0 179 2 325 4
325 2 0 53 -20 113 -43z" />
<path d="M628 1242 c-79 -19 -78 -15 -53 -153 18 -102 51 -201 98 -294 69
-137 66 -145 67 178 l0 287 -22 -1 c-13 -1 -53 -9 -90 -17z" />
</g>
</svg>
</a>
<div class="clear-form"><i class="icon-cancel"></i></div>
<form class="search-form" onsubmit="return search(true);">
<input type="text" id="q" autocomplete="off" value="" placeholder="Search millions of videos...">
<a title="RandomTV" href="/video/Russian" class="randomQuery"><span class="deskrandom"></span></a>
<button type="submit" title="Search button">Search</button>
<div class="head-menu"></div>
</form>
<div class="head-menu module-browse"><a href="/browse">Browse</a></div>
<div class="head-menu module-hottest"><a href="/hottest">Hottest</a></div>
<div class="head-menu module-categories"><a href="/categories">Category</a></div>
<div class="head-menu module-pornstars"><a href="/pornstars">Pornstars</a></div>
<div class="head-menu"><a href="https://theporndude.com/" target="_blank" rel="nofollow" style="color: #000;">Best Porn Sites</a></div>
<button class="btn-search" type="button"><i class="icon-left"></i></button>
<span class="hide-item">

<a href="/pornstars"><span class="btn-pornstars"></span></a>
<a href="/categories"><span class="btn-catt"></span></a>
<a href="/hottest"><span class="btn-hottest"></span></a>
<a href="/browse"><span class="btn-browse"></span></a>
</span>
<span class="auth login hide-item" title="Login" onclick="Auth.Login();"></span>
</div>
</div>
<div id="progress_content"></div>
<div class="suggest_search"><div class="suggest_inner width"><ul class="suggest_items"></ul></div></div>
</div>
<div class="bg_width">
<div class="width">

<div class="aboveVideo standalonead" style="text-align: center;padding-top: 75px;margin-bottom: -70px;">

<span style="
    text-align: center;
"> <a style="
    text-align: left;
    color: #fff;
    font-size: 15px;
    text-decoration: none;
    padding: 5px;
    top: 61px;
    margin: 5px;
    border-radius: 25px;
    display: block;
    background-color: #5e62ff;
    " href="https://artsporn.com" target="_blank">
ℹ: ARTSPORN.COM - Another new Alternative, faster website without ads. 👊⚡️⚡️⚡️</a></span>


<iframe width="300" height="150" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10009021?time=1575323689465" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10012984"></iframe>




</div>
<div class="aboveVideoPC standalonead" style="text-align: center;padding-top: 75px;margin-bottom: -70px;">








</div>
<div class="content-wrapper" id="content">
<div itemscope itemtype="http://schema.org/VideoObject">
<link itemprop="thumbnailUrl" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
<span itemprop="thumbnail" itemscope itemtype="http://schema.org/ImageObject">
<link itemprop="url" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
<link itemprop="contentUrl" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
</span>
<meta itemprop="name" content="2012 X-Games 18 motocross best trick" />
<meta itemprop="uploadDate" content="2013-07-17T16:03:39+04:00">
<meta itemprop="description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality" />
<meta itemprop="duration" content="T28M50S" />
</div>

<div class="video">
<a class="standalonead" style="font-size: 14px; font-weight: bold;color:#0a0a0a; text-align: center;" href="https://landing.brazzersnetwork.com/?ats=eyJhIjoyOTIyOTcsImMiOjU3NzAzNTQ4LCJuIjoxNCwicyI6OTAsImUiOjg4MDMsInAiOjU3fQ==" target="_blank" rel="nofollow"><div class="message-container" style="background-color:#ffd001;padding: 7px;font-size: 17px;font-weight: 700;"><span class="nomobile">EXCLUSIVE DaftSex OFFER - Join BRAZZERS Only 1$ Today ! [PROMO]</span> <span class="nomobile"><span style="color:rgba(255, 208, 1)"></span><span style="color:rgba(255, 208, 1)"><span></span></span></span></div></a>
<div style="background:#000000;outline: 0px solid #fff;" class="frame_wrapper">
<script id="data-embed-video-O6E6HPniMHSrF5QyHg_qoTkX-NIe6cUlOdTZEl-c9KbTp_p9L4mZrQF0i9V0WPMlWjm8RyrNlryM46Vfhx0glKEBYUw42qUz-2EJL_Kci1o">
if (window.DaxabPlayer && window.DaxabPlayer.Init) {
  DaxabPlayer.Init({
    id: 'video-53425992_165397466',
    origin: window.globEmbedUrl.replace('/player/', ''),
    embedUrl: window.globEmbedUrl,
    hash: "O6E6HPniMHSrF5QyHg_qoTkX-NIe6cUlOdTZEl-c9KbTp_p9L4mZrQF0i9V0WPMlWjm8RyrNlryM46Vfhx0glKEBYUw42qUz-2EJL_Kci1o",
    color: "f12b24",
    styles: {
      border: 0,
      overflow: 'hidden',
      marginBottom: '-5px'
    },
    attrs: {
      frameborder: 0,
      allowFullScreen: ''
    },
    events: function(data) {
      if (data.event == 'player_show') {
        dt();

        HistoryWatch.set("-53425992_165397466", "WyIyMDEyIFgtR2FtZXMgMTggbW90b2Nyb3NzIGJlc3QgdHJpY2siLDE3MzAsNjIsImh0dHBzOlwvXC9zdW45LTQ2LnVzZXJhcGkuY29tXC9jNTM1MzA0XC91NjI1MjQ2NVwvdmlkZW9cL2xfZGQ5MzBlOGYuanBnIiwiODc4OGE3MDgzNDAzZWNkOTg0ZWNiOGExNThhMmJjN2IiXQ==");
      }
    }
  });
}
</script>
</div>
<div class="video_info_wrapper">
<h1 class="heading">2012 X-Games 18 motocross best trick</h1>
<div class="voting-module video-watch">
<span class="icon-container">
<span class="icon icon1"></span>
<span class="icon icon2 hidden-vote"></span>
</span>
<span class="text-container">
<span class="text text1"></span>
<span class="text text2"></span>
</span>
</div>
<script>
var videoLikes = 0;

_likes(videoLikes, "-53425992_165397466", "f277e7e3");

logVideoId("-53425992_165397466", "f277e7e3");
</script>
<div class="video_fav" alt="To favorites" title="To favorites" onclick="Fav.Toggle(this, '-53425992_165397466', '6adab556e398e4602ff6ca8224d02364');"></div>
<div class="video_embed" title="Share" onclick="Embed.show(-53425992, 165397466);"></div>
<div class="main_share_panel">
<div id="addthis_sharing_toolbox">
<div class="share_btn share_btn_re"><span class="re_icon" onclick="return Share.reddit();"></span></div>
<div class="share_btn share_btn_fb"><span class="fb_icon" onclick="return Share.facebook();"></span></div>
<div class="share_btn share_btn_tw"><span class="tw_icon" onclick="return Share.twitter();"></span></div>
<div class="share_btn share_btn_vk"><span class="vk_icon" onclick="return Share.vkontakte();"></span></div>
</div>
</div>
<div class="clear"></div>
<div class="bordertop">
<br>Views: 62 <br>Published: 17 Jul at 04:03 PM </div></div>
<div>Duration: 28:50</div>
</div>
<div class="tabs_wrapper">
<div class="tabs">
<style>.tabs > div.tab-btn{width: 50%;}</style>
<div class="tab-btn tab-show" id="tab_similar">Related</div>
<div class="tab-btn" id="tab_comments">Comments</div>
</div>
</div>
<div class="rightbar tab_similar tab-result tab-show">
<span style="margin: 0px 13px;padding-bottom: 15px;" class="standalonead">
<iframe width="300" height="250" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10002729?time=1555363895445" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10002729"></iframe>
</span>
<div class="video-item video5817_163924401" data-video="WyJXaW50ZXIgWCBHYW1lcyAyMDEyIFtBc3Blbl0gLUZpbmFsIE1lbnMgU3VwZXJQaXBlIiw1MjQ0LDQzLCJodHRwczpcL1wvc3VuOS03My51c2VyYXBpLmNvbVwvYzUxOTYxMVwvdTA1ODE3XC92aWRlb1wvbF83YzRiNjI4Zi5qcGciLCJiNDI1NzY4NmJlYjIxYmE5MjU3NmM5M2NkY2E0YzQxYSJd" data-embedUrl="https://daxab.com/player/H35kySibxPfPsPpWrmHOzeaypwZwX4CzSEky5HSymFtpt42WCjhKinZ3nfAJHzX-CEiV6_29so3ZHZIkbETKNA" data-playerHash="H35kySibxPfPsPpWrmHOzeaypwZwX4CzSEky5HSymFtpt42WCjhKinZ3nfAJHzX-CEiV6_29so3ZHZIkbETKNA" data-favHash="24659d3a3191ece1b857645e1fc8a685">
<a href="/watch/5817_163924401" data-id="5817_163924401">
<div class="video-show"></div>
<div class="video-fav" data-hash="24659d3a3191ece1b857645e1fc8a685"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-73.userapi.com/c519611/u05817/video/l_7c4b628f.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">43</span>
<span class="video-time">01:27:24</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Winter X Games 2012 [Aspen] -Final Mens SuperPipe</div>
</a>
</div>
<div class="video-item video-4296410_167364583" data-video="WyJYLWdhbWVzIDIwMTIgYm14IHBhcmsgZmluYWwgKNC90LAg0YDRg9GB0YHQutC+0LwpIiw1MjkzLDM1LCJodHRwczpcL1wvc3VuOS01NS51c2VyYXBpLmNvbVwvYzUzNTUyMlwvdTk3MDA0ODdcL3ZpZGVvXC9sXzgwYzVjOThmLmpwZyIsIjc5YWY1NjFmYzFiYjQ5NmFhZjM5MzEzNzBkNjBkZTQ4Il0=" data-embedUrl="https://daxab.com/player/GzgJYCBrpnZ8lZigorXjm6b8at1VCN3rCpgLWZ3Wj9sRZQV1io8qz1lTEuvb5mjpwHagcR7fmErF_ni-oySFEg" data-playerHash="GzgJYCBrpnZ8lZigorXjm6b8at1VCN3rCpgLWZ3Wj9sRZQV1io8qz1lTEuvb5mjpwHagcR7fmErF_ni-oySFEg" data-favHash="24c1b3a842c081128ac6b11cf664db03">
<a href="/watch/-4296410_167364583" data-id="-4296410_167364583">
<div class="video-show"></div>
<div class="video-fav" data-hash="24c1b3a842c081128ac6b11cf664db03"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-55.userapi.com/c535522/u9700487/video/l_80c5c98f.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">8 years ago</span>
<span class="video-view">35</span>
<span class="video-time">01:28:13</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012 bmx park final (на русском)</div>
</a>
</div>
<div class="video-item video-20774214_163747625" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxKSBIRCIsNjY5MiwxNzUsImh0dHBzOlwvXC9zdW45LTgwLnVzZXJhcGkuY29tXC9jNTE0MjE4XC91MTE0MDIzMTJcL3ZpZGVvXC9sXzE3NWVhYzgxLmpwZyIsIjFmNTlmYmM5NTc0MjY4N2JhNmEwOGQ5ZWY3ZjUzMmZlIl0=" data-embedUrl="https://daxab.com/player/OqLfZobwoofubd30hrMb3an81ijEj7vqzDiinHfwPzw2_Od66BtRnIIcRzq1ms1fzgnNr3USH9Vbc7Tf1p7_QQ" data-playerHash="OqLfZobwoofubd30hrMb3an81ijEj7vqzDiinHfwPzw2_Od66BtRnIIcRzq1ms1fzgnNr3USH9Vbc7Tf1p7_QQ" data-favHash="50bb3585ce2c07e4f89225006b652011">
<a href="/watch/-20774214_163747625" data-id="-20774214_163747625">
<div class="video-show"></div>
<div class="video-fav" data-hash="50bb3585ce2c07e4f89225006b652011"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c514218/u11402312/video/l_175eac81.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">175</span>
<span class="video-time">01:51:32</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1) HD</div>
</a>
</div>
<div class="video-item video-127946280_456239036" data-video="WyJFcm9nZSEgSCBtbyBHYW1lIG1vIEthaWhhdHN1IFphbm1haSDQrdGA0L7Qs9C1ISAwMlwv0JfQsNCx0YvRgtGM0YHRjyDQsiDRgdC+0LfQtNCw0L3QuNC4INGF0LXQvdGC0LDQuSDQuNCz0YDRiyAoKzE4IEhlbnRhaSBIRCkiLDE4NDYsNDA0NSwiaHR0cHM6XC9cL3N1bjYtMjIudXNlcmFwaS5jb21cL2M2MjY2MTlcL3Y2MjY2MTkyODBcLzI2MTExXC9TSmhKdExNNDBYNC5qcGciLCJhNmI1NmM0M2NkNmQxNTJiMzljYWExMzNhZmQzNGUwZSJd" data-embedUrl="https://daxab.com/player/WN55Jh7WRptH-usbqmfN9CYvwdVXQP1-L83B8-BIUv-9yfhF5NmGbXv__F9PFU4dubBcyxnUL3Zu26OWpV7CCHFLe0azcmqeFX9ex5hUZ3A" data-playerHash="WN55Jh7WRptH-usbqmfN9CYvwdVXQP1-L83B8-BIUv-9yfhF5NmGbXv__F9PFU4dubBcyxnUL3Zu26OWpV7CCHFLe0azcmqeFX9ex5hUZ3A" data-favHash="3a7c012b7e3a9ade5274c30b1f34afa5">
<a href="/watch/-127946280_456239036" data-id="-127946280_456239036">
<div class="video-show"></div>
<div class="video-fav" data-hash="3a7c012b7e3a9ade5274c30b1f34afa5"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c626619/v626619280/26111/SJhJtLM40X4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">5 years ago</span>
<span class="video-view">4k</span>
<span class="video-time">30:46</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Eroge! H mo Game mo Kaihatsu Zanmai Эроге! 02/Забыться в создании хентай игры (+18 Hentai HD)</div>
</a>
</div>
<div class="video-item video-20774214_163586566" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxKSIsNjc4NCwyMDQsImh0dHBzOlwvXC9zdW45LTg2LnVzZXJhcGkuY29tXC9jNTMyMzE1XC91OTcyODAxMVwvdmlkZW9cL2xfNTYzNzMyMmMuanBnIiwiYWQxZjRiYzE1ODQ0M2VlODE2MDVlZjcyNTE0YzczMDIiXQ==" data-embedUrl="https://daxab.com/player/5lgEdH0ZM6u3lcy96l6IBgJmUE9ABtV_lwWy4NatO8_AJxjQDPAA29GdDQ6rcs4fp7wGH78CzKSipBli2G2rrw" data-playerHash="5lgEdH0ZM6u3lcy96l6IBgJmUE9ABtV_lwWy4NatO8_AJxjQDPAA29GdDQ6rcs4fp7wGH78CzKSipBli2G2rrw" data-favHash="255bdad6a740955e40ef7e3cc4be92d1">
<a href="/watch/-20774214_163586566" data-id="-20774214_163586566">
<div class="video-show"></div>
<div class="video-fav" data-hash="255bdad6a740955e40ef7e3cc4be92d1"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-86.userapi.com/c532315/u9728011/video/l_5637322c.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">204</span>
<span class="video-time">01:53:04</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1)</div>
</a>
</div>
<div class="video-item video-147765253_456239519" data-video="WyJHeW1uYXN0aWNzIC0gQXJ0aXN0aWMgLSBNZW4mIzM5O3MgVGVhbSBGaW5hbCB8IExvbmRvbiAyMDEyIE9seW1waWMgR2FtZXMiLDExNjQwLDEsImh0dHBzOlwvXC9zdW42LTIwLnVzZXJhcGkuY29tXC93Y2t5eHgwLTdCQjlFa05BRDJYbVpfeDN1ZjZVemUtcnVxVl94d1wvYXZpSDc4Vk1OZEkuanBnIiwiN2FmNjY0MDdiNDQwMGM3NTc2NWRmYTYzZGJmZWEwMTQiXQ==" data-embedUrl="https://daxab.com/player/3apza-k_tAY8Qhg98MuhK1jq3SsTP7FSBpah4OfVqcMg2VTxiI4AH7n7budBY9L2f89LPCTmJ9z5b5BKo2eBZh7sKsjvP6040RijRGssu7E" data-playerHash="3apza-k_tAY8Qhg98MuhK1jq3SsTP7FSBpah4OfVqcMg2VTxiI4AH7n7budBY9L2f89LPCTmJ9z5b5BKo2eBZh7sKsjvP6040RijRGssu7E" data-favHash="21df7febeb200b63d0e7d35d0cdd5e05">
<a href="/watch/-147765253_456239519" data-id="-147765253_456239519">
<div class="video-show"></div>
<div class="video-fav" data-hash="21df7febeb200b63d0e7d35d0cdd5e05"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/wckyxx0-7BB9EkNAD2XmZ_x3uf6Uze-ruqV_xw/aviH78VMNdI.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 months ago</span>
<span class="video-view">1</span>
<span class="video-time">03:14:00</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Gymnastics - Artistic - Men&amp;#39;s Team Final | London 2012 Olympic Games</div>
</a>
</div>
<div class="video-item video-188576299_456239468" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAxIFs3MjBwXSIsMTg2OSwxOTEyLCJodHRwczpcL1wvc3VuNi0yMC51c2VyYXBpLmNvbVwvYzg1NzQyMFwvdjg1NzQyMDM5NVwvMjJhZTBmXC9xV0dMVjlycDgyRS5qcGciLCI2NzM2ZGM1NzgzOWRhYTFjMGI3M2YwN2UxZDhhM2ZkNSJd" data-embedUrl="https://daxab.com/player/Q4uF88OkOgX4Mc_sBny0kD47PtSkuzB92Gm_D2J2HOjaa04rnjDt5b06qOuF_kVh9Y0YAgt1GhqDSbO7jnaNS31oqT0Guc27mXw5NICS2aI" data-playerHash="Q4uF88OkOgX4Mc_sBny0kD47PtSkuzB92Gm_D2J2HOjaa04rnjDt5b06qOuF_kVh9Y0YAgt1GhqDSbO7jnaNS31oqT0Guc27mXw5NICS2aI" data-favHash="cc2ea25fe6368387cd0d23e5d82299d1">
<a href="/watch/-188576299_456239468" data-id="-188576299_456239468">
<div class="video-show"></div>
<div class="video-fav" data-hash="cc2ea25fe6368387cd0d23e5d82299d1"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/c857420/v857420395/22ae0f/qWGLV9rp82E.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,9k</span>
<span class="video-time">31:09</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 01 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239466" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAzIFs3MjBwXSIsMTgwNiwxNzMwLCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NTcyOFwvdjg1NTcyODM5NVwvMjRmMjY0XC9lcHl0TURrMFJkSS5qcGciLCI3Y2RiMDhjOTFhNmMwOWZiNjQ3YzBkNDhmZTllMzZlMyJd" data-embedUrl="https://daxab.com/player/EF1L_0qswWKPY28Y6H7ophXs_m-nzFZl82XY6ThD--yKHnFJEHnETx7IdhrLch8_izaehbU8K04CkfHTrFo3NQqEEqfK60zBC7nA0avPasI" data-playerHash="EF1L_0qswWKPY28Y6H7ophXs_m-nzFZl82XY6ThD--yKHnFJEHnETx7IdhrLch8_izaehbU8K04CkfHTrFo3NQqEEqfK60zBC7nA0avPasI" data-favHash="ac89a8b41ee3f0f4315343b2c6f6a9fa">
<a href="/watch/-188576299_456239466" data-id="-188576299_456239466">
<div class="video-show"></div>
<div class="video-fav" data-hash="ac89a8b41ee3f0f4315343b2c6f6a9fa"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c855728/v855728395/24f264/epytMDk0RdI.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,7k</span>
<span class="video-time">30:06</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 03 [720p]</div>
</a>
</div>
<div class="video-item video-20774214_163763516" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTQgKExpdmUgU2hvdyAyKSBIRCIsNjAzNCwxMzYsImh0dHBzOlwvXC9zdW45LTEzLnVzZXJhcGkuY29tXC9jNTI3NjEwXC91MTE0MDIzMTJcL3ZpZGVvXC9sXzg1MzBmODBiLmpwZyIsIjNjNjFjYzYyMDY2YmI5ZWJhM2RhNjZkNTU3Y2M5MWMyIl0=" data-embedUrl="https://daxab.com/player/0Lb_8uTYs3xkoGDe7l63q6Z7wZylKJV1Sm6xerm94j7-m8lovX4BDuDP5bAYXly3z2oqySKkFzNyHugquzdbLg" data-playerHash="0Lb_8uTYs3xkoGDe7l63q6Z7wZylKJV1Sm6xerm94j7-m8lovX4BDuDP5bAYXly3z2oqySKkFzNyHugquzdbLg" data-favHash="0938a89746b3a3d4fa400d6265b36a06">
<a href="/watch/-20774214_163763516" data-id="-20774214_163763516">
<div class="video-show"></div>
<div class="video-fav" data-hash="0938a89746b3a3d4fa400d6265b36a06"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-13.userapi.com/c527610/u11402312/video/l_8530f80b.jpg"></div>

<div class="video-play"></div>
 <div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">136</span>
<span class="video-time">01:40:34</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x14 (Live Show 2) HD</div>
</a>
</div>
<div class="video-item video-901607_162943067" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIE1hbmBzIE1vdG8gWCBFbmR1cm8gWCIsNDc5OSw4MCwiaHR0cHM6XC9cL3N1bjktNDkudXNlcmFwaS5jb21cL2M1Mjc1MDdcL3UzMDY5OTMxN1wvdmlkZW9cL2xfYTZmNDJkMDkuanBnIiwiYmNmN2I5NjA1ZmNmZDI3OTFhNzUzY2FkYjNjZDViYTAiXQ==" data-embedUrl="https://daxab.com/player/7Yk8M5o7-P6llvnWVLDJnFxMdQIuPgW7Y8TBc8ULDZg4GkdY26Kek1eqOzcMy4UziAvO0uXF97EzsWXnjy1ulA" data-playerHash="7Yk8M5o7-P6llvnWVLDJnFxMdQIuPgW7Y8TBc8ULDZg4GkdY26Kek1eqOzcMy4UziAvO0uXF97EzsWXnjy1ulA" data-favHash="c3778c5de8bcf23b7d197e025de7fd81">
<a href="/watch/-901607_162943067" data-id="-901607_162943067">
<div class="video-show"></div>
<div class="video-fav" data-hash="c3778c5de8bcf23b7d197e025de7fd81"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-49.userapi.com/c527507/u30699317/video/l_a6f42d09.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">80</span>
<span class="video-time">01:19:59</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Man`s Moto X Enduro X</div>
</a>
</div>
<div class="video-item video-188576299_456239467" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDA0IFs3MjBwXSIsMTgwNSwxMzgwLCJodHRwczpcL1wvc3VuNi0yMS51c2VyYXBpLmNvbVwvYzg1NTIzNlwvdjg1NTIzNjM5NVwvMjU0ODc1XC9VYUFDbnNmTUNGWS5qcGciLCI0ZTg2ZDE5M2I1ZWExZmQ0ODhkNGZlNWQ0ZGMzZTkzNCJd" data-embedUrl="https://daxab.com/player/aDjIZ5Fnurd6tA2qurRrhS0yIzrTWzIni1m45LurZ3QnqewD6urnAz4TotPycD32ZJ9SlUAMxWSq66kqugzjaYLgae3sw_z9mVnd_ywf7O8" data-playerHash="aDjIZ5Fnurd6tA2qurRrhS0yIzrTWzIni1m45LurZ3QnqewD6urnAz4TotPycD32ZJ9SlUAMxWSq66kqugzjaYLgae3sw_z9mVnd_ywf7O8" data-favHash="78bdcfd180ecfe4a5ad2e93a0d744515">
<a href="/watch/-188576299_456239467" data-id="-188576299_456239467">
<div class="video-show"></div>
<div class="video-fav" data-hash="78bdcfd180ecfe4a5ad2e93a0d744515"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-21.userapi.com/c855236/v855236395/254875/UaACnsfMCFY.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,4k</span>
<span class="video-time">30:05</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 04 [720p]</div>
</a>
</div>
<div class="video-item video-200135128_456239373" data-video="WyLQkNC80LXRgNC40LrQsNC90YHQutC40Lkg0LrQvtGI0LzQsNGAIC0gQWxhbiBXYWtlJiMzOTtzIEFtZXJpY2FuIE5pZ2h0bWFyZSAjMSIsMjUwMSwxLCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvVHp4NTZSQnR4YXpuUV81X2lrVFpmZmUzcmhmSWgwZWVhQ2dra1FcLy0tZWFOSkdrTnhnLmpwZyIsIjMzMzM4NzZkMTczMjkzMzU4NmVhYjQxYzBmOWZlNGJjIl0=" data-embedUrl="https://daxab.com/player/P1UpeGt1sQYKIFkN6-jwpXjg3qZKXZs2yuWplPwLAsUNotWS-iO5su7h2V1bbs5D4vYrrXlZ9dEVaVNlQTltLjTj1Uqbs4YdHHrUZAyzrOA" data-playerHash="P1UpeGt1sQYKIFkN6-jwpXjg3qZKXZs2yuWplPwLAsUNotWS-iO5su7h2V1bbs5D4vYrrXlZ9dEVaVNlQTltLjTj1Uqbs4YdHHrUZAyzrOA" data-favHash="268336d8383e223acd548ce94a4c1928">
<a href="/watch/-200135128_456239373" data-id="-200135128_456239373">
<div class="video-show"></div>
<div class="video-fav" data-hash="268336d8383e223acd548ce94a4c1928"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/Tzx56RBtxaznQ_5_ikTZffe3rhfIh0eeaCgkkQ/--eaNJGkNxg.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 months ago</span>
<span class="video-view">1</span>
<span class="video-time">41:41</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Американский кошмар - Alan Wake&amp;#39;s American Nightmare #1</div>
</a>
</div>
<div class="video-item video195091338_456239074" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIEJlc3QgVHJpY2siLDI0NTcsMiwiaHR0cHM6XC9cL3N1bjYtMjEudXNlcmFwaS5jb21cL2M2MzY5MjVcL3Y2MzY5MjUzMzhcLzJmMWRcLzlKbm9iQ1R0ZjlnLmpwZyIsImUwNWQwZmYwMWJiNDE5MWFhYzRkOWQwNDNkNjk1OWMyIl0=" data-embedUrl="https://daxab.com/player/29mIdKGH2pjdLz-RtI_Qf-hW_xbQAKziRkXbb8AKnnK0OXZefp6qKYe-9obho3iOdQh0Sd9r_qq43nrj4prpBQ" data-playerHash="29mIdKGH2pjdLz-RtI_Qf-hW_xbQAKziRkXbb8AKnnK0OXZefp6qKYe-9obho3iOdQh0Sd9r_qq43nrj4prpBQ" data-favHash="1393143e615e35dcd69cbdbe2d1e56f3">
<a href="/watch/195091338_456239074" data-id="195091338_456239074">
<div class="video-show"></div>
<div class="video-fav" data-hash="1393143e615e35dcd69cbdbe2d1e56f3"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-21.userapi.com/c636925/v636925338/2f1d/9JnobCTtf9g.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">6 years ago</span>
<span class="video-view">2</span>
<span class="video-time">40:57</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Best Trick</div>
</a>
</div>
<div class="video-item video-188576299_456239333" data-video="WyLQl9Cw0YHRgtC10L3Rh9C40LLQvi3QoNCw0YHQv9GD0YLQvdCw0Y8g0LTQtdCy0YPRiNC60LAg0KHRg9C60YPQvNC4IFRzdW5kZXJlIElucmFuIFNob3VqbyBTdWt1bWkgLSAwMiBbNzIwcF0iLDExNTEsNTY2LCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NzIxNlwvdjg1NzIxNjE1NFwvMWE5Yzc5XC9pemdpTm9pTmZLby5qcGciLCIxMTg1ZjdjMWFlNDQyZjc5OGJjM2RlMGUzODQ5NjM1OCJd" data-embedUrl="https://daxab.com/player/JGzGDLcJwtHhvWvTuMuToBnETTdPRrFZlrN1MzyEV1SltNGH0e2RjhvQq11lOR5r17gsHGXKHdFcPP67ZIo-F5fnEnWqnjkQH5uKiFg-tjA" data-playerHash="JGzGDLcJwtHhvWvTuMuToBnETTdPRrFZlrN1MzyEV1SltNGH0e2RjhvQq11lOR5r17gsHGXKHdFcPP67ZIo-F5fnEnWqnjkQH5uKiFg-tjA" data-favHash="1059f8f2c8602da44c308a4c7842b98c">
<a href="/watch/-188576299_456239333" data-id="-188576299_456239333">
<div class="video-show"></div>
<div class="video-fav" data-hash="1059f8f2c8602da44c308a4c7842b98c"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c857216/v857216154/1a9c79/izgiNoiNfKo.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">566</span>
<span class="video-time">19:11</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Застенчиво-Распутная девушка Сукуми Tsundere Inran Shoujo Sukumi - 02 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239465" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAyIFs3MjBwXSIsMTgwNiwxMjAyLCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NTYyMFwvdjg1NTYyMDM5NVwvMjQ5ZDg3XC9CcGI5Q2tBUWFBZy5qcGciLCIzNjYyZDFjMWE5NjQ5OTkzZGExMGI3NWYyOTc0MGRjNyJd" data-embedUrl="https://daxab.com/player/rz15soca6PAfiNSfsu8g-Zy3V2KIaR_YnGFr5oK1O1LXUjkT6KLcrEPCLUANow_y_6nAijB9f1HfCSEc4X9vafKDmfMrWib5qM2vQZ8qEA8" data-playerHash="rz15soca6PAfiNSfsu8g-Zy3V2KIaR_YnGFr5oK1O1LXUjkT6KLcrEPCLUANow_y_6nAijB9f1HfCSEc4X9vafKDmfMrWib5qM2vQZ8qEA8" data-favHash="ff491e850a72875ffb536f7e4fe9c880">
<a href="/watch/-188576299_456239465" data-id="-188576299_456239465">
<div class="video-show"></div>
<div class="video-fav" data-hash="ff491e850a72875ffb536f7e4fe9c880"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c855620/v855620395/249d87/Bpb9CkAQaAg.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,2k</span>
<span class="video-time">30:06</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 02 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239332" data-video="WyLQl9Cw0YHRgtC10L3Rh9C40LLQvi3QoNCw0YHQv9GD0YLQvdCw0Y8g0LTQtdCy0YPRiNC60LAg0KHRg9C60YPQvNC4IFRzdW5kZXJlIElucmFuIFNob3VqbyBTdWt1bWkgLSAwMSBbNzIwcF0iLDEwODIsOTE4LCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvVVJDWFp3aHhjT285TVR2QWtzWjZpUTd6TXFZXzR5alZiWFByandcL3ZIQnB6RFRmLVNjLmpwZyIsIjY1MTEyODU3MmUzNTkzMGRhYjEyYmQ2MWRkNWNiMWMyIl0=" data-embedUrl="https://daxab.com/player/izB26dpK5eMjQHtHWd_jwaqZyjNW7KQ-hViOOMr_YNA8hm4gUO984YXcVDLzJAcMLJyPDxuXoGxubsbF4F0Qq-sIU-TEBfxoh-d_65EJwc0" data-playerHash="izB26dpK5eMjQHtHWd_jwaqZyjNW7KQ-hViOOMr_YNA8hm4gUO984YXcVDLzJAcMLJyPDxuXoGxubsbF4F0Qq-sIU-TEBfxoh-d_65EJwc0" data-favHash="a3947601bc8cd944b48dba7d4c2d585a">
<a href="/watch/-188576299_456239332" data-id="-188576299_456239332">
<div class="video-show"></div>
<div class="video-fav" data-hash="a3947601bc8cd944b48dba7d4c2d585a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/URCXZwhxcOo9MTvAksZ6iQ7zMqY_4yjVbXPrjw/vHBpzDTf-Sc.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">918</span>
<span class="video-time">18:02</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Застенчиво-Распутная девушка Сукуми Tsundere Inran Shoujo Sukumi - 01 [720p]</div>
</a>
</div>
<div class="video-item video-901607_162904584" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIFN0ZXAgVXAiLDMyMzAsNzgsImh0dHBzOlwvXC9zdW45LTY3LnVzZXJhcGkuY29tXC9jNTI2NjE2XC91MzA2OTkzMTdcL3ZpZGVvXC9sXzI0NTEyZWQ0LmpwZyIsIjVhYjZhYjY2MzllMGFiYjI4Y2FkMTA1NzlkNGU0Y2Y4Il0=" data-embedUrl="https://daxab.com/player/l9MrNghHqVDkPfvu8GnMyVAPL9BHOjO018-cLqTSzilyY207-OaMCSqAzyWBeNdaKpukWjb3KzXEzUJCB_ak0Q" data-playerHash="l9MrNghHqVDkPfvu8GnMyVAPL9BHOjO018-cLqTSzilyY207-OaMCSqAzyWBeNdaKpukWjb3KzXEzUJCB_ak0Q" data-favHash="7c5221b338e8f47aef78cdfd04b38762">
<a href="/watch/-901607_162904584" data-id="-901607_162904584">
<div class="video-show"></div>
<div class="video-fav" data-hash="7c5221b338e8f47aef78cdfd04b38762"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-67.userapi.com/c526616/u30699317/video/l_24512ed4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">78</span>
<span class="video-time">53:50</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Step Up</div>
</a>
</div>
<div class="video-item video-150082602_456239164" data-video="WyLQn9GD0YLQtdGI0LXRgdGC0LLQuNC1INCa0LDRgdGB0LDQvdC00YDRiyAyLiDQmtC+0L3QtdGGINGB0LLQtdGC0LAgMjAxMi4gIyAxIC0g0J/QvtC40YHQutC4INC90LDRh9C40L3QsNGO0YLRgdGPIiwxODY3LDEsImh0dHBzOlwvXC9zdW42LTIwLnVzZXJhcGkuY29tXC9jODMwMzA4XC92ODMwMzA4MTI1XC8xN2E1OWRcL1JUV1Rtb2QxOEJ3LmpwZyIsImFkNDI4MTQyOGMzMmEzZTdkMjRmZTczNjhlOTc5YzExIl0=" data-embedUrl="https://daxab.com/player/MYyNlYiwf4WMLNKUyskErZwf5LCsWlg4_uhOtTIgli2LtLZktN5rvMHn1b3csyLURj7AfVFpj2yhdPilaCwz9wYMGWHBuba6Gax3QKuoEXs" data-playerHash="MYyNlYiwf4WMLNKUyskErZwf5LCsWlg4_uhOtTIgli2LtLZktN5rvMHn1b3csyLURj7AfVFpj2yhdPilaCwz9wYMGWHBuba6Gax3QKuoEXs" data-favHash="f3e0f335d61cf72c6f3c24bb83dcaaaf">
<a href="/watch/-150082602_456239164" data-id="-150082602_456239164">
<div class="video-show"></div>
<div class="video-fav" data-hash="f3e0f335d61cf72c6f3c24bb83dcaaaf"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/c830308/v830308125/17a59d/RTWTmod18Bw.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">3 years ago</span>
<span class="video-view">1</span>
<span class="video-time">31:07</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Путешествие Кассандры 2. Конец света 2012. # 1 - Поиски начинаются</div>
</a>
</div>
<div class="video-item video-20774214_163637013" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTQgKExpdmUgU2hvdyAyKSIsNjA1OCwxMzcsImh0dHBzOlwvXC9zdW45LTgwLnVzZXJhcGkuY29tXC9jNTI3MzE3XC91OTcyODAxMVwvdmlkZW9cL2xfMjZlOWZhMmEuanBnIiwiNzM1NDA1ZjA5YjcwMTcyMDI3M2U3MDRiNDU5NjJhZTAiXQ==" data-embedUrl="https://daxab.com/player/qaIVOBJi1jWD1vr6XoeiQpIor0csK4VE7anGcBG4YbZmuJjCDviEcDRXR6xzfc7sZfMpwL3GORJjl8CwRDbeiA" data-playerHash="qaIVOBJi1jWD1vr6XoeiQpIor0csK4VE7anGcBG4YbZmuJjCDviEcDRXR6xzfc7sZfMpwL3GORJjl8CwRDbeiA" data-favHash="d462fb2019aed8ca25f27225aac6c177">
<a href="/watch/-20774214_163637013" data-id="-20774214_163637013">
<div class="video-show"></div>
<div class="video-fav" data-hash="d462fb2019aed8ca25f27225aac6c177"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c527317/u9728011/video/l_26e9fa2a.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">137</span>
<span class="video-time">01:40:58</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x14 (Live Show 2)</div>
</a>
</div>
<div class="video-item video-19767527_165512903" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gRm91cnRoIGRheS4gQk1YIEJpZyBBaXIiLDI5NTUsNDcsImh0dHBzOlwvXC9zdW45LTEwLnVzZXJhcGkuY29tXC9jNTA2MTE4XC91ODE5ODkyMDhcL3ZpZGVvXC9sX2VkZmYxMGU4LmpwZyIsIjQ1ZWExY2M0YWQ1MDNkNTQ4ZDI1ZWY3ODk4MTVkODJkIl0=" data-embedUrl="https://daxab.com/player/VYDcI7rPUrk4exBObbm_ol3FUqoQ0w_8JRGDMLS4PdXCxcJ0d_sCc-j8uQnY2zbv7SQ4bYkabxXcBlXsDWcTVA" data-playerHash="VYDcI7rPUrk4exBObbm_ol3FUqoQ0w_8JRGDMLS4PdXCxcJ0d_sCc-j8uQnY2zbv7SQ4bYkabxXcBlXsDWcTVA" data-favHash="dde051d566857522cb8860d118466d73">
<a href="/watch/-19767527_165512903" data-id="-19767527_165512903">
<div class="video-show"></div>
<div class="video-fav" data-hash="dde051d566857522cb8860d118466d73"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-10.userapi.com/c506118/u81989208/video/l_edff10e8.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">47</span>
<span class="video-time">49:15</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Fourth day. BMX Big Air</div>
</a>
</div>
<div class="video-item video-53908459_456239661" data-video="WyJYLdC40LPRgNCwIDIgXC8gWCBHYW1lIDIiLDYxOTUsODEsImh0dHBzOlwvXC9zdW42LTIyLnVzZXJhcGkuY29tXC9jNjM2MjE4XC92NjM2MjE4NDU5XC80ODY0M1wvazFoWEpFWk1SNzQuanBnIiwiYjYzN2IzNzFmNjIxNTUxMThlMzYxZWI2NTE1OGNmMTMiXQ==" data-embedUrl="https://daxab.com/player/qnpIGdnPPnq8rCb0r_pOVCJpALEb31gteC6v3ZXQWomZRd7hxrdD8YjqVSLt8GcSdRzOEtYO62SCENnZVAANfQ" data-playerHash="qnpIGdnPPnq8rCb0r_pOVCJpALEb31gteC6v3ZXQWomZRd7hxrdD8YjqVSLt8GcSdRzOEtYO62SCENnZVAANfQ" data-favHash="fcc330c464b617c167c9bfb94242a787">
<a href="/watch/-53908459_456239661" data-id="-53908459_456239661">
<div class="video-show"></div>
<div class="video-fav" data-hash="fcc330c464b617c167c9bfb94242a787"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c636218/v636218459/48643/k1hXJEZMR74.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">5 years ago</span>
<span class="video-view">81</span>
<span class="video-time">01:43:15</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-игра 2 / X Game 2</div>
</a>
</div>
<div class="video-item video-901607_162940653" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIE1vdG8gRnJlZXN0eWxlIiwzNzg1LDcwLCJodHRwczpcL1wvc3VuOS00NS51c2VyYXBpLmNvbVwvYzUxODYxNlwvdTMwNjk5MzE3XC92aWRlb1wvbF82MmE2MTVmOS5qcGciLCI4NWYzNjI0MjMxYjJkOTVkZTE1OGQ4MjM5Yzg5OWUxYiJd" data-embedUrl="https://daxab.com/player/YmnadgArmYuW45_EaNUpz6EcQ9MIlhwrobbD1tI1Nr4s-uzuv9kQLwDVw6lIM8ulwOpxLh76XUml1n91dsZxtg" data-playerHash="YmnadgArmYuW45_EaNUpz6EcQ9MIlhwrobbD1tI1Nr4s-uzuv9kQLwDVw6lIM8ulwOpxLh76XUml1n91dsZxtg" data-favHash="fccdce282eb9ecdf8bd54f3045922e3c">
<a href="/watch/-901607_162940653" data-id="-901607_162940653">
<div class="video-show"></div>
<div class="video-fav" data-hash="fccdce282eb9ecdf8bd54f3045922e3c"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-45.userapi.com/c518616/u30699317/video/l_62a615f9.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">70</span>
<span class="video-time">01:03:05</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Moto Freestyle</div>
</a>
</div>
<div class="video-item video-20774214_163592952" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxXC8gUGFydCAxKSBIRCIsMzM3OSw3MywiaHR0cHM6XC9cL3N1bjktNTQudXNlcmFwaS5jb21cL2M1MjkyMDJcL3UzMDg0OTU1XC92aWRlb1wvbF85Mjc0NDRkMC5qcGciLCJjM2ZmZGZhY2RhNDU5OGJiOTc1MTYwNzc3YTU4ZWRmMSJd" data-embedUrl="https://daxab.com/player/yDgGkLnQ6A-9S-O2J4yVCy8oSnJX9aTw26Rz4OSJo32Vd_0f5WQriLsi_b5Q8IPgM2SfnmGd_pXDxwuawRkeag" data-playerHash="yDgGkLnQ6A-9S-O2J4yVCy8oSnJX9aTw26Rz4OSJo32Vd_0f5WQriLsi_b5Q8IPgM2SfnmGd_pXDxwuawRkeag" data-favHash="3576c9c62c9f70156112ddb8ea7f498e">
<a href="/watch/-20774214_163592952" data-id="-20774214_163592952">
<div class="video-show"></div>
<div class="video-fav" data-hash="3576c9c62c9f70156112ddb8ea7f498e"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-54.userapi.com/c529202/u3084955/video/l_927444d0.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">73</span>
<span class="video-time">56:19</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1/ Part 1) HD</div>
</a>
</div>
<div class="video-item video-190464408_456239270" data-video="WyJTbGVlcGluZyBEb2dzIOKeqCDQn9GA0L7RhdC+0LbQtNC10L3QuNC1IDEyIOKeqCDQntC00L7Qu9C20LXQvdC40Y8g4oSWIDIgI1NsZWVwaW5nRG9ncyAjU3RhcnBlcml5YSIsMjU3NiwxLCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvNzBJZ0FONW1tclFjajFjR0FEZ2RaZkNPSkhVejJPeHFDdVJmUWdcL2w4Y0JfTXpwcVJFLmpwZyIsIjg5M2Y3YmJlMWQ3MjBmNDdjNzRmODg0NGQ0ZjI1NDhlIl0=" data-embedUrl="https://daxab.com/player/JB0LNWKCfaZJ50TdUoxom9477IN1gWiq2r5pYuaYCchEQIdQ8WFRwibKxpKpZPau5b2AgLRF_r8cjHpUpPiKg5I7T8pLMbnwS18ytHIsNAU" data-playerHash="JB0LNWKCfaZJ50TdUoxom9477IN1gWiq2r5pYuaYCchEQIdQ8WFRwibKxpKpZPau5b2AgLRF_r8cjHpUpPiKg5I7T8pLMbnwS18ytHIsNAU" data-favHash="dfaa105378c66240f1bf25aa8494588d">
<a href="/watch/-190464408_456239270" data-id="-190464408_456239270">
<div class="video-show"></div>
<div class="video-fav" data-hash="dfaa105378c66240f1bf25aa8494588d"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/70IgAN5mmrQcj1cGADgdZfCOJHUz2OxqCuRfQg/l8cB_MzpqRE.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1</span>
<span class="video-time">42:56</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Sleeping Dogs ➨ Прохождение 12 ➨ Одолжения № 2 #SleepingDogs #Starperiya</div>
</a>
</div>
<div class="video-item video384329_164980719" data-video="WyJYLWdhbWVzLCBBc2lhLCBWZXJ0LCAyMDEyIHllYXIiLDI4MzQsMTUsImh0dHBzOlwvXC9zdW45LTIyLnVzZXJhcGkuY29tXC9jNTE4MjEzXC91Mzg0MzI5XC92aWRlb1wvbF82MjEyODI1ZC5qcGciLCJlNzMzZGY4NTdhMGE3ZTczN2FmN2Y1ZDAwMDA4MGI2MCJd" data-embedUrl="https://daxab.com/player/RDq2IAtRMEvfieQl3-BfE4yjPN_HI4-bl5hxGVTeckZ6DseBTvLCz1Z8PfnD1R-ku_gZby-8j1AdT5RpUyM4tw" data-playerHash="RDq2IAtRMEvfieQl3-BfE4yjPN_HI4-bl5hxGVTeckZ6DseBTvLCz1Z8PfnD1R-ku_gZby-8j1AdT5RpUyM4tw" data-favHash="e5a881faca6da5f106ae9f0095b97d3a">
<a href="/watch/384329_164980719" data-id="384329_164980719">
<div class="video-show"></div>
<div class="video-fav" data-hash="e5a881faca6da5f106ae9f0095b97d3a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-22.userapi.com/c518213/u384329/video/l_6212825d.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">15</span>
<span class="video-time">47:14</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games, Asia, Vert, 2012 year</div>
</a>
</div>
<div class="video-item video-12783167_170084732" data-video="WyJYLUdhbWVzIDIwMTIgUmFsbHktQ3Jvc3MiLDU3MDAsMzYsImh0dHBzOlwvXC9zdW45LTM1LnVzZXJhcGkuY29tXC9jNTEzMzEyXC91MTQzOTcyOTQ5XC92aWRlb1wvbF8xMmM1MzFmNi5qcGciLCI5MDQ1MDMyMmY4MjA1OWQzMmNhNjFmNWFkMDUzNzMyYiJd" data-embedUrl="https://daxab.com/player/AG5grUkgYW6m6k4xaLEMT6ltkQc3VvhJgtgAHVsjgXAF-2G3DvGQ7525QhuLKwrn5S1AVWWHLuC4xsdz3PNAew" data-playerHash="AG5grUkgYW6m6k4xaLEMT6ltkQc3VvhJgtgAHVsjgXAF-2G3DvGQ7525QhuLKwrn5S1AVWWHLuC4xsdz3PNAew" data-favHash="a6401941b889bf3e78bc1df3df07fa72">
<a href="/watch/-12783167_170084732" data-id="-12783167_170084732">
<div class="video-show"></div>
<div class="video-fav" data-hash="a6401941b889bf3e78bc1df3df07fa72"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-35.userapi.com/c513312/u143972949/video/l_12c531f6.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 years ago</span>
<span class="video-view">36</span>
<span class="video-time">01:35:00</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games 2012 Rally-Cross</div>
</a>
</div>
<div class="video-item video-19767527_165206109" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gRmlyc3QiLDMxNDYsMjgsImh0dHBzOlwvXC9zdW45LTgyLnVzZXJhcGkuY29tXC9jNTMyMzA0XC91ODE5ODkyMDhcL3ZpZGVvXC9sXzYwNWY0ZmQ0LmpwZyIsIjdkZmU1MDE3MjJhNzMzZGFiZGZmZjMyM2M2Njk3NDUwIl0=" data-embedUrl="https://daxab.com/player/8dkUvv3TIb5FO9cdqWUk0zRyffrTaTlkVbGj4-G9bzhUKUH0dI1LHhJNe_7AFWYElv2skktSuxDRuVijEfrR_A" data-playerHash="8dkUvv3TIb5FO9cdqWUk0zRyffrTaTlkVbGj4-G9bzhUKUH0dI1LHhJNe_7AFWYElv2skktSuxDRuVijEfrR_A" data-favHash="cb12f1c853d023943344fa576e1d169a">
<a href="/watch/-19767527_165206109" data-id="-19767527_165206109">
<div class="video-show"></div>
<div class="video-fav" data-hash="cb12f1c853d023943344fa576e1d169a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-82.userapi.com/c532304/u81989208/video/l_605f4fd4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">28</span>
<span class="video-time">52:26</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. First</div>
</a>
</div>
<div class="video-item video-19767527_165214922" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gVGhyaWQgZGF5LiBCTVggU3RyZWV0IEZpbmFsIiwxMjcwLDExLCJodHRwczpcL1wvc3VuOS00Ni51c2VyYXBpLmNvbVwvYzUzNTUxMlwvdTgxOTg5MjA4XC92aWRlb1wvbF82NmRkZGZmYy5qcGciLCI5MGU1YjNiZGU4MjNhMjhkNTFlMzRiOGU3NmU2MzYzNiJd" data-embedUrl="https://daxab.com/player/cvuYoILEELr5xSsMYq9b1NeLs3ubFFwgKim9Z9R_xmRuuF4YGgDD3_0p-SxQ2SGUC7HgORZs5-0iArnMnZdLVA" data-playerHash="cvuYoILEELr5xSsMYq9b1NeLs3ubFFwgKim9Z9R_xmRuuF4YGgDD3_0p-SxQ2SGUC7HgORZs5-0iArnMnZdLVA" data-favHash="a89d88f3a3ec1b0d933a35869c8a565a">
<a href="/watch/-19767527_165214922" data-id="-19767527_165214922">
<div class="video-show"></div>
<div class="video-fav" data-hash="a89d88f3a3ec1b0d933a35869c8a565a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-46.userapi.com/c535512/u81989208/video/l_66dddffc.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">11</span>
<span class="video-time">21:10</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Thrid day. BMX Street Final</div>
</a>
</div>
<div class="video-item video-901607_162943115" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIFJldmlldyIsNTM4MSwxNiwiaHR0cHM6XC9cL3N1bjktODcudXNlcmFwaS5jb21cL2M1MjcyMTZcL3UzMDY5OTMxN1wvdmlkZW9cL2xfZDhiOTA2ZjQuanBnIiwiM2MyY2QzNDFhMTM4ODlkNjcxNjJmNmFlMTU5ZDBlMTIiXQ==" data-embedUrl="https://daxab.com/player/-ze8ANRY8SkrkcyM6u4Co7py9SOVca6R3nFQkn7I3JSVVKWacOx599wEtiBSB-qXR-K2pVZzS6y9cNboqk1xzg" data-playerHash="-ze8ANRY8SkrkcyM6u4Co7py9SOVca6R3nFQkn7I3JSVVKWacOx599wEtiBSB-qXR-K2pVZzS6y9cNboqk1xzg" data-favHash="f9d2d9c63926cc6aedd852f38b190488">
<a href="/watch/-901607_162943115" data-id="-901607_162943115">
<div class="video-show"></div>
<div class="video-fav" data-hash="f9d2d9c63926cc6aedd852f38b190488"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-87.userapi.com/c527216/u30699317/video/l_d8b906f4.jpg"></div>
 
<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">16</span>
<span class="video-time">01:29:41</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Review</div>
</a>
</div>
<div class="video-item video-19767527_165214946" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gVGhyaWQgZGF5LiBCTVggVmVydCIsOTkzLDg0LCJodHRwczpcL1wvc3VuOS03OS51c2VyYXBpLmNvbVwvYzUxNDUwNFwvdTgxOTg5MjA4XC92aWRlb1wvbF9jZDk1NjI3Ny5qcGciLCIwZjVhMDQ4NzkwYTM2NWYxYzNlNzUyNWI3NGIxYjQ1ZCJd" data-embedUrl="https://daxab.com/player/E74Q-wC5tCZIWKCeWXU16ey1YMi5rTXXE8qPnHoW_pwCZg7wuPh7oVqr2B9nJxabdkFEN7gWXPHxkCvnhFu0KA" data-playerHash="E74Q-wC5tCZIWKCeWXU16ey1YMi5rTXXE8qPnHoW_pwCZg7wuPh7oVqr2B9nJxabdkFEN7gWXPHxkCvnhFu0KA" data-favHash="8af51ac89ba5920efcbea5001540648a">
<a href="/watch/-19767527_165214946" data-id="-19767527_165214946">
<div class="video-show"></div>
<div class="video-fav" data-hash="8af51ac89ba5920efcbea5001540648a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-79.userapi.com/c514504/u81989208/video/l_cd956277.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">84</span>
<span class="video-time">16:33</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Thrid day. BMX Vert</div>
</a>
</div>
<div class="video-item video581608605_456239155" data-video="WyJSb2NrIHRoZSBWb3RlISAtIC0gUHJvcGFnYW5kYVdhdGNoICggMTQ0IFggMTQ0ICkubXA0IiwxMzE1LDE4NCwiaHR0cHM6XC9cL3N1bjYtMjMudXNlcmFwaS5jb21cLzFwdVZmMjRaVlMyd0VvNldVb3lJdV9EM2hQY1I1S1BoMWd0Uk93XC9WY2txVzB6WEVlUS5qcGciLCI3OGJlNzFlOTdmZWU0ODgwZmUxNzE5NWM1YTkxMGRjYyJd" data-embedUrl="https://daxab.com/player/YgLknCSkVMC4SOJu_8rLyV-nq7BNF180jM6ELt4nkmS6TGYC_Zwl7wyt-GB8isd_p1FgGxZdcPWGOfvK7dTjGA" data-playerHash="YgLknCSkVMC4SOJu_8rLyV-nq7BNF180jM6ELt4nkmS6TGYC_Zwl7wyt-GB8isd_p1FgGxZdcPWGOfvK7dTjGA" data-favHash="77b9ed060e5f68bd61219753639c34cf">
<a href="/watch/581608605_456239155" data-id="581608605_456239155">
<div class="video-show"></div>
<div class="video-fav" data-hash="77b9ed060e5f68bd61219753639c34cf"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/1puVf24ZVS2wEo6WUoyIu_D3hPcR5KPh1gtROw/VckqW0zXEeQ.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">184</span>
<span class="video-time">21:55</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Rock the Vote! - - PropagandaWatch ( 144 X 144 ).mp4</div>
</a>
</div>
<div class="video-item video-20774214_163592955" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxXC8gUGFydCAyKSBIRCIsMzMxMyw0MCwiaHR0cHM6XC9cL3N1bjktNjEudXNlcmFwaS5jb21cL2M1MTg2MDJcL3UzMDg0OTU1XC92aWRlb1wvbF83MmE0MTdiMi5qcGciLCI2MTM1NjNhMzdkZTY3NThjY2FiYzk2Njc5NzljZDg0OSJd" data-embedUrl="https://daxab.com/player/BVZmX_ynwdHDiLeefBrsT-6zMQmm4qNPkIMb0zCmlkZlcezSnXT4fASJ8OE0bPZcGuytv9L8W6GfwdsHCEXQYA" data-playerHash="BVZmX_ynwdHDiLeefBrsT-6zMQmm4qNPkIMb0zCmlkZlcezSnXT4fASJ8OE0bPZcGuytv9L8W6GfwdsHCEXQYA" data-favHash="d8daed6fac8e218db9f4e78de3e73b29">
<a href="/watch/-20774214_163592955" data-id="-20774214_163592955">
<div class="video-show"></div>
<div class="video-fav" data-hash="d8daed6fac8e218db9f4e78de3e73b29"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-61.userapi.com/c518602/u3084955/video/l_72a417b2.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">40</span>
<span class="video-time">55:13</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1/ Part 2) HD</div>
</a>
</div>
<div class="video-item video-4296410_167364578" data-video="WyJYLWdhbWVzIDIwMTIgYm14IHZlcnQgZmluYWwgKCDQvdCwINGA0YPRgdGB0LrQvtC8KSIsOTkzLDYyLCJodHRwczpcL1wvc3VuOS0zMy51c2VyYXBpLmNvbVwvYzUyMzYwMFwvdTk3MDA0ODdcL3ZpZGVvXC9sX2NkYmZhMWUyLmpwZyIsIjE5NmVhYWE3NzE2MDhiYmJkYzc4ZjE5YTg3MDk5NDVjIl0=" data-embedUrl="https://daxab.com/player/HmYW1eCazTK9R3qsOOLBSkrx68zfHJKoezWSFlMGnpeGXPstPuGhcpppEASrQW1rLmgEzA-Cu6IY3HbAAYbwCg" data-playerHash="HmYW1eCazTK9R3qsOOLBSkrx68zfHJKoezWSFlMGnpeGXPstPuGhcpppEASrQW1rLmgEzA-Cu6IY3HbAAYbwCg" data-favHash="ed545ebb89984148892cfa8ee7c45ef8">
<a href="/watch/-4296410_167364578" data-id="-4296410_167364578">
<div class="video-show"></div>
<div class="video-fav" data-hash="ed545ebb89984148892cfa8ee7c45ef8"></div>
 <div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-33.userapi.com/c523600/u9700487/video/l_cdbfa1e2.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">8 years ago</span>
<span class="video-view">62</span>
<span class="video-time">16:33</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012 bmx vert final ( на русском)</div>
</a>
</div>
<div class="video-item video-145081708_456242402" data-video="WyLQuNC1IEJvcmRlcmxhbmRzIEdPVFkgWzIwMDldIFBDXC9QUzNcL1hib3gzNjAuINCf0YDQvtGF0L7QttC00LXQvdC40LUg0LfQsCDQm9C40LvQuNGCINGHLjggKzE4IFtkYXIiLDI2NDcsMTE4LCJodHRwczpcL1wvaS5teWNkbi5tZVwvZ2V0VmlkZW9QcmV2aWV3P2lkPTEzMjIzNzY4MjU1NjMmaWR4PTEmdHlwZT0zOSZ0a249U3VPTUVURDh2R1dRMTNIaHdhdzI0WmhoX0xRJmZuPXZpZF9sIiwiZmRkNjMwM2Y3YTczNzUxYjlkZGQzZGFiYjE5NGEwNzYiXQ==" data-embedUrl="https://daxab.com/player/TjXbUgND0QosN-MkxsdMTvR_rdYLNw7KIjbiZl5T40M4HUqMXZQrcnmBPX2qTWMRzq3cltDfcAbUKvOwqgT-rw9BBWzCZntm8Zu4O8RezO0" data-playerHash="TjXbUgND0QosN-MkxsdMTvR_rdYLNw7KIjbiZl5T40M4HUqMXZQrcnmBPX2qTWMRzq3cltDfcAbUKvOwqgT-rw9BBWzCZntm8Zu4O8RezO0" data-favHash="697a4941bacc445326c3e7e84ee48753">
<a href="/watch/-145081708_456242402" data-id="-145081708_456242402">
<div class="video-show"></div>
<div class="video-fav" data-hash="697a4941bacc445326c3e7e84ee48753"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://i.mycdn.me/getVideoPreview?id=1322376825563&idx=1&type=39&tkn=SuOMETD8vGWQ13Hhwaw24Zhh_LQ&fn=vid_l"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">118</span>
<span class="video-time">44:07</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">ие Borderlands GOTY [2009] PC/PS3/Xbox360. Прохождение за Лилит ч.8 +18 [dar</div>
</a>
</div>
<div class="video-item video104849236_456277453" data-video="WyJNQVggUEFZTkUgMyBOUEMgV2FycyAyMyAoVUZFIHZzIENyYWNoYSBQcmV0bykiLDE4ODMsMSwiaHR0cHM6XC9cL3N1bjYtMjIudXNlcmFwaS5jb21cL1d4d08yUXhIVUk4Qm5qaks1R3U2TGQxMGlLVWZDYkFpQTVMRjlnXC9zTGdBSEQ4U0lwOC5qcGciLCI5ZTJkMjU5Mzk5YmZhYzBhNzU1YWVmOGMxNDE1ZjhhZSJd" data-embedUrl="https://daxab.com/player/490TfoQQWRdCiZ6SRKF4oOXoFt-alYr0LCNhN2XnflE2vX6lfpp0-o8cEuPh6YVv2FRT5UrGrRV6HAt50av5sQ" data-playerHash="490TfoQQWRdCiZ6SRKF4oOXoFt-alYr0LCNhN2XnflE2vX6lfpp0-o8cEuPh6YVv2FRT5UrGrRV6HAt50av5sQ" data-favHash="daff3e6ec27aa95eabcde1e74d2bdced">
<a href="/watch/104849236_456277453" data-id="104849236_456277453">
<div class="video-show"></div>
<div class="video-fav" data-hash="daff3e6ec27aa95eabcde1e74d2bdced"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/WxwO2QxHUI8BnjjK5Gu6Ld10iKUfCbAiA5LF9g/sLgAHD8SIp8.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 months ago</span>
<span class="video-view">1</span>
<span class="video-time">31:23</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">MAX PAYNE 3 NPC Wars 23 (UFE vs Cracha Preto)</div>
</a>
</div>
<div class="video-item video-901607_162902722" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuINCe0LHQt9C+0YAg0L/QtdGA0LLQvtCz0L4g0LTQvdGPIiwyNjkyLDE0LCJodHRwczpcL1wvc3VuOS0zMy51c2VyYXBpLmNvbVwvYzUyNjUwN1wvdTMwNjk5MzE3XC92aWRlb1wvbF8xYWFlYTc5ZC5qcGciLCJjNjU2ODIyYzQ3YzY3MTEyM2U5Y2QxYzYxMWZhOTQzMyJd" data-embedUrl="https://daxab.com/player/MPP59m69lok5-Y8H2-7d8VeeYLNoS0OAu5MMH119VzX_gINxC8tuxieSD_JJXXXpc5gewrlaG4KJqQ6sBqovNQ" data-playerHash="MPP59m69lok5-Y8H2-7d8VeeYLNoS0OAu5MMH119VzX_gINxC8tuxieSD_JJXXXpc5gewrlaG4KJqQ6sBqovNQ" data-favHash="ad3b2031e76f2b45329c183030b3b5e2">
<a href="/watch/-901607_162902722" data-id="-901607_162902722">
<div class="video-show"></div>
<div class="video-fav" data-hash="ad3b2031e76f2b45329c183030b3b5e2"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-33.userapi.com/c526507/u30699317/video/l_1aaea79d.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">14</span>
<span class="video-time">44:52</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Обзор первого дня</div>
</a>
</div>
<div class="video-item video593543_162951285" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIEJlc3QgVHJpY2siLDI0NTcsMjUsImh0dHBzOlwvXC9zdW45LTY5LnVzZXJhcGkuY29tXC9jNTI3MTA3XC91MzA2OTkzMTdcL3ZpZGVvXC9sXzk5ZGM1MjljLmpwZyIsIjBmODBkOWU2YWYzNTQ2NDY0YTdmZTE3OTRkYjk1YjVmIl0=" data-embedUrl="https://daxab.com/player/zH7mjdkr55I7sTSDgqCETS10HOpxudfewjW5sYsuS7GcOethdXAYdp_ksuyKW8ltL2Y98DVLzpvxdIqiWjM8Jw" data-playerHash="zH7mjdkr55I7sTSDgqCETS10HOpxudfewjW5sYsuS7GcOethdXAYdp_ksuyKW8ltL2Y98DVLzpvxdIqiWjM8Jw" data-favHash="d6ab0848c96f33c96ad3032c4a0d0637">
<a href="/watch/593543_162951285" data-id="593543_162951285">
<div class="video-show"></div>
<div class="video-fav" data-hash="d6ab0848c96f33c96ad3032c4a0d0637"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-69.userapi.com/c527107/u30699317/video/l_99dc529c.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">25</span>
<span class="video-time">40:57</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Best Trick</div>
</a>
</div>
<div class="video-item video-197946026_456239614" data-video="WyLQn9GA0L7RhdC+0LbQtNC10L3QuNC1IFNsZWVwaW5nIERvZ3MgKDIwMTMpICMyIC0g0JrQu9GD0LEgXCLQkdCw0Lwt0JHQsNC8XCIiLDczMDgsNDMsImh0dHBzOlwvXC9zdW42LTIzLnVzZXJhcGkuY29tXC9xMGxBdlpKd21wZE9samtzd3BZYW5MeFpKUl9aTDF6YjB1RXdfZ1wvYllXZE50M05xSDAuanBnIiwiYmU2NjkwN2I3ZGJjNzY1YjVkNzE5MjU5ZWQ0NTZmMDEiXQ==" data-embedUrl="https://daxab.com/player/4rih6FWu_lvGEAkBIRqCkzv0bVdPgxL27iG7u_O8E2K5ToKfAXHvFfiPQoX5j2a1pgLM2YC8vvZlFn6R3T7CCAjTGJ_ypGFaKZHPkNZdehA" data-playerHash="4rih6FWu_lvGEAkBIRqCkzv0bVdPgxL27iG7u_O8E2K5ToKfAXHvFfiPQoX5j2a1pgLM2YC8vvZlFn6R3T7CCAjTGJ_ypGFaKZHPkNZdehA" data-favHash="004476a96ec3d868483e77418cf244f2">
<a href="/watch/-197946026_456239614" data-id="-197946026_456239614">
<div class="video-show"></div>
<div class="video-fav" data-hash="004476a96ec3d868483e77418cf244f2"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/q0lAvZJwmpdOljkswpYanLxZJR_ZL1zb0uEw_g/bYWdNt3NqH0.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">4 months ago</span>
<span class="video-view">43</span>
<span class="video-time">02:01:48</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Прохождение Sleeping Dogs (2013) #2 - Клуб &quot;Бам-Бам&quot;</div>
</a>
</div>
<div class="video-item video-27575945_163557091" data-video="WyJYLWdhbWVzIDIwMTIs0KTRgNC40YHRgtCw0LnQuyzQn9C+0LvQvdC+0YHRgtGM0Y4hIiwzNzg1LDEzLCJodHRwczpcL1wvc3VuOS04MC51c2VyYXBpLmNvbVwvYzUyNjIxNVwvdTU5NDM1OTcxXC92aWRlb1wvbF84YThkNGI0NS5qcGciLCI3OTMwYzNkY2Y5NzdkZDhiYWM1ZWQ1NDJiYTkwZGIyOCJd" data-embedUrl="https://daxab.com/player/AbF3ZvgiT0YhfzJkU_739TmsDhd3LxNDlTnjEHtmtDb2CnGZ4gVkGYtpB4coNoLGqVB8J-hbicHmd3IWTA8gHw" data-playerHash="AbF3ZvgiT0YhfzJkU_739TmsDhd3LxNDlTnjEHtmtDb2CnGZ4gVkGYtpB4coNoLGqVB8J-hbicHmd3IWTA8gHw" data-favHash="9795118eba65377777021ab858d2594b">
<a href="/watch/-27575945_163557091" data-id="-27575945_163557091">
<div class="video-show"></div>
<div class="video-fav" data-hash="9795118eba65377777021ab858d2594b"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c526215/u59435971/video/l_8a8d4b45.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">13</span>
<span class="video-time">01:03:05</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012,Фристайл,Полностью!</div>
</a>
</div>
<span class="standalonead" style="margin: 0px 13px;padding-bottom: 15px;">
<iframe width="300" height="250" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10002729?time=1555363895445" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10002729"></iframe>
</span>
<div class="more-similar" onclick="event.cancelBubble = true; loadMoreSimilar(this);" data-page="1" data-video="-53425992_165397466" data-hash="84d987ceaf814a97d050f983234f2315d5157387">Show more</div>
</div>

<script>
$('.tabs > div.tab-btn').on('click', function() {
  if ($(this).hasClass('tab-show')) {
    return;
  }

  $(this).parent().children('div').removeClass('tab-show');
  $(this).addClass('tab-show');

  $('.tab-result').removeClass('tab-show');
  $('.tab-result.' + this.id).addClass('tab-show');

  // if ($('.width').width() < 1020 && this.id == 'tab_comments') {
  //   loadComments($('#video_hash').val().trim());
  // }
});
</script>
</div>
</div>
</div>
<div class="footer">
<div class="width">
<div class="now">
<b>You might be interested</b>
<span><a href="/video/True Anal">True Anal</a> <a href="/video/Russian">Russian</a> <a href="/video/Mai Thai">Mai Thai</a> <a href="/video/Rory Knox">Rory Knox</a> <a href="/video/Hotfallingdevil">Hotfallingdevil</a> <a href="/video/Ariella Ferrera">Ariella Ferrera</a> <a href="/video/Veronica Avluv">Veronica Avluv</a> <a href="/video/Latina Bbc">Latina Bbc</a> <a href="/video/Bride">Bride</a> <a href="/video/Hentai">Hentai</a> <a href="/video/Little Caprice">Little Caprice</a> <a href="/video/Private">Private</a> <a href="/video/Mona Azar">Mona Azar</a> <a href="/video/Mercedes Carrera">Mercedes Carrera</a> <a href="/video/Lolly Dames">Lolly Dames</a> <a href="/video/Vivian Azure">Vivian Azure</a> <a href="/video/Rachel Starr">Rachel Starr</a> <a href="/video/Abbie Maley">Abbie Maley</a> <a href="/video/Suzu Matsouka">Suzu Matsouka</a> <a href="/video/Tayland Shemale">Tayland Shemale</a></span>
</div>
<div class="clear"></div>
<div class="copy">
<a href="/">DaftSex</a>
<a href="/browse">Browse</a>
<a href="/hottest">Hottest</a>
<a href="/categories">Category</a>
<a href="/pornstars">Pornstars</a>
<a href="/holders">DMCA</a>
<a href="/privacy">Privacy</a>
<a href="/faq">FAQ</a>
<a href="/contact">Contact Us</a>
<a href="https://daft.sex/" target="_blank" title="Start search Page for DaftSex">DaftSex Search</a>
<a href="https://theporndude.com/" target="_blank" rel="nofollow" style="font-weight: bold;">Best Porn Sites</a>
<a href="https://twitter.com/DaftPost" target="_blank" class="twitter font-weight-bold" style="background: #01abf1 url(/img/twitter.png) no-repeat scroll center center;border-radius: 25px;color: transparent;background-size: 27px;"><i class="icon-twistter"></i>Twitter</a>
</div>
</div>
</div>
</div>
<div class="bg_layer"></div>

<script>
onPageLoaded();
$(window).load(onPageReady);
previewEvents();
</script>
<script type='text/javascript' src='//sufficientretiredbunker.com/a5/96/32/a59632dda777535e591fa2e7dde66a93.js'></script>
<script type="text/javascript" async src="/js/app.ve1c0c4626c025980.js"></script>
<script type="text/javascript">
!function(){var w,n=window.outerWidth-window.innerWidth,o=window.outerHeight-window.innerHeight,t=$(window).width(),e=$(window).height();function i(i){w&&clearTimeout(w),w=setTimeout(function(){var i=window.outerWidth-window.innerWidth,w=window.outerHeight-window.innerHeight;n!==i&&o!==w||n==i&&o==w||t==$(window).width()&&e==$(window).height()||t!==$(window).width()&&e!==$(window).height()||!winFullscreen&&(400<i&&n<i&&400<Math.abs(i-n)||200<w&&o<w&&200<Math.abs(w-o))&&(setCookie("_dt",1,{path:"/",expires:1800,domain:"."+location.hostname}),dt()),n=window.outerWidth-window.innerWidth,o=window.outerHeight-window.innerHeight,t=$(window).width(),e=$(window).height()},500)}browser.mobile||$(window).off("resize",i).on("resize",i)}();
</script>

<div style="display:none;">
<script type="text/javascript">
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
</script>
</div>


</body>
</html>
<!DOCTYPE html>
<html>
<head data-extra="aIX2DL3JIGnO7i8k6HvdT4">
<meta charset="UTF-8">
<title>2012 X-Games 18 motocross best trick — DaftSex</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="/index.js?6" defer></script>
<link rel="manifest" href="/manifest.webmanifest">
<meta name="robots" content="all" />
<meta name="robots" content="noarchive" />
<meta name="description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality">
<meta name="keywords" content="Watch, video, daftsex, motocross, best, trick, daftsex">
<meta property="og:url" content="https://daftsex.com/watch/-53425992_165397466" />
<meta property="og:type" content="video.movie" />
<meta property="og:title" content="2012 X-Games 18 motocross best trick" />
<meta property="og:description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality" />
<meta property="og:image" content="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg" />
<meta property="og:video:type" content="text/html" />
<link href="/css/robotocondensed.v4b6944ca5bbf3c8b.css" rel="stylesheet" type="text/css">
<link rel="icon" type="image/png" sizes="196x196" href="/img/daftlogo196x196.png?2">
<link rel="shortcut icon" href="/img/favicon.ico?5" />
<meta name="theme-color" content="#C00">
<link rel="stylesheet" href="/css/common.v112268244c82433e.css" />
<link rel="stylesheet" href="/css/icons.v38e1854b6d41caec.css" />
<link rel="stylesheet" href="/css/dark.ve32b1eba318e418e.css" />
<script type="text/javascript">
window.globEmbedUrl = 'https://daxab.com/player/';
window.timeNow = 1658939950;
setInterval(function() {
  window.timeNow++;
}, 1000);
window.liteopen = false;
window.is_logged = false;
</script>
<script type="text/javascript" src="/js/jquery-2.1.1.min.v18b7e87c91d98481.js"></script>
<script type="text/javascript" src="/js/history.v955089448af5a0c8.js"></script>
<script type="text/javascript" src="/js/nprogress.v3410974b8841b4f3.js"></script>
<script type="text/javascript" src="/js/nouislider.v2192f61dc764023a.js"></script>
<script type="text/javascript" src="/js/select.ve363dc0076d2c78b.js"></script>
<script type="text/javascript" src="/js/common.vb1f31c4b59a9e4d1.js"></script>
<script type="text/javascript" src="/js/auth.vfa3c32a15fba2304.js"></script>
<script type="text/javascript" src="/js/jquery.mutations.min.v4b147b138a5b1019.js"></script>
<script type="text/javascript" src="/js/fav.v764365b62392eb58.js"></script>
<script type="text/javascript" src="/js/share.vdf8ddf291dc2f417.js"></script>
<script type="text/javascript" src="/js/likes.v5e342c5feda70804.js"></script>
<script type="text/javascript">
window._stv = '67c5c441c9e2';
window.log_version = 'fbcb78248ac55029';
</script>
</head>
<body>
<a class="up" onclick="toTop();">&uarr;</a>
<div class="page_wrapper">
<div class="header">
<div class="width">
<div class="header_inner">
<a onclick="window.history.go(-1); return false;" title="Back" class="back-logo"><i class="icon-left"></i></a>
<a href="/" title="DaftSex" class="logo">
<svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="55.000000pt" height="50.000000pt" viewBox="0 0 311.000000 127.000000" preserveAspectRatio="xMidYMid meet">
<g transform="translate(0.000000,180.000000) scale(0.100000,-0.100000)" fill="#dd3333" stroke="none">
<path d="M895 1713 c-142 -78 -362 -148 -559 -178 -54 -9 -102 -18 -107 -21
-14 -8 -10 -228 5 -354 36 -284 110 -501 235 -692 110 -167 280 -320 442 -398
l46 -23 54 27 c149 75 311 216 408 356 59 84 131 221 168 320 35 92 79 287 93
410 12 101 18 346 8 355 -2 2 -49 11 -104 19 -215 33 -424 99 -578 184 l-51
29 -60 -34z m15 -818 l0 -636 -45 31 c-128 88 -246 229 -327 390 -86 171 -148
427 -148 612 0 72 2 78 23 83 209 46 339 85 447 134 25 11 46 21 48 21 1 0 2
-286 2 -635z m197 592 c99 -40 258 -87 343 -102 79 -14 74 -4 67 -133 -4 -63
-11 -120 -16 -125 -5 -5 -38 -6 -83 -1 l-73 9 3 46 3 45 -63 13 c-35 6 -75 14
-90 18 l-28 5 0 -95 c0 -81 3 -96 18 -102 9 -3 81 -9 160 -12 78 -3 142 -10
142 -15 0 -14 -39 -155 -61 -221 -35 -101 -118 -253 -188 -340 -65 -79 -226
-221 -242 -212 -8 5 -12 501 -5 514 3 4 43 2 90 -3 l85 -9 3 -41 3 -41 37 65
c59 105 64 96 -61 104 -61 3 -122 11 -136 16 l-25 10 0 325 c0 179 2 325 4
325 2 0 53 -20 113 -43z" />
<path d="M628 1242 c-79 -19 -78 -15 -53 -153 18 -102 51 -201 98 -294 69
-137 66 -145 67 178 l0 287 -22 -1 c-13 -1 -53 -9 -90 -17z" />
</g>
</svg>
</a>
<div class="clear-form"><i class="icon-cancel"></i></div>
<form class="search-form" onsubmit="return search(true);">
<input type="text" id="q" autocomplete="off" value="" placeholder="Search millions of videos...">
<a title="RandomTV" href="/video/Dpp Dp" class="randomQuery"><span class="deskrandom"></span></a>
<button type="submit" title="Search button">Search</button>
<div class="head-menu"></div>
</form>
<div class="head-menu module-browse"><a href="/browse">Browse</a></div>
<div class="head-menu module-hottest"><a href="/hottest">Hottest</a></div>
<div class="head-menu module-categories"><a href="/categories">Category</a></div>
<div class="head-menu module-pornstars"><a href="/pornstars">Pornstars</a></div>
<div class="head-menu"><a href="https://theporndude.com/" target="_blank" rel="nofollow" style="color: #000;">Best Porn Sites</a></div>
<button class="btn-search" type="button"><i class="icon-left"></i></button>
<span class="hide-item">

<a href="/pornstars"><span class="btn-pornstars"></span></a>
<a href="/categories"><span class="btn-catt"></span></a>
<a href="/hottest"><span class="btn-hottest"></span></a>
<a href="/browse"><span class="btn-browse"></span></a>
</span>
<span class="auth login hide-item" title="Login" onclick="Auth.Login();"></span>
</div>
</div>
<div id="progress_content"></div>
<div class="suggest_search"><div class="suggest_inner width"><ul class="suggest_items"></ul></div></div>
</div>
<div class="bg_width">
<div class="width">

<div class="aboveVideo standalonead" style="text-align: center;padding-top: 75px;margin-bottom: -70px;">

<span style="
    text-align: center;
"> <a style="
    text-align: left;
    color: #fff;
    font-size: 15px;
    text-decoration: none;
    padding: 5px;
    top: 61px;
    margin: 5px;
    border-radius: 25px;
    display: block;
    background-color: #5e62ff;
    " href="https://artsporn.com" target="_blank">
ℹ: ARTSPORN.COM - Another new Alternative, faster website without ads. 👊⚡️⚡️⚡️</a></span>


<iframe width="300" height="150" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10009021?time=1575323689465" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10012984"></iframe>




</div>
<div class="aboveVideoPC standalonead" style="text-align: center;padding-top: 75px;margin-bottom: -70px;">








</div>
<div class="content-wrapper" id="content">
<div itemscope itemtype="http://schema.org/VideoObject">
<link itemprop="thumbnailUrl" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
<span itemprop="thumbnail" itemscope itemtype="http://schema.org/ImageObject">
<link itemprop="url" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
<link itemprop="contentUrl" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
</span>
<meta itemprop="name" content="2012 X-Games 18 motocross best trick" />
<meta itemprop="uploadDate" content="2013-07-17T16:03:39+04:00">
<meta itemprop="description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality" />
<meta itemprop="duration" content="T28M50S" />
</div>

<div class="video">
<a class="standalonead" style="font-size: 14px; font-weight: bold;color:#0a0a0a; text-align: center;" href="https://landing.brazzersnetwork.com/?ats=eyJhIjoyOTIyOTcsImMiOjU3NzAzNTQ4LCJuIjoxNCwicyI6OTAsImUiOjg4MDMsInAiOjU3fQ==" target="_blank" rel="nofollow"><div class="message-container" style="background-color:#ffd001;padding: 7px;font-size: 17px;font-weight: 700;"><span class="nomobile">EXCLUSIVE DaftSex OFFER - Join BRAZZERS Only 1$ Today ! [PROMO]</span> <span class="nomobile"><span style="color:rgba(255, 208, 1)"></span><span style="color:rgba(255, 208, 1)"><span></span></span></span></div></a>
<div style="background:#000000;outline: 0px solid #fff;" class="frame_wrapper">
<script id="data-embed-video-frEDjd_hTdWwHAZ9e1dRSxRBv_JTO5mAOj7MhSUOof6JksrdHCW1ahEzjkUFTFhrnLuhsPWol2Aqntf_TuxbDfbWuzgFvcoWb3ebTxoH32s">
if (window.DaxabPlayer && window.DaxabPlayer.Init) {
  DaxabPlayer.Init({
    id: 'video-53425992_165397466',
    origin: window.globEmbedUrl.replace('/player/', ''),
    embedUrl: window.globEmbedUrl,
    hash: "frEDjd_hTdWwHAZ9e1dRSxRBv_JTO5mAOj7MhSUOof6JksrdHCW1ahEzjkUFTFhrnLuhsPWol2Aqntf_TuxbDfbWuzgFvcoWb3ebTxoH32s",
    color: "f12b24",
    styles: {
      border: 0,
      overflow: 'hidden',
      marginBottom: '-5px'
    },
    attrs: {
      frameborder: 0,
      allowFullScreen: ''
    },
    events: function(data) {
      if (data.event == 'player_show') {
        dt();

        HistoryWatch.set("-53425992_165397466", "WyIyMDEyIFgtR2FtZXMgMTggbW90b2Nyb3NzIGJlc3QgdHJpY2siLDE3MzAsNjIsImh0dHBzOlwvXC9zdW45LTQ2LnVzZXJhcGkuY29tXC9jNTM1MzA0XC91NjI1MjQ2NVwvdmlkZW9cL2xfZGQ5MzBlOGYuanBnIiwiODc4OGE3MDgzNDAzZWNkOTg0ZWNiOGExNThhMmJjN2IiXQ==");
      }
    }
  });
}
</script>
</div>
<div class="video_info_wrapper">
<h1 class="heading">2012 X-Games 18 motocross best trick</h1>
<div class="voting-module video-watch">
<span class="icon-container">
<span class="icon icon1"></span>
<span class="icon icon2 hidden-vote"></span>
</span>
<span class="text-container">
<span class="text text1"></span>
<span class="text text2"></span>
</span>
</div>
<script>
var videoLikes = 0;

_likes(videoLikes, "-53425992_165397466", "f277e7e3");

logVideoId("-53425992_165397466", "f277e7e3");
</script>
<div class="video_fav" alt="To favorites" title="To favorites" onclick="Fav.Toggle(this, '-53425992_165397466', '6adab556e398e4602ff6ca8224d02364');"></div>
<div class="video_embed" title="Share" onclick="Embed.show(-53425992, 165397466);"></div>
<div class="main_share_panel">
<div id="addthis_sharing_toolbox">
<div class="share_btn share_btn_re"><span class="re_icon" onclick="return Share.reddit();"></span></div>
<div class="share_btn share_btn_fb"><span class="fb_icon" onclick="return Share.facebook();"></span></div>
<div class="share_btn share_btn_tw"><span class="tw_icon" onclick="return Share.twitter();"></span></div>
<div class="share_btn share_btn_vk"><span class="vk_icon" onclick="return Share.vkontakte();"></span></div>
</div>
</div>
<div class="clear"></div>
<div class="bordertop">
<br>Views: 62 <br>Published: 17 Jul at 04:03 PM </div></div>
<div>Duration: 28:50</div>
</div>
<div class="tabs_wrapper">
<div class="tabs">
<style>.tabs > div.tab-btn{width: 50%;}</style>
<div class="tab-btn tab-show" id="tab_similar">Related</div>
<div class="tab-btn" id="tab_comments">Comments</div>
</div>
</div>
<div class="rightbar tab_similar tab-result tab-show">
<span style="margin: 0px 13px;padding-bottom: 15px;" class="standalonead">
<iframe width="300" height="250" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10002729?time=1555363895445" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10002729"></iframe>
</span>
<div class="video-item video5817_163924401" data-video="WyJXaW50ZXIgWCBHYW1lcyAyMDEyIFtBc3Blbl0gLUZpbmFsIE1lbnMgU3VwZXJQaXBlIiw1MjQ0LDQzLCJodHRwczpcL1wvc3VuOS03My51c2VyYXBpLmNvbVwvYzUxOTYxMVwvdTA1ODE3XC92aWRlb1wvbF83YzRiNjI4Zi5qcGciLCJiNDI1NzY4NmJlYjIxYmE5MjU3NmM5M2NkY2E0YzQxYSJd" data-embedUrl="https://daxab.com/player/f0rhrBu5bKPNEP9JhJeRZqv7aSFkR3cfd70s_8EyX_cYFgZU5fLK2aR-ZN1QX0ulF3E834HcvKc57aiVFYdgQg" data-playerHash="f0rhrBu5bKPNEP9JhJeRZqv7aSFkR3cfd70s_8EyX_cYFgZU5fLK2aR-ZN1QX0ulF3E834HcvKc57aiVFYdgQg" data-favHash="24659d3a3191ece1b857645e1fc8a685">
<a href="/watch/5817_163924401" data-id="5817_163924401">
<div class="video-show"></div>
<div class="video-fav" data-hash="24659d3a3191ece1b857645e1fc8a685"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-73.userapi.com/c519611/u05817/video/l_7c4b628f.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">43</span>
<span class="video-time">01:27:24</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Winter X Games 2012 [Aspen] -Final Mens SuperPipe</div>
</a>
</div>
<div class="video-item video-4296410_167364583" data-video="WyJYLWdhbWVzIDIwMTIgYm14IHBhcmsgZmluYWwgKNC90LAg0YDRg9GB0YHQutC+0LwpIiw1MjkzLDM1LCJodHRwczpcL1wvc3VuOS01NS51c2VyYXBpLmNvbVwvYzUzNTUyMlwvdTk3MDA0ODdcL3ZpZGVvXC9sXzgwYzVjOThmLmpwZyIsIjc5YWY1NjFmYzFiYjQ5NmFhZjM5MzEzNzBkNjBkZTQ4Il0=" data-embedUrl="https://daxab.com/player/S-Q1iF-oIs4HjRGjMqa3amjq0dFCE3o4NiGy_702rlvDiMeZEV2i9ZWiq3l8dd2nV_7VKD6gon8zcGxCh7X-SA" data-playerHash="S-Q1iF-oIs4HjRGjMqa3amjq0dFCE3o4NiGy_702rlvDiMeZEV2i9ZWiq3l8dd2nV_7VKD6gon8zcGxCh7X-SA" data-favHash="24c1b3a842c081128ac6b11cf664db03">
<a href="/watch/-4296410_167364583" data-id="-4296410_167364583">
<div class="video-show"></div>
<div class="video-fav" data-hash="24c1b3a842c081128ac6b11cf664db03"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-55.userapi.com/c535522/u9700487/video/l_80c5c98f.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">8 years ago</span>
<span class="video-view">35</span>
<span class="video-time">01:28:13</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012 bmx park final (на русском)</div>
</a>
</div>
<div class="video-item video-20774214_163747625" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxKSBIRCIsNjY5MiwxNzUsImh0dHBzOlwvXC9zdW45LTgwLnVzZXJhcGkuY29tXC9jNTE0MjE4XC91MTE0MDIzMTJcL3ZpZGVvXC9sXzE3NWVhYzgxLmpwZyIsIjFmNTlmYmM5NTc0MjY4N2JhNmEwOGQ5ZWY3ZjUzMmZlIl0=" data-embedUrl="https://daxab.com/player/UIKd_u9WycS2_cH6_7Df7fjv0KQh9AMRARbeBUJn482AKQCOUAjeVGp7TSjoaXZBxisvdp2UQk-NTztzph7diw" data-playerHash="UIKd_u9WycS2_cH6_7Df7fjv0KQh9AMRARbeBUJn482AKQCOUAjeVGp7TSjoaXZBxisvdp2UQk-NTztzph7diw" data-favHash="50bb3585ce2c07e4f89225006b652011">
<a href="/watch/-20774214_163747625" data-id="-20774214_163747625">
<div class="video-show"></div>
<div class="video-fav" data-hash="50bb3585ce2c07e4f89225006b652011"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c514218/u11402312/video/l_175eac81.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">175</span>
<span class="video-time">01:51:32</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1) HD</div>
</a>
</div>
<div class="video-item video-127946280_456239036" data-video="WyJFcm9nZSEgSCBtbyBHYW1lIG1vIEthaWhhdHN1IFphbm1haSDQrdGA0L7Qs9C1ISAwMlwv0JfQsNCx0YvRgtGM0YHRjyDQsiDRgdC+0LfQtNCw0L3QuNC4INGF0LXQvdGC0LDQuSDQuNCz0YDRiyAoKzE4IEhlbnRhaSBIRCkiLDE4NDYsNDA0NSwiaHR0cHM6XC9cL3N1bjYtMjIudXNlcmFwaS5jb21cL2M2MjY2MTlcL3Y2MjY2MTkyODBcLzI2MTExXC9TSmhKdExNNDBYNC5qcGciLCJhNmI1NmM0M2NkNmQxNTJiMzljYWExMzNhZmQzNGUwZSJd" data-embedUrl="https://daxab.com/player/yevwqcKdDJJon4t338IvB7ZpfCNLvad9_i1fxV8zkfD-6X1sKFBywA9cqYuTFXB8RxU1Bxdq-OfUydUJ_zODsl-AQzNB2vfnSVWVzpXm6BI" data-playerHash="yevwqcKdDJJon4t338IvB7ZpfCNLvad9_i1fxV8zkfD-6X1sKFBywA9cqYuTFXB8RxU1Bxdq-OfUydUJ_zODsl-AQzNB2vfnSVWVzpXm6BI" data-favHash="3a7c012b7e3a9ade5274c30b1f34afa5">
<a href="/watch/-127946280_456239036" data-id="-127946280_456239036">
<div class="video-show"></div>
<div class="video-fav" data-hash="3a7c012b7e3a9ade5274c30b1f34afa5"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c626619/v626619280/26111/SJhJtLM40X4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">5 years ago</span>
<span class="video-view">4k</span>
<span class="video-time">30:46</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Eroge! H mo Game mo Kaihatsu Zanmai Эроге! 02/Забыться в создании хентай игры (+18 Hentai HD)</div>
</a>
</div>
<div class="video-item video-20774214_163586566" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxKSIsNjc4NCwyMDQsImh0dHBzOlwvXC9zdW45LTg2LnVzZXJhcGkuY29tXC9jNTMyMzE1XC91OTcyODAxMVwvdmlkZW9cL2xfNTYzNzMyMmMuanBnIiwiYWQxZjRiYzE1ODQ0M2VlODE2MDVlZjcyNTE0YzczMDIiXQ==" data-embedUrl="https://daxab.com/player/9xdCJ7sssAwg39bADHxYp4T0EpeLz7YqdS3WY60bLJxWFhg3MdmFIJG2eq5KbZRIrbwzIyUJhMUKWj5fMG7grg" data-playerHash="9xdCJ7sssAwg39bADHxYp4T0EpeLz7YqdS3WY60bLJxWFhg3MdmFIJG2eq5KbZRIrbwzIyUJhMUKWj5fMG7grg" data-favHash="255bdad6a740955e40ef7e3cc4be92d1">
<a href="/watch/-20774214_163586566" data-id="-20774214_163586566">
<div class="video-show"></div>
<div class="video-fav" data-hash="255bdad6a740955e40ef7e3cc4be92d1"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-86.userapi.com/c532315/u9728011/video/l_5637322c.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">204</span>
<span class="video-time">01:53:04</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1)</div>
</a>
</div>
<div class="video-item video-147765253_456239519" data-video="WyJHeW1uYXN0aWNzIC0gQXJ0aXN0aWMgLSBNZW4mIzM5O3MgVGVhbSBGaW5hbCB8IExvbmRvbiAyMDEyIE9seW1waWMgR2FtZXMiLDExNjQwLDEsImh0dHBzOlwvXC9zdW42LTIwLnVzZXJhcGkuY29tXC93Y2t5eHgwLTdCQjlFa05BRDJYbVpfeDN1ZjZVemUtcnVxVl94d1wvYXZpSDc4Vk1OZEkuanBnIiwiN2FmNjY0MDdiNDQwMGM3NTc2NWRmYTYzZGJmZWEwMTQiXQ==" data-embedUrl="https://daxab.com/player/9NLfSWm1I1SAFlpqeaAZd1R7R5H4ra4HxHTMVDRwGDyxPUTqs-GFHP9cwmElqkUgr1Y-Hc1c6t9pclC6POZJsEWiZj2gXs4SGZYAN5yGkrc" data-playerHash="9NLfSWm1I1SAFlpqeaAZd1R7R5H4ra4HxHTMVDRwGDyxPUTqs-GFHP9cwmElqkUgr1Y-Hc1c6t9pclC6POZJsEWiZj2gXs4SGZYAN5yGkrc" data-favHash="21df7febeb200b63d0e7d35d0cdd5e05">
<a href="/watch/-147765253_456239519" data-id="-147765253_456239519">
<div class="video-show"></div>
<div class="video-fav" data-hash="21df7febeb200b63d0e7d35d0cdd5e05"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/wckyxx0-7BB9EkNAD2XmZ_x3uf6Uze-ruqV_xw/aviH78VMNdI.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 months ago</span>
<span class="video-view">1</span>
<span class="video-time">03:14:00</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Gymnastics - Artistic - Men&amp;#39;s Team Final | London 2012 Olympic Games</div>
</a>
</div>
<div class="video-item video-188576299_456239468" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAxIFs3MjBwXSIsMTg2OSwxOTEyLCJodHRwczpcL1wvc3VuNi0yMC51c2VyYXBpLmNvbVwvYzg1NzQyMFwvdjg1NzQyMDM5NVwvMjJhZTBmXC9xV0dMVjlycDgyRS5qcGciLCI2NzM2ZGM1NzgzOWRhYTFjMGI3M2YwN2UxZDhhM2ZkNSJd" data-embedUrl="https://daxab.com/player/9qboGfHppXMX4ckyUt7124H0Q7kKw5MLMVDxwpM7v-DjVihbK93kQwsU2xrFBL61-XtZvYGVWB-0ZKwudMPMLa3hMHivOKxuiaT-mXI_rTM" data-playerHash="9qboGfHppXMX4ckyUt7124H0Q7kKw5MLMVDxwpM7v-DjVihbK93kQwsU2xrFBL61-XtZvYGVWB-0ZKwudMPMLa3hMHivOKxuiaT-mXI_rTM" data-favHash="cc2ea25fe6368387cd0d23e5d82299d1">
<a href="/watch/-188576299_456239468" data-id="-188576299_456239468">
<div class="video-show"></div>
<div class="video-fav" data-hash="cc2ea25fe6368387cd0d23e5d82299d1"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/c857420/v857420395/22ae0f/qWGLV9rp82E.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,9k</span>
<span class="video-time">31:09</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 01 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239466" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAzIFs3MjBwXSIsMTgwNiwxNzMwLCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NTcyOFwvdjg1NTcyODM5NVwvMjRmMjY0XC9lcHl0TURrMFJkSS5qcGciLCI3Y2RiMDhjOTFhNmMwOWZiNjQ3YzBkNDhmZTllMzZlMyJd" data-embedUrl="https://daxab.com/player/YAiGbHeNS9gpniP5ljCEUK9C9CJY1bMg9wvSsGhbbxWKXK0AlVl_td1YohhihT1aobKViByPJFdZvYoQhDdwgnlS6HXqJaIl_68ok9eflgU" data-playerHash="YAiGbHeNS9gpniP5ljCEUK9C9CJY1bMg9wvSsGhbbxWKXK0AlVl_td1YohhihT1aobKViByPJFdZvYoQhDdwgnlS6HXqJaIl_68ok9eflgU" data-favHash="ac89a8b41ee3f0f4315343b2c6f6a9fa">
<a href="/watch/-188576299_456239466" data-id="-188576299_456239466">
<div class="video-show"></div>
<div class="video-fav" data-hash="ac89a8b41ee3f0f4315343b2c6f6a9fa"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c855728/v855728395/24f264/epytMDk0RdI.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,7k</span>
<span class="video-time">30:06</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 03 [720p]</div>
</a>
</div>
<div class="video-item video-20774214_163763516" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTQgKExpdmUgU2hvdyAyKSBIRCIsNjAzNCwxMzYsImh0dHBzOlwvXC9zdW45LTEzLnVzZXJhcGkuY29tXC9jNTI3NjEwXC91MTE0MDIzMTJcL3ZpZGVvXC9sXzg1MzBmODBiLmpwZyIsIjNjNjFjYzYyMDY2YmI5ZWJhM2RhNjZkNTU3Y2M5MWMyIl0=" data-embedUrl="https://daxab.com/player/q5mqH_K7-Rw0WsdXN1_JgyO0kcUrORGwwW8zQOWEA68eRfi658PB_vO27s3c96xk25bZZGSX5k4S1cda8FxGpg" data-playerHash="q5mqH_K7-Rw0WsdXN1_JgyO0kcUrORGwwW8zQOWEA68eRfi658PB_vO27s3c96xk25bZZGSX5k4S1cda8FxGpg" data-favHash="0938a89746b3a3d4fa400d6265b36a06">
<a href="/watch/-20774214_163763516" data-id="-20774214_163763516">
<div class="video-show"></div>
<div class="video-fav" data-hash="0938a89746b3a3d4fa400d6265b36a06"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-13.userapi.com/c527610/u11402312/video/l_8530f80b.jpg"></div>

<div class="video-play"></div>
 <div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">136</span>
<span class="video-time">01:40:34</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x14 (Live Show 2) HD</div>
</a>
</div>
<div class="video-item video-901607_162943067" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIE1hbmBzIE1vdG8gWCBFbmR1cm8gWCIsNDc5OSw4MCwiaHR0cHM6XC9cL3N1bjktNDkudXNlcmFwaS5jb21cL2M1Mjc1MDdcL3UzMDY5OTMxN1wvdmlkZW9cL2xfYTZmNDJkMDkuanBnIiwiYmNmN2I5NjA1ZmNmZDI3OTFhNzUzY2FkYjNjZDViYTAiXQ==" data-embedUrl="https://daxab.com/player/s7XPpjeL8jmVKsQo5ev__yYwvTpyfHN2FMUYASnoSp5XtsPlJJ3vf5Do0K8Jjub6m7-E4wiOlHAPG3Y94sJ2RQ" data-playerHash="s7XPpjeL8jmVKsQo5ev__yYwvTpyfHN2FMUYASnoSp5XtsPlJJ3vf5Do0K8Jjub6m7-E4wiOlHAPG3Y94sJ2RQ" data-favHash="c3778c5de8bcf23b7d197e025de7fd81">
<a href="/watch/-901607_162943067" data-id="-901607_162943067">
<div class="video-show"></div>
<div class="video-fav" data-hash="c3778c5de8bcf23b7d197e025de7fd81"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-49.userapi.com/c527507/u30699317/video/l_a6f42d09.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">80</span>
<span class="video-time">01:19:59</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Man`s Moto X Enduro X</div>
</a>
</div>
<div class="video-item video-188576299_456239467" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDA0IFs3MjBwXSIsMTgwNSwxMzgwLCJodHRwczpcL1wvc3VuNi0yMS51c2VyYXBpLmNvbVwvYzg1NTIzNlwvdjg1NTIzNjM5NVwvMjU0ODc1XC9VYUFDbnNmTUNGWS5qcGciLCI0ZTg2ZDE5M2I1ZWExZmQ0ODhkNGZlNWQ0ZGMzZTkzNCJd" data-embedUrl="https://daxab.com/player/1qfoB1oatRr4atqTtnMyydGbh7UE96YLNAIBtcvEI9mKh7L11tD1ehY1LuQc7Wq6nnWybksghJtabRwhwr-KuN-VDnNjAzTW7bdX4iM18nU" data-playerHash="1qfoB1oatRr4atqTtnMyydGbh7UE96YLNAIBtcvEI9mKh7L11tD1ehY1LuQc7Wq6nnWybksghJtabRwhwr-KuN-VDnNjAzTW7bdX4iM18nU" data-favHash="78bdcfd180ecfe4a5ad2e93a0d744515">
<a href="/watch/-188576299_456239467" data-id="-188576299_456239467">
<div class="video-show"></div>
<div class="video-fav" data-hash="78bdcfd180ecfe4a5ad2e93a0d744515"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-21.userapi.com/c855236/v855236395/254875/UaACnsfMCFY.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,4k</span>
<span class="video-time">30:05</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 04 [720p]</div>
</a>
</div>
<div class="video-item video-200135128_456239373" data-video="WyLQkNC80LXRgNC40LrQsNC90YHQutC40Lkg0LrQvtGI0LzQsNGAIC0gQWxhbiBXYWtlJiMzOTtzIEFtZXJpY2FuIE5pZ2h0bWFyZSAjMSIsMjUwMSwxLCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvVHp4NTZSQnR4YXpuUV81X2lrVFpmZmUzcmhmSWgwZWVhQ2dra1FcLy0tZWFOSkdrTnhnLmpwZyIsIjMzMzM4NzZkMTczMjkzMzU4NmVhYjQxYzBmOWZlNGJjIl0=" data-embedUrl="https://daxab.com/player/vQtS035d76BBNOsihScq5HNNfFvs56e77u3LLmgeA2EoJ3qGpmrEvmAHg8Dx7LVYJyctPSJAv4jWbpeeuD9fdZqLDoiqwDFdd93q2UDQiHw" data-playerHash="vQtS035d76BBNOsihScq5HNNfFvs56e77u3LLmgeA2EoJ3qGpmrEvmAHg8Dx7LVYJyctPSJAv4jWbpeeuD9fdZqLDoiqwDFdd93q2UDQiHw" data-favHash="268336d8383e223acd548ce94a4c1928">
<a href="/watch/-200135128_456239373" data-id="-200135128_456239373">
<div class="video-show"></div>
<div class="video-fav" data-hash="268336d8383e223acd548ce94a4c1928"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/Tzx56RBtxaznQ_5_ikTZffe3rhfIh0eeaCgkkQ/--eaNJGkNxg.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 months ago</span>
<span class="video-view">1</span>
<span class="video-time">41:41</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Американский кошмар - Alan Wake&amp;#39;s American Nightmare #1</div>
</a>
</div>
<div class="video-item video195091338_456239074" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIEJlc3QgVHJpY2siLDI0NTcsMiwiaHR0cHM6XC9cL3N1bjYtMjEudXNlcmFwaS5jb21cL2M2MzY5MjVcL3Y2MzY5MjUzMzhcLzJmMWRcLzlKbm9iQ1R0ZjlnLmpwZyIsImUwNWQwZmYwMWJiNDE5MWFhYzRkOWQwNDNkNjk1OWMyIl0=" data-embedUrl="https://daxab.com/player/MHcf4c8BBxp7eT22PaLrkkMCUHXHg9vOM5ZaKn4EXzDSMLVJrJQCU3dNq8kUG38VKaOF90P6DfqyIOe3PpmlLA" data-playerHash="MHcf4c8BBxp7eT22PaLrkkMCUHXHg9vOM5ZaKn4EXzDSMLVJrJQCU3dNq8kUG38VKaOF90P6DfqyIOe3PpmlLA" data-favHash="1393143e615e35dcd69cbdbe2d1e56f3">
<a href="/watch/195091338_456239074" data-id="195091338_456239074">
<div class="video-show"></div>
<div class="video-fav" data-hash="1393143e615e35dcd69cbdbe2d1e56f3"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-21.userapi.com/c636925/v636925338/2f1d/9JnobCTtf9g.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">6 years ago</span>
<span class="video-view">2</span>
<span class="video-time">40:57</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Best Trick</div>
</a>
</div>
<div class="video-item video-188576299_456239333" data-video="WyLQl9Cw0YHRgtC10L3Rh9C40LLQvi3QoNCw0YHQv9GD0YLQvdCw0Y8g0LTQtdCy0YPRiNC60LAg0KHRg9C60YPQvNC4IFRzdW5kZXJlIElucmFuIFNob3VqbyBTdWt1bWkgLSAwMiBbNzIwcF0iLDExNTEsNTY2LCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NzIxNlwvdjg1NzIxNjE1NFwvMWE5Yzc5XC9pemdpTm9pTmZLby5qcGciLCIxMTg1ZjdjMWFlNDQyZjc5OGJjM2RlMGUzODQ5NjM1OCJd" data-embedUrl="https://daxab.com/player/zIW_wYR54YFZ4JOI_YYtKQhpWkSK6OPpLRyhQBzI5bIAVHq6gfIJxG7Mu-ky4lsSsQcnPGErUHcZAwywAusfKRwoIR9vca9WR6t8pWBlQQk" data-playerHash="zIW_wYR54YFZ4JOI_YYtKQhpWkSK6OPpLRyhQBzI5bIAVHq6gfIJxG7Mu-ky4lsSsQcnPGErUHcZAwywAusfKRwoIR9vca9WR6t8pWBlQQk" data-favHash="1059f8f2c8602da44c308a4c7842b98c">
<a href="/watch/-188576299_456239333" data-id="-188576299_456239333">
<div class="video-show"></div>
<div class="video-fav" data-hash="1059f8f2c8602da44c308a4c7842b98c"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c857216/v857216154/1a9c79/izgiNoiNfKo.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">566</span>
<span class="video-time">19:11</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Застенчиво-Распутная девушка Сукуми Tsundere Inran Shoujo Sukumi - 02 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239465" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAyIFs3MjBwXSIsMTgwNiwxMjAyLCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NTYyMFwvdjg1NTYyMDM5NVwvMjQ5ZDg3XC9CcGI5Q2tBUWFBZy5qcGciLCIzNjYyZDFjMWE5NjQ5OTkzZGExMGI3NWYyOTc0MGRjNyJd" data-embedUrl="https://daxab.com/player/lRoz47jlN40BWsgXrhq1ohWY2u1PSIXpw7pAbWHAfNCSI7unbE7JexP0yCKakNxjX6xaTleiaWzbSMU2GrUD-0j9uMb6gC_3YH9L9sbjxOg" data-playerHash="lRoz47jlN40BWsgXrhq1ohWY2u1PSIXpw7pAbWHAfNCSI7unbE7JexP0yCKakNxjX6xaTleiaWzbSMU2GrUD-0j9uMb6gC_3YH9L9sbjxOg" data-favHash="ff491e850a72875ffb536f7e4fe9c880">
<a href="/watch/-188576299_456239465" data-id="-188576299_456239465">
<div class="video-show"></div>
<div class="video-fav" data-hash="ff491e850a72875ffb536f7e4fe9c880"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c855620/v855620395/249d87/Bpb9CkAQaAg.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,2k</span>
<span class="video-time">30:06</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 02 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239332" data-video="WyLQl9Cw0YHRgtC10L3Rh9C40LLQvi3QoNCw0YHQv9GD0YLQvdCw0Y8g0LTQtdCy0YPRiNC60LAg0KHRg9C60YPQvNC4IFRzdW5kZXJlIElucmFuIFNob3VqbyBTdWt1bWkgLSAwMSBbNzIwcF0iLDEwODIsOTE4LCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvVVJDWFp3aHhjT285TVR2QWtzWjZpUTd6TXFZXzR5alZiWFByandcL3ZIQnB6RFRmLVNjLmpwZyIsIjY1MTEyODU3MmUzNTkzMGRhYjEyYmQ2MWRkNWNiMWMyIl0=" data-embedUrl="https://daxab.com/player/BKZEG6PpZNJLThfTvvqrwOXa-AdZMMZnN-PXBersiPrv2xEWVEiESys7jGtTsmvwYVppGAZxJf8yaaFEKWCpbYq8iWIYxa4hD4FA3XSAUkw" data-playerHash="BKZEG6PpZNJLThfTvvqrwOXa-AdZMMZnN-PXBersiPrv2xEWVEiESys7jGtTsmvwYVppGAZxJf8yaaFEKWCpbYq8iWIYxa4hD4FA3XSAUkw" data-favHash="a3947601bc8cd944b48dba7d4c2d585a">
<a href="/watch/-188576299_456239332" data-id="-188576299_456239332">
<div class="video-show"></div>
<div class="video-fav" data-hash="a3947601bc8cd944b48dba7d4c2d585a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/URCXZwhxcOo9MTvAksZ6iQ7zMqY_4yjVbXPrjw/vHBpzDTf-Sc.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">918</span>
<span class="video-time">18:02</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Застенчиво-Распутная девушка Сукуми Tsundere Inran Shoujo Sukumi - 01 [720p]</div>
</a>
</div>
<div class="video-item video-901607_162904584" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIFN0ZXAgVXAiLDMyMzAsNzgsImh0dHBzOlwvXC9zdW45LTY3LnVzZXJhcGkuY29tXC9jNTI2NjE2XC91MzA2OTkzMTdcL3ZpZGVvXC9sXzI0NTEyZWQ0LmpwZyIsIjVhYjZhYjY2MzllMGFiYjI4Y2FkMTA1NzlkNGU0Y2Y4Il0=" data-embedUrl="https://daxab.com/player/W69cNCVEIuI2HZEHD8aKroVc28dg8PpU1aRDz7NZDswPB67yAQfd4X08pifArEpBEzMFB8GiNoL7i__KtlcyOQ" data-playerHash="W69cNCVEIuI2HZEHD8aKroVc28dg8PpU1aRDz7NZDswPB67yAQfd4X08pifArEpBEzMFB8GiNoL7i__KtlcyOQ" data-favHash="7c5221b338e8f47aef78cdfd04b38762">
<a href="/watch/-901607_162904584" data-id="-901607_162904584">
<div class="video-show"></div>
<div class="video-fav" data-hash="7c5221b338e8f47aef78cdfd04b38762"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-67.userapi.com/c526616/u30699317/video/l_24512ed4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">78</span>
<span class="video-time">53:50</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Step Up</div>
</a>
</div>
<div class="video-item video-150082602_456239164" data-video="WyLQn9GD0YLQtdGI0LXRgdGC0LLQuNC1INCa0LDRgdGB0LDQvdC00YDRiyAyLiDQmtC+0L3QtdGGINGB0LLQtdGC0LAgMjAxMi4gIyAxIC0g0J/QvtC40YHQutC4INC90LDRh9C40L3QsNGO0YLRgdGPIiwxODY3LDEsImh0dHBzOlwvXC9zdW42LTIwLnVzZXJhcGkuY29tXC9jODMwMzA4XC92ODMwMzA4MTI1XC8xN2E1OWRcL1JUV1Rtb2QxOEJ3LmpwZyIsImFkNDI4MTQyOGMzMmEzZTdkMjRmZTczNjhlOTc5YzExIl0=" data-embedUrl="https://daxab.com/player/2_WF1p823GFzjxDdrNFS8DB1YRlUSKYwrzPH3qzDJigwDj3jkHiwiGI8HyxfNQYOtDCrpYghDBaqVRKweY5I0dtFpFpmZHXQuo-jidPKpvE" data-playerHash="2_WF1p823GFzjxDdrNFS8DB1YRlUSKYwrzPH3qzDJigwDj3jkHiwiGI8HyxfNQYOtDCrpYghDBaqVRKweY5I0dtFpFpmZHXQuo-jidPKpvE" data-favHash="f3e0f335d61cf72c6f3c24bb83dcaaaf">
<a href="/watch/-150082602_456239164" data-id="-150082602_456239164">
<div class="video-show"></div>
<div class="video-fav" data-hash="f3e0f335d61cf72c6f3c24bb83dcaaaf"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/c830308/v830308125/17a59d/RTWTmod18Bw.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">3 years ago</span>
<span class="video-view">1</span>
<span class="video-time">31:07</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Путешествие Кассандры 2. Конец света 2012. # 1 - Поиски начинаются</div>
</a>
</div>
<div class="video-item video-20774214_163637013" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTQgKExpdmUgU2hvdyAyKSIsNjA1OCwxMzcsImh0dHBzOlwvXC9zdW45LTgwLnVzZXJhcGkuY29tXC9jNTI3MzE3XC91OTcyODAxMVwvdmlkZW9cL2xfMjZlOWZhMmEuanBnIiwiNzM1NDA1ZjA5YjcwMTcyMDI3M2U3MDRiNDU5NjJhZTAiXQ==" data-embedUrl="https://daxab.com/player/wtzxPi-T24_XKLlF24k7Xj9ULslZ9pEtpnkboGnvFJMixbwVVYrkXtN8e6JvYlOtZ2eFBju8YY8e-wtigUrvag" data-playerHash="wtzxPi-T24_XKLlF24k7Xj9ULslZ9pEtpnkboGnvFJMixbwVVYrkXtN8e6JvYlOtZ2eFBju8YY8e-wtigUrvag" data-favHash="d462fb2019aed8ca25f27225aac6c177">
<a href="/watch/-20774214_163637013" data-id="-20774214_163637013">
<div class="video-show"></div>
<div class="video-fav" data-hash="d462fb2019aed8ca25f27225aac6c177"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c527317/u9728011/video/l_26e9fa2a.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">137</span>
<span class="video-time">01:40:58</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x14 (Live Show 2)</div>
</a>
</div>
<div class="video-item video-19767527_165512903" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gRm91cnRoIGRheS4gQk1YIEJpZyBBaXIiLDI5NTUsNDcsImh0dHBzOlwvXC9zdW45LTEwLnVzZXJhcGkuY29tXC9jNTA2MTE4XC91ODE5ODkyMDhcL3ZpZGVvXC9sX2VkZmYxMGU4LmpwZyIsIjQ1ZWExY2M0YWQ1MDNkNTQ4ZDI1ZWY3ODk4MTVkODJkIl0=" data-embedUrl="https://daxab.com/player/vKb84we0nBMteef_xrKLzcZNlLqNAJZ1QHdW9gLpwj3hbiDem0EvkpNmycg0S-MEODLG2TQAcLGH5E-wXsEDPQ" data-playerHash="vKb84we0nBMteef_xrKLzcZNlLqNAJZ1QHdW9gLpwj3hbiDem0EvkpNmycg0S-MEODLG2TQAcLGH5E-wXsEDPQ" data-favHash="dde051d566857522cb8860d118466d73">
<a href="/watch/-19767527_165512903" data-id="-19767527_165512903">
 <div class="video-show"></div>
<div class="video-fav" data-hash="dde051d566857522cb8860d118466d73"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-10.userapi.com/c506118/u81989208/video/l_edff10e8.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">47</span>
<span class="video-time">49:15</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Fourth day. BMX Big Air</div>
</a>
</div>
<div class="video-item video-53908459_456239661" data-video="WyJYLdC40LPRgNCwIDIgXC8gWCBHYW1lIDIiLDYxOTUsODEsImh0dHBzOlwvXC9zdW42LTIyLnVzZXJhcGkuY29tXC9jNjM2MjE4XC92NjM2MjE4NDU5XC80ODY0M1wvazFoWEpFWk1SNzQuanBnIiwiYjYzN2IzNzFmNjIxNTUxMThlMzYxZWI2NTE1OGNmMTMiXQ==" data-embedUrl="https://daxab.com/player/y57zCnaZrN9cymJWJ1UX5c8d-ItIXw5skjkrHbRwCIaeGs41sdvHt-IMfBPWr-9DzYXnNqoINUMYIOjYfLEWyQ" data-playerHash="y57zCnaZrN9cymJWJ1UX5c8d-ItIXw5skjkrHbRwCIaeGs41sdvHt-IMfBPWr-9DzYXnNqoINUMYIOjYfLEWyQ" data-favHash="fcc330c464b617c167c9bfb94242a787">
<a href="/watch/-53908459_456239661" data-id="-53908459_456239661">
<div class="video-show"></div>
<div class="video-fav" data-hash="fcc330c464b617c167c9bfb94242a787"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c636218/v636218459/48643/k1hXJEZMR74.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">5 years ago</span>
<span class="video-view">81</span>
<span class="video-time">01:43:15</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-игра 2 / X Game 2</div>
</a>
</div>
<div class="video-item video-901607_162940653" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIE1vdG8gRnJlZXN0eWxlIiwzNzg1LDcwLCJodHRwczpcL1wvc3VuOS00NS51c2VyYXBpLmNvbVwvYzUxODYxNlwvdTMwNjk5MzE3XC92aWRlb1wvbF82MmE2MTVmOS5qcGciLCI4NWYzNjI0MjMxYjJkOTVkZTE1OGQ4MjM5Yzg5OWUxYiJd" data-embedUrl="https://daxab.com/player/RF8-6i8OIjCVErUBfRyjYahSJRO6a59b3_w6hVMQuvhrmugZ_WaeiVG32bhn4BUcJvLcs-iNowoZYHS_bE6U4Q" data-playerHash="RF8-6i8OIjCVErUBfRyjYahSJRO6a59b3_w6hVMQuvhrmugZ_WaeiVG32bhn4BUcJvLcs-iNowoZYHS_bE6U4Q" data-favHash="fccdce282eb9ecdf8bd54f3045922e3c">
<a href="/watch/-901607_162940653" data-id="-901607_162940653">
<div class="video-show"></div>
<div class="video-fav" data-hash="fccdce282eb9ecdf8bd54f3045922e3c"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-45.userapi.com/c518616/u30699317/video/l_62a615f9.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">70</span>
<span class="video-time">01:03:05</span>
</div>
</div>
 <div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Moto Freestyle</div>
</a>
</div>
<div class="video-item video-20774214_163592952" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxXC8gUGFydCAxKSBIRCIsMzM3OSw3MywiaHR0cHM6XC9cL3N1bjktNTQudXNlcmFwaS5jb21cL2M1MjkyMDJcL3UzMDg0OTU1XC92aWRlb1wvbF85Mjc0NDRkMC5qcGciLCJjM2ZmZGZhY2RhNDU5OGJiOTc1MTYwNzc3YTU4ZWRmMSJd" data-embedUrl="https://daxab.com/player/dvbQyrzKznYweXMYEWuumjo-hdQpJwDQ2DQvOdS9t5B79gT_ZhoOy3CCVXc5cIB8EtTtSOEeN8wv73eivavSXQ" data-playerHash="dvbQyrzKznYweXMYEWuumjo-hdQpJwDQ2DQvOdS9t5B79gT_ZhoOy3CCVXc5cIB8EtTtSOEeN8wv73eivavSXQ" data-favHash="3576c9c62c9f70156112ddb8ea7f498e">
<a href="/watch/-20774214_163592952" data-id="-20774214_163592952">
<div class="video-show"></div>
<div class="video-fav" data-hash="3576c9c62c9f70156112ddb8ea7f498e"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-54.userapi.com/c529202/u3084955/video/l_927444d0.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">73</span>
<span class="video-time">56:19</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1/ Part 1) HD</div>
</a>
</div>
<div class="video-item video-190464408_456239270" data-video="WyJTbGVlcGluZyBEb2dzIOKeqCDQn9GA0L7RhdC+0LbQtNC10L3QuNC1IDEyIOKeqCDQntC00L7Qu9C20LXQvdC40Y8g4oSWIDIgI1NsZWVwaW5nRG9ncyAjU3RhcnBlcml5YSIsMjU3NiwxLCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvNzBJZ0FONW1tclFjajFjR0FEZ2RaZkNPSkhVejJPeHFDdVJmUWdcL2w4Y0JfTXpwcVJFLmpwZyIsIjg5M2Y3YmJlMWQ3MjBmNDdjNzRmODg0NGQ0ZjI1NDhlIl0=" data-embedUrl="https://daxab.com/player/w-SVELFnqIiMpiAWZQ1Sfr-d7jFJeX02nfBqUmJXUE4PAFyXrcHBEFw13Lw5e-vQ1yAUV53IgNAKUZNuASI3z6YP8qvfMLK3p69Xg6E5ot0" data-playerHash="w-SVELFnqIiMpiAWZQ1Sfr-d7jFJeX02nfBqUmJXUE4PAFyXrcHBEFw13Lw5e-vQ1yAUV53IgNAKUZNuASI3z6YP8qvfMLK3p69Xg6E5ot0" data-favHash="dfaa105378c66240f1bf25aa8494588d">
<a href="/watch/-190464408_456239270" data-id="-190464408_456239270">
<div class="video-show"></div>
<div class="video-fav" data-hash="dfaa105378c66240f1bf25aa8494588d"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/70IgAN5mmrQcj1cGADgdZfCOJHUz2OxqCuRfQg/l8cB_MzpqRE.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1</span>
<span class="video-time">42:56</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Sleeping Dogs ➨ Прохождение 12 ➨ Одолжения № 2 #SleepingDogs #Starperiya</div>
</a>
</div>
<div class="video-item video384329_164980719" data-video="WyJYLWdhbWVzLCBBc2lhLCBWZXJ0LCAyMDEyIHllYXIiLDI4MzQsMTUsImh0dHBzOlwvXC9zdW45LTIyLnVzZXJhcGkuY29tXC9jNTE4MjEzXC91Mzg0MzI5XC92aWRlb1wvbF82MjEyODI1ZC5qcGciLCJlNzMzZGY4NTdhMGE3ZTczN2FmN2Y1ZDAwMDA4MGI2MCJd" data-embedUrl="https://daxab.com/player/fGXRNnulJoVXj-Ddr53N4Qe3sltDF1ABhesATb2G8EPI2iS02typMN45GoswLt3Ewo06XoFSre_L9o7q6CV80A" data-playerHash="fGXRNnulJoVXj-Ddr53N4Qe3sltDF1ABhesATb2G8EPI2iS02typMN45GoswLt3Ewo06XoFSre_L9o7q6CV80A" data-favHash="e5a881faca6da5f106ae9f0095b97d3a">
<a href="/watch/384329_164980719" data-id="384329_164980719">
<div class="video-show"></div>
<div class="video-fav" data-hash="e5a881faca6da5f106ae9f0095b97d3a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-22.userapi.com/c518213/u384329/video/l_6212825d.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">15</span>
<span class="video-time">47:14</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games, Asia, Vert, 2012 year</div>
</a>
</div>
<div class="video-item video-12783167_170084732" data-video="WyJYLUdhbWVzIDIwMTIgUmFsbHktQ3Jvc3MiLDU3MDAsMzYsImh0dHBzOlwvXC9zdW45LTM1LnVzZXJhcGkuY29tXC9jNTEzMzEyXC91MTQzOTcyOTQ5XC92aWRlb1wvbF8xMmM1MzFmNi5qcGciLCI5MDQ1MDMyMmY4MjA1OWQzMmNhNjFmNWFkMDUzNzMyYiJd" data-embedUrl="https://daxab.com/player/pcaGEa5zT9S8eIxvcuOo95w78JuTKstM8t8oDjadFet50aFgzYdyAW7gPU-M9mVwjGS4bjBvEKh0AfRAUv0t9g" data-playerHash="pcaGEa5zT9S8eIxvcuOo95w78JuTKstM8t8oDjadFet50aFgzYdyAW7gPU-M9mVwjGS4bjBvEKh0AfRAUv0t9g" data-favHash="a6401941b889bf3e78bc1df3df07fa72">
<a href="/watch/-12783167_170084732" data-id="-12783167_170084732">
<div class="video-show"></div>
<div class="video-fav" data-hash="a6401941b889bf3e78bc1df3df07fa72"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-35.userapi.com/c513312/u143972949/video/l_12c531f6.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 years ago</span>
<span class="video-view">36</span>
<span class="video-time">01:35:00</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games 2012 Rally-Cross</div>
</a>
</div>
<div class="video-item video-19767527_165206109" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gRmlyc3QiLDMxNDYsMjgsImh0dHBzOlwvXC9zdW45LTgyLnVzZXJhcGkuY29tXC9jNTMyMzA0XC91ODE5ODkyMDhcL3ZpZGVvXC9sXzYwNWY0ZmQ0LmpwZyIsIjdkZmU1MDE3MjJhNzMzZGFiZGZmZjMyM2M2Njk3NDUwIl0=" data-embedUrl="https://daxab.com/player/kpkImdfhiLa7Y9yJAOoOL19XA5mRLtxVYbTm03dLFS_0ZWArrnahwBdbv1bWS3L60k5I0V-NW3mxsN__Er1RgA" data-playerHash="kpkImdfhiLa7Y9yJAOoOL19XA5mRLtxVYbTm03dLFS_0ZWArrnahwBdbv1bWS3L60k5I0V-NW3mxsN__Er1RgA" data-favHash="cb12f1c853d023943344fa576e1d169a">
<a href="/watch/-19767527_165206109" data-id="-19767527_165206109">
<div class="video-show"></div>
<div class="video-fav" data-hash="cb12f1c853d023943344fa576e1d169a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-82.userapi.com/c532304/u81989208/video/l_605f4fd4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">28</span>
<span class="video-time">52:26</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. First</div>
</a>
</div>
<div class="video-item video-19767527_165214922" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gVGhyaWQgZGF5LiBCTVggU3RyZWV0IEZpbmFsIiwxMjcwLDExLCJodHRwczpcL1wvc3VuOS00Ni51c2VyYXBpLmNvbVwvYzUzNTUxMlwvdTgxOTg5MjA4XC92aWRlb1wvbF82NmRkZGZmYy5qcGciLCI5MGU1YjNiZGU4MjNhMjhkNTFlMzRiOGU3NmU2MzYzNiJd" data-embedUrl="https://daxab.com/player/jKEKyaZeaN0DY78rtsG9F8TRiNt0oDGnAYZwkMupKT4Cc4GtHo6ZoM5pXPkMF_oP_0dOTn1JVEgo5iXDSdOZFA" data-playerHash="jKEKyaZeaN0DY78rtsG9F8TRiNt0oDGnAYZwkMupKT4Cc4GtHo6ZoM5pXPkMF_oP_0dOTn1JVEgo5iXDSdOZFA" data-favHash="a89d88f3a3ec1b0d933a35869c8a565a">
<a href="/watch/-19767527_165214922" data-id="-19767527_165214922">
<div class="video-show"></div>
<div class="video-fav" data-hash="a89d88f3a3ec1b0d933a35869c8a565a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-46.userapi.com/c535512/u81989208/video/l_66dddffc.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">11</span>
<span class="video-time">21:10</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Thrid day. BMX Street Final</div>
</a>
</div>
<div class="video-item video-901607_162943115" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIFJldmlldyIsNTM4MSwxNiwiaHR0cHM6XC9cL3N1bjktODcudXNlcmFwaS5jb21cL2M1MjcyMTZcL3UzMDY5OTMxN1wvdmlkZW9cL2xfZDhiOTA2ZjQuanBnIiwiM2MyY2QzNDFhMTM4ODlkNjcxNjJmNmFlMTU5ZDBlMTIiXQ==" data-embedUrl="https://daxab.com/player/f241LryL_vYA1InT3MlfIS1rZZsecAzi3LAXsiJlQj4pkeAZROvdNrB2C99whHP5HYawCIFZ_K5ZBWSu_EOCpA" data-playerHash="f241LryL_vYA1InT3MlfIS1rZZsecAzi3LAXsiJlQj4pkeAZROvdNrB2C99whHP5HYawCIFZ_K5ZBWSu_EOCpA" data-favHash="f9d2d9c63926cc6aedd852f38b190488">
<a href="/watch/-901607_162943115" data-id="-901607_162943115">
<div class="video-show"></div>
<div class="video-fav" data-hash="f9d2d9c63926cc6aedd852f38b190488"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-87.userapi.com/c527216/u30699317/video/l_d8b906f4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">16</span>
<span class="video-time">01:29:41</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Review</div>
</a>
</div>
<div class="video-item video-19767527_165214946" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gVGhyaWQgZGF5LiBCTVggVmVydCIsOTkzLDg0LCJodHRwczpcL1wvc3VuOS03OS51c2VyYXBpLmNvbVwvYzUxNDUwNFwvdTgxOTg5MjA4XC92aWRlb1wvbF9jZDk1NjI3Ny5qcGciLCIwZjVhMDQ4NzkwYTM2NWYxYzNlNzUyNWI3NGIxYjQ1ZCJd" data-embedUrl="https://daxab.com/player/ZgIImb6xj40OOQGHh4reKRLMKIt4gZWcRWCMF5FKfVrVRfRXRy5n1URfG-nFBHtyc3WwTmVIySfWh522sqE8wg" data-playerHash="ZgIImb6xj40OOQGHh4reKRLMKIt4gZWcRWCMF5FKfVrVRfRXRy5n1URfG-nFBHtyc3WwTmVIySfWh522sqE8wg" data-favHash="8af51ac89ba5920efcbea5001540648a">
<a href="/watch/-19767527_165214946" data-id="-19767527_165214946">
<div class="video-show"></div>
<div class="video-fav" data-hash="8af51ac89ba5920efcbea5001540648a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-79.userapi.com/c514504/u81989208/video/l_cd956277.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">84</span>
<span class="video-time">16:33</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Thrid day. BMX Vert</div>
</a>
</div>
<div class="video-item video581608605_456239155" data-video="WyJSb2NrIHRoZSBWb3RlISAtIC0gUHJvcGFnYW5kYVdhdGNoICggMTQ0IFggMTQ0ICkubXA0IiwxMzE1LDE4NCwiaHR0cHM6XC9cL3N1bjYtMjMudXNlcmFwaS5jb21cLzFwdVZmMjRaVlMyd0VvNldVb3lJdV9EM2hQY1I1S1BoMWd0Uk93XC9WY2txVzB6WEVlUS5qcGciLCI3OGJlNzFlOTdmZWU0ODgwZmUxNzE5NWM1YTkxMGRjYyJd" data-embedUrl="https://daxab.com/player/0btEdTT6fApwBtyrhoehfFwSi4aaSTdlS-WK7EtIxHeoC_DPuWvCMCwnsR9tkBpqwkp-cG8qRu9bG8WXzyW7AA" data-playerHash="0btEdTT6fApwBtyrhoehfFwSi4aaSTdlS-WK7EtIxHeoC_DPuWvCMCwnsR9tkBpqwkp-cG8qRu9bG8WXzyW7AA" data-favHash="77b9ed060e5f68bd61219753639c34cf">
<a href="/watch/581608605_456239155" data-id="581608605_456239155">
<div class="video-show"></div>
<div class="video-fav" data-hash="77b9ed060e5f68bd61219753639c34cf"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/1puVf24ZVS2wEo6WUoyIu_D3hPcR5KPh1gtROw/VckqW0zXEeQ.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">184</span>
<span class="video-time">21:55</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Rock the Vote! - - PropagandaWatch ( 144 X 144 ).mp4</div>
</a>
</div>
<div class="video-item video-20774214_163592955" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxXC8gUGFydCAyKSBIRCIsMzMxMyw0MCwiaHR0cHM6XC9cL3N1bjktNjEudXNlcmFwaS5jb21cL2M1MTg2MDJcL3UzMDg0OTU1XC92aWRlb1wvbF83MmE0MTdiMi5qcGciLCI2MTM1NjNhMzdkZTY3NThjY2FiYzk2Njc5NzljZDg0OSJd" data-embedUrl="https://daxab.com/player/1nP5r1jXLtueJ4CPoM02uk-OvhOl8ZhJpjMcYGllDaqW_LXEmacQevuPWS_Bhiu-vytjO7A4c50CzvD83wS6Gw" data-playerHash="1nP5r1jXLtueJ4CPoM02uk-OvhOl8ZhJpjMcYGllDaqW_LXEmacQevuPWS_Bhiu-vytjO7A4c50CzvD83wS6Gw" data-favHash="d8daed6fac8e218db9f4e78de3e73b29">
<a href="/watch/-20774214_163592955" data-id="-20774214_163592955">
<div class="video-show"></div>
<div class="video-fav" data-hash="d8daed6fac8e218db9f4e78de3e73b29"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-61.userapi.com/c518602/u3084955/video/l_72a417b2.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">40</span>
<span class="video-time">55:13</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1/ Part 2) HD</div>
</a>
</div>
<div class="video-item video-4296410_167364578" data-video="WyJYLWdhbWVzIDIwMTIgYm14IHZlcnQgZmluYWwgKCDQvdCwINGA0YPRgdGB0LrQvtC8KSIsOTkzLDYyLCJodHRwczpcL1wvc3VuOS0zMy51c2VyYXBpLmNvbVwvYzUyMzYwMFwvdTk3MDA0ODdcL3ZpZGVvXC9sX2NkYmZhMWUyLmpwZyIsIjE5NmVhYWE3NzE2MDhiYmJkYzc4ZjE5YTg3MDk5NDVjIl0=" data-embedUrl="https://daxab.com/player/D3cAhrquuGmxBhAaVDKaWWWmKTbmReZPfP7_rBUaWE3D3GzMsV7o59O8r8JMSml7di_njCDtHAOsn7L52qK8kQ" data-playerHash="D3cAhrquuGmxBhAaVDKaWWWmKTbmReZPfP7_rBUaWE3D3GzMsV7o59O8r8JMSml7di_njCDtHAOsn7L52qK8kQ" data-favHash="ed545ebb89984148892cfa8ee7c45ef8">
<a href="/watch/-4296410_167364578" data-id="-4296410_167364578">
<div class="video-show"></div>
<div class="video-fav" data-hash="ed545ebb89984148892cfa8ee7c45ef8"></div>
 <div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-33.userapi.com/c523600/u9700487/video/l_cdbfa1e2.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">8 years ago</span>
<span class="video-view">62</span>
<span class="video-time">16:33</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012 bmx vert final ( на русском)</div>
</a>
</div>
<div class="video-item video-145081708_456242402" data-video="WyLQuNC1IEJvcmRlcmxhbmRzIEdPVFkgWzIwMDldIFBDXC9QUzNcL1hib3gzNjAuINCf0YDQvtGF0L7QttC00LXQvdC40LUg0LfQsCDQm9C40LvQuNGCINGHLjggKzE4IFtkYXIiLDI2NDcsMTE4LCJodHRwczpcL1wvaS5teWNkbi5tZVwvZ2V0VmlkZW9QcmV2aWV3P2lkPTEzMjIzNzY4MjU1NjMmaWR4PTEmdHlwZT0zOSZ0a249U3VPTUVURDh2R1dRMTNIaHdhdzI0WmhoX0xRJmZuPXZpZF9sIiwiZmRkNjMwM2Y3YTczNzUxYjlkZGQzZGFiYjE5NGEwNzYiXQ==" data-embedUrl="https://daxab.com/player/zfHuJPYSlWkLie9M0yZxZ5x4CqGeh36eVYL5pljfb5TX2k44L58Q-WGDjCJI2_LI87NEXpDf0UIFN1Cph0wE_7JuEfT0Ub1gc99AWDp__lQ" data-playerHash="zfHuJPYSlWkLie9M0yZxZ5x4CqGeh36eVYL5pljfb5TX2k44L58Q-WGDjCJI2_LI87NEXpDf0UIFN1Cph0wE_7JuEfT0Ub1gc99AWDp__lQ" data-favHash="697a4941bacc445326c3e7e84ee48753">
<a href="/watch/-145081708_456242402" data-id="-145081708_456242402">
<div class="video-show"></div>
<div class="video-fav" data-hash="697a4941bacc445326c3e7e84ee48753"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://i.mycdn.me/getVideoPreview?id=1322376825563&idx=1&type=39&tkn=SuOMETD8vGWQ13Hhwaw24Zhh_LQ&fn=vid_l"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">118</span>
<span class="video-time">44:07</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">ие Borderlands GOTY [2009] PC/PS3/Xbox360. Прохождение за Лилит ч.8 +18 [dar</div>
</a>
</div>
<div class="video-item video104849236_456277453" data-video="WyJNQVggUEFZTkUgMyBOUEMgV2FycyAyMyAoVUZFIHZzIENyYWNoYSBQcmV0bykiLDE4ODMsMSwiaHR0cHM6XC9cL3N1bjYtMjIudXNlcmFwaS5jb21cL1d4d08yUXhIVUk4Qm5qaks1R3U2TGQxMGlLVWZDYkFpQTVMRjlnXC9zTGdBSEQ4U0lwOC5qcGciLCI5ZTJkMjU5Mzk5YmZhYzBhNzU1YWVmOGMxNDE1ZjhhZSJd" data-embedUrl="https://daxab.com/player/wUOiDxuw-k93B_BIdCsRVFy5ik9jbrKhpadH6Gdf1yYqp5BsU5qAnJVQ0xsWKT_KrGig2AVgMRKy3vrW5YnTsw" data-playerHash="wUOiDxuw-k93B_BIdCsRVFy5ik9jbrKhpadH6Gdf1yYqp5BsU5qAnJVQ0xsWKT_KrGig2AVgMRKy3vrW5YnTsw" data-favHash="daff3e6ec27aa95eabcde1e74d2bdced">
<a href="/watch/104849236_456277453" data-id="104849236_456277453">
<div class="video-show"></div>
<div class="video-fav" data-hash="daff3e6ec27aa95eabcde1e74d2bdced"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/WxwO2QxHUI8BnjjK5Gu6Ld10iKUfCbAiA5LF9g/sLgAHD8SIp8.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 months ago</span>
<span class="video-view">1</span>
<span class="video-time">31:23</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">MAX PAYNE 3 NPC Wars 23 (UFE vs Cracha Preto)</div>
</a>
</div>
<div class="video-item video-901607_162902722" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuINCe0LHQt9C+0YAg0L/QtdGA0LLQvtCz0L4g0LTQvdGPIiwyNjkyLDE0LCJodHRwczpcL1wvc3VuOS0zMy51c2VyYXBpLmNvbVwvYzUyNjUwN1wvdTMwNjk5MzE3XC92aWRlb1wvbF8xYWFlYTc5ZC5qcGciLCJjNjU2ODIyYzQ3YzY3MTEyM2U5Y2QxYzYxMWZhOTQzMyJd" data-embedUrl="https://daxab.com/player/Va58pCdOxe1vs2lJd3-x5E2ku8DqO4nMhAD7NVn-HnvEeazFWBIbxrsGI9p22YulSaVbfM8_gjYR3wlNM87Ajg" data-playerHash="Va58pCdOxe1vs2lJd3-x5E2ku8DqO4nMhAD7NVn-HnvEeazFWBIbxrsGI9p22YulSaVbfM8_gjYR3wlNM87Ajg" data-favHash="ad3b2031e76f2b45329c183030b3b5e2">
<a href="/watch/-901607_162902722" data-id="-901607_162902722">
<div class="video-show"></div>
<div class="video-fav" data-hash="ad3b2031e76f2b45329c183030b3b5e2"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-33.userapi.com/c526507/u30699317/video/l_1aaea79d.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">14</span>
<span class="video-time">44:52</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Обзор первого дня</div>
</a>
</div>
<div class="video-item video593543_162951285" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIEJlc3QgVHJpY2siLDI0NTcsMjUsImh0dHBzOlwvXC9zdW45LTY5LnVzZXJhcGkuY29tXC9jNTI3MTA3XC91MzA2OTkzMTdcL3ZpZGVvXC9sXzk5ZGM1MjljLmpwZyIsIjBmODBkOWU2YWYzNTQ2NDY0YTdmZTE3OTRkYjk1YjVmIl0=" data-embedUrl="https://daxab.com/player/jb5NxdASCt46cqAN1YecXOX62HuWz48A8PC7Q8kSCyGVFZcBpKuWY4E-qUlRrUnSRkhIxtJmHViXeBuM07qoTQ" data-playerHash="jb5NxdASCt46cqAN1YecXOX62HuWz48A8PC7Q8kSCyGVFZcBpKuWY4E-qUlRrUnSRkhIxtJmHViXeBuM07qoTQ" data-favHash="d6ab0848c96f33c96ad3032c4a0d0637">
<a href="/watch/593543_162951285" data-id="593543_162951285">
<div class="video-show"></div>
<div class="video-fav" data-hash="d6ab0848c96f33c96ad3032c4a0d0637"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-69.userapi.com/c527107/u30699317/video/l_99dc529c.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">25</span>
<span class="video-time">40:57</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Best Trick</div>
</a>
</div>
<div class="video-item video-197946026_456239614" data-video="WyLQn9GA0L7RhdC+0LbQtNC10L3QuNC1IFNsZWVwaW5nIERvZ3MgKDIwMTMpICMyIC0g0JrQu9GD0LEgXCLQkdCw0Lwt0JHQsNC8XCIiLDczMDgsNDMsImh0dHBzOlwvXC9zdW42LTIzLnVzZXJhcGkuY29tXC9xMGxBdlpKd21wZE9samtzd3BZYW5MeFpKUl9aTDF6YjB1RXdfZ1wvYllXZE50M05xSDAuanBnIiwiYmU2NjkwN2I3ZGJjNzY1YjVkNzE5MjU5ZWQ0NTZmMDEiXQ==" data-embedUrl="https://daxab.com/player/TXZ-ViI2w3D5qa8lVvJ4kdrDCOC6tTBW4SOfBg64VxvgKQ00TT4VmvAVUG2T3FjkNHePP97Jnfo3dPF6j4PnzuUkDFmDnAamCMlnsDxfyX0" data-playerHash="TXZ-ViI2w3D5qa8lVvJ4kdrDCOC6tTBW4SOfBg64VxvgKQ00TT4VmvAVUG2T3FjkNHePP97Jnfo3dPF6j4PnzuUkDFmDnAamCMlnsDxfyX0" data-favHash="004476a96ec3d868483e77418cf244f2">
<a href="/watch/-197946026_456239614" data-id="-197946026_456239614">
<div class="video-show"></div>
<div class="video-fav" data-hash="004476a96ec3d868483e77418cf244f2"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/q0lAvZJwmpdOljkswpYanLxZJR_ZL1zb0uEw_g/bYWdNt3NqH0.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">4 months ago</span>
<span class="video-view">43</span>
<span class="video-time">02:01:48</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Прохождение Sleeping Dogs (2013) #2 - Клуб &quot;Бам-Бам&quot;</div>
</a>
</div>
<div class="video-item video-27575945_163557091" data-video="WyJYLWdhbWVzIDIwMTIs0KTRgNC40YHRgtCw0LnQuyzQn9C+0LvQvdC+0YHRgtGM0Y4hIiwzNzg1LDEzLCJodHRwczpcL1wvc3VuOS04MC51c2VyYXBpLmNvbVwvYzUyNjIxNVwvdTU5NDM1OTcxXC92aWRlb1wvbF84YThkNGI0NS5qcGciLCI3OTMwYzNkY2Y5NzdkZDhiYWM1ZWQ1NDJiYTkwZGIyOCJd" data-embedUrl="https://daxab.com/player/vUH3ss63lC0Do0cs-bk0oEcq_X5RFXhsUz4RC8Gz1hJhCiSVjAHyTqDqjswCxU4PAfZF3TP3mhHT-jutCS3e6g" data-playerHash="vUH3ss63lC0Do0cs-bk0oEcq_X5RFXhsUz4RC8Gz1hJhCiSVjAHyTqDqjswCxU4PAfZF3TP3mhHT-jutCS3e6g" data-favHash="9795118eba65377777021ab858d2594b">
<a href="/watch/-27575945_163557091" data-id="-27575945_163557091">
<div class="video-show"></div>
<div class="video-fav" data-hash="9795118eba65377777021ab858d2594b"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c526215/u59435971/video/l_8a8d4b45.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">13</span>
<span class="video-time">01:03:05</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012,Фристайл,Полностью!</div>
</a>
</div>
<span class="standalonead" style="margin: 0px 13px;padding-bottom: 15px;">
<iframe width="300" height="250" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10002729?time=1555363895445" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10002729"></iframe>
</span>
<div class="more-similar" onclick="event.cancelBubble = true; loadMoreSimilar(this);" data-page="1" data-video="-53425992_165397466" data-hash="84d987ceaf814a97d050f983234f2315d5157387">Show more</div>
</div>

<script>
$('.tabs > div.tab-btn').on('click', function() {
  if ($(this).hasClass('tab-show')) {
    return;
  }

  $(this).parent().children('div').removeClass('tab-show');
  $(this).addClass('tab-show');

  $('.tab-result').removeClass('tab-show');
  $('.tab-result.' + this.id).addClass('tab-show');

  // if ($('.width').width() < 1020 && this.id == 'tab_comments') {
  //   loadComments($('#video_hash').val().trim());
  // }
});
</script>
</div>
</div>
</div>
<div class="footer">
<div class="width">
<div class="now">
<b>You might be interested</b>
<span><a href="/video/Ashley Adams">Ashley Adams</a> <a href="/video/Anal">Anal</a> <a href="/video/Dpp Dp">Dpp Dp</a> <a href="/video/Milly">Milly</a> <a href="/video/Mandy Rhea">Mandy Rhea</a> <a href="/video/Belle Clair">Belle Clair</a> <a href="/video/Fake Taxi">Fake Taxi</a> <a href="/video/Johanna Gonzalez">Johanna Gonzalez</a> <a href="/video/Jacquieetmichel">Jacquieetmichel</a> <a href="/video/Lisa Ann A">Lisa Ann A</a> <a href="/video/Mom In Bed">Mom In Bed</a> <a href="/video/Webseries">Webseries</a> <a href="/video/Lola Fae Bangcom">Lola Fae Bangcom</a> <a href="/video/Trans500">Trans500</a> <a href="/video/Mya Lane">Mya Lane</a> <a href="/video/Melissa Moore">Melissa Moore</a> <a href="/video/Porncube Порно">Porncube Порно</a> <a href="/video/Francesca Le">Francesca Le</a> <a href="/video/Sapphire Lapiedra">Sapphire Lapiedra</a> <a href="/video/Danny D">Danny D</a></span>
</div>
<div class="clear"></div>
<div class="copy">
<a href="/">DaftSex</a>
<a href="/browse">Browse</a>
<a href="/hottest">Hottest</a>
<a href="/categories">Category</a>
<a href="/pornstars">Pornstars</a>
<a href="/holders">DMCA</a>
<a href="/privacy">Privacy</a>
<a href="/faq">FAQ</a>
<a href="/contact">Contact Us</a>
<a href="https://daft.sex/" target="_blank" title="Start search Page for DaftSex">DaftSex Search</a>
<a href="https://theporndude.com/" target="_blank" rel="nofollow" style="font-weight: bold;">Best Porn Sites</a>
<a href="https://twitter.com/DaftPost" target="_blank" class="twitter font-weight-bold" style="background: #01abf1 url(/img/twitter.png) no-repeat scroll center center;border-radius: 25px;color: transparent;background-size: 27px;"><i class="icon-twistter"></i>Twitter</a>
</div>
</div>
</div>
</div>
<div class="bg_layer"></div>

<script>
onPageLoaded();
$(window).load(onPageReady);
previewEvents();
</script>
<script type='text/javascript' src='//sufficientretiredbunker.com/a5/96/32/a59632dda777535e591fa2e7dde66a93.js'></script>
<script type="text/javascript" async src="/js/app.ve1c0c4626c025980.js"></script>
<script type="text/javascript">
!function(){var w,n=window.outerWidth-window.innerWidth,o=window.outerHeight-window.innerHeight,t=$(window).width(),e=$(window).height();function i(i){w&&clearTimeout(w),w=setTimeout(function(){var i=window.outerWidth-window.innerWidth,w=window.outerHeight-window.innerHeight;n!==i&&o!==w||n==i&&o==w||t==$(window).width()&&e==$(window).height()||t!==$(window).width()&&e!==$(window).height()||!winFullscreen&&(400<i&&n<i&&400<Math.abs(i-n)||200<w&&o<w&&200<Math.abs(w-o))&&(setCookie("_dt",1,{path:"/",expires:1800,domain:"."+location.hostname}),dt()),n=window.outerWidth-window.innerWidth,o=window.outerHeight-window.innerHeight,t=$(window).width(),e=$(window).height()},500)}browser.mobile||$(window).off("resize",i).on("resize",i)}();
</script>

<div style="display:none;">
<script type="text/javascript">
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
</script>
</div>


</body>
</html>
<!DOCTYPE html>
<html>
<head data-extra="aIX2DL3JIGnO7i8k6HvdT4">
<meta charset="UTF-8">
<title>2012 X-Games 18 motocross best trick — DaftSex</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="/index.js?6" defer></script>
<link rel="manifest" href="/manifest.webmanifest">
<meta name="robots" content="all" />
<meta name="robots" content="noarchive" />
<meta name="description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality">
<meta name="keywords" content="Watch, video, daftsex, motocross, best, trick, daftsex">
<meta property="og:url" content="https://daftsex.com/watch/-53425992_165397466" />
<meta property="og:type" content="video.movie" />
<meta property="og:title" content="2012 X-Games 18 motocross best trick" />
<meta property="og:description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality" />
<meta property="og:image" content="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg" />
<meta property="og:video:type" content="text/html" />
<link href="/css/robotocondensed.v4b6944ca5bbf3c8b.css" rel="stylesheet" type="text/css">
<link rel="icon" type="image/png" sizes="196x196" href="/img/daftlogo196x196.png?2">
<link rel="shortcut icon" href="/img/favicon.ico?5" />
<meta name="theme-color" content="#C00">
<link rel="stylesheet" href="/css/common.v112268244c82433e.css" />
<link rel="stylesheet" href="/css/icons.v38e1854b6d41caec.css" />
<link rel="stylesheet" href="/css/dark.ve32b1eba318e418e.css" />
<script type="text/javascript">
window.globEmbedUrl = 'https://daxab.com/player/';
window.timeNow = 1658940075;
setInterval(function() {
  window.timeNow++;
}, 1000);
window.liteopen = false;
window.is_logged = false;
</script>
<script type="text/javascript" src="/js/jquery-2.1.1.min.v18b7e87c91d98481.js"></script>
<script type="text/javascript" src="/js/history.v955089448af5a0c8.js"></script>
<script type="text/javascript" src="/js/nprogress.v3410974b8841b4f3.js"></script>
<script type="text/javascript" src="/js/nouislider.v2192f61dc764023a.js"></script>
<script type="text/javascript" src="/js/select.ve363dc0076d2c78b.js"></script>
<script type="text/javascript" src="/js/common.vb1f31c4b59a9e4d1.js"></script>
<script type="text/javascript" src="/js/auth.vfa3c32a15fba2304.js"></script>
<script type="text/javascript" src="/js/jquery.mutations.min.v4b147b138a5b1019.js"></script>
<script type="text/javascript" src="/js/fav.v764365b62392eb58.js"></script>
<script type="text/javascript" src="/js/share.vdf8ddf291dc2f417.js"></script>
<script type="text/javascript" src="/js/likes.v5e342c5feda70804.js"></script>
<script type="text/javascript">
window._stv = '67c5c441c9e2';
window.log_version = 'fbcb78248ac55029';
</script>
</head>
<body>
<a class="up" onclick="toTop();">&uarr;</a>
<div class="page_wrapper">
<div class="header">
<div class="width">
<div class="header_inner">
<a onclick="window.history.go(-1); return false;" title="Back" class="back-logo"><i class="icon-left"></i></a>
<a href="/" title="DaftSex" class="logo">
<svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="55.000000pt" height="50.000000pt" viewBox="0 0 311.000000 127.000000" preserveAspectRatio="xMidYMid meet">
<g transform="translate(0.000000,180.000000) scale(0.100000,-0.100000)" fill="#dd3333" stroke="none">
<path d="M895 1713 c-142 -78 -362 -148 -559 -178 -54 -9 -102 -18 -107 -21
-14 -8 -10 -228 5 -354 36 -284 110 -501 235 -692 110 -167 280 -320 442 -398
l46 -23 54 27 c149 75 311 216 408 356 59 84 131 221 168 320 35 92 79 287 93
410 12 101 18 346 8 355 -2 2 -49 11 -104 19 -215 33 -424 99 -578 184 l-51
29 -60 -34z m15 -818 l0 -636 -45 31 c-128 88 -246 229 -327 390 -86 171 -148
427 -148 612 0 72 2 78 23 83 209 46 339 85 447 134 25 11 46 21 48 21 1 0 2
-286 2 -635z m197 592 c99 -40 258 -87 343 -102 79 -14 74 -4 67 -133 -4 -63
-11 -120 -16 -125 -5 -5 -38 -6 -83 -1 l-73 9 3 46 3 45 -63 13 c-35 6 -75 14
-90 18 l-28 5 0 -95 c0 -81 3 -96 18 -102 9 -3 81 -9 160 -12 78 -3 142 -10
142 -15 0 -14 -39 -155 -61 -221 -35 -101 -118 -253 -188 -340 -65 -79 -226
-221 -242 -212 -8 5 -12 501 -5 514 3 4 43 2 90 -3 l85 -9 3 -41 3 -41 37 65
c59 105 64 96 -61 104 -61 3 -122 11 -136 16 l-25 10 0 325 c0 179 2 325 4
325 2 0 53 -20 113 -43z" />
<path d="M628 1242 c-79 -19 -78 -15 -53 -153 18 -102 51 -201 98 -294 69
-137 66 -145 67 178 l0 287 -22 -1 c-13 -1 -53 -9 -90 -17z" />
</g>
</svg>
</a>
<div class="clear-form"><i class="icon-cancel"></i></div>
<form class="search-form" onsubmit="return search(true);">
<input type="text" id="q" autocomplete="off" value="" placeholder="Search millions of videos...">
<a title="RandomTV" href="/video/Jade Venus" class="randomQuery"><span class="deskrandom"></span></a>
<button type="submit" title="Search button">Search</button>
<div class="head-menu"></div>
</form>
<div class="head-menu module-browse"><a href="/browse">Browse</a></div>
<div class="head-menu module-hottest"><a href="/hottest">Hottest</a></div>
<div class="head-menu module-categories"><a href="/categories">Category</a></div>
<div class="head-menu module-pornstars"><a href="/pornstars">Pornstars</a></div>
<div class="head-menu"><a href="https://theporndude.com/" target="_blank" rel="nofollow" style="color: #000;">Best Porn Sites</a></div>
<button class="btn-search" type="button"><i class="icon-left"></i></button>
<span class="hide-item">

<a href="/pornstars"><span class="btn-pornstars"></span></a>
<a href="/categories"><span class="btn-catt"></span></a>
<a href="/hottest"><span class="btn-hottest"></span></a>
<a href="/browse"><span class="btn-browse"></span></a>
</span>
<span class="auth login hide-item" title="Login" onclick="Auth.Login();"></span>
</div>
</div>
<div id="progress_content"></div>
<div class="suggest_search"><div class="suggest_inner width"><ul class="suggest_items"></ul></div></div>
</div>
<div class="bg_width">
<div class="width">

<div class="aboveVideo standalonead" style="text-align: center;padding-top: 75px;margin-bottom: -70px;">

<span style="
    text-align: center;
"> <a style="
    text-align: left;
    color: #fff;
    font-size: 15px;
    text-decoration: none;
    padding: 5px;
    top: 61px;
    margin: 5px;
    border-radius: 25px;
    display: block;
    background-color: #5e62ff;
    " href="https://artsporn.com" target="_blank">
ℹ: ARTSPORN.COM - Another new Alternative, faster website without ads. 👊⚡️⚡️⚡️</a></span>


<iframe width="300" height="150" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10009021?time=1575323689465" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10012984"></iframe>




</div>
<div class="aboveVideoPC standalonead" style="text-align: center;padding-top: 75px;margin-bottom: -70px;">








</div>
<div class="content-wrapper" id="content">
<div itemscope itemtype="http://schema.org/VideoObject">
<link itemprop="thumbnailUrl" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
<span itemprop="thumbnail" itemscope itemtype="http://schema.org/ImageObject">
<link itemprop="url" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
<link itemprop="contentUrl" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
</span>
<meta itemprop="name" content="2012 X-Games 18 motocross best trick" />
<meta itemprop="uploadDate" content="2013-07-17T16:03:39+04:00">
<meta itemprop="description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality" />
<meta itemprop="duration" content="T28M50S" />
</div>

<div class="video">
<a class="standalonead" style="font-size: 14px; font-weight: bold;color:#0a0a0a; text-align: center;" href="https://landing.brazzersnetwork.com/?ats=eyJhIjoyOTIyOTcsImMiOjU3NzAzNTQ4LCJuIjoxNCwicyI6OTAsImUiOjg4MDMsInAiOjU3fQ==" target="_blank" rel="nofollow"><div class="message-container" style="background-color:#ffd001;padding: 7px;font-size: 17px;font-weight: 700;"><span class="nomobile">EXCLUSIVE DaftSex OFFER - Join BRAZZERS Only 1$ Today ! [PROMO]</span> <span class="nomobile"><span style="color:rgba(255, 208, 1)"></span><span style="color:rgba(255, 208, 1)"><span></span></span></span></div></a>
<div style="background:#000000;outline: 0px solid #fff;" class="frame_wrapper">
<script id="data-embed-video--za4958UIcdmtirzE7-10AReaELDU1V4DI6bYOZT6RQO2NIOX0xgCMnHYczIf2FS3oag2fkPA58mVJgEictI163pcysY2edhuP9aIiBY_E0">
if (window.DaxabPlayer && window.DaxabPlayer.Init) {
  DaxabPlayer.Init({
    id: 'video-53425992_165397466',
    origin: window.globEmbedUrl.replace('/player/', ''),
    embedUrl: window.globEmbedUrl,
    hash: "-za4958UIcdmtirzE7-10AReaELDU1V4DI6bYOZT6RQO2NIOX0xgCMnHYczIf2FS3oag2fkPA58mVJgEictI163pcysY2edhuP9aIiBY_E0",
    color: "f12b24",
    styles: {
      border: 0,
      overflow: 'hidden',
      marginBottom: '-5px'
    },
    attrs: {
      frameborder: 0,
      allowFullScreen: ''
    },
    events: function(data) {
      if (data.event == 'player_show') {
        dt();

        HistoryWatch.set("-53425992_165397466", "WyIyMDEyIFgtR2FtZXMgMTggbW90b2Nyb3NzIGJlc3QgdHJpY2siLDE3MzAsNjIsImh0dHBzOlwvXC9zdW45LTQ2LnVzZXJhcGkuY29tXC9jNTM1MzA0XC91NjI1MjQ2NVwvdmlkZW9cL2xfZGQ5MzBlOGYuanBnIiwiODc4OGE3MDgzNDAzZWNkOTg0ZWNiOGExNThhMmJjN2IiXQ==");
      }
    }
  });
}
</script>
</div>
<div class="video_info_wrapper">
<h1 class="heading">2012 X-Games 18 motocross best trick</h1>
<div class="voting-module video-watch">
<span class="icon-container">
<span class="icon icon1"></span>
<span class="icon icon2 hidden-vote"></span>
</span>
<span class="text-container">
<span class="text text1"></span>
<span class="text text2"></span>
</span>
</div>
<script>
var videoLikes = 0;

_likes(videoLikes, "-53425992_165397466", "f277e7e3");

logVideoId("-53425992_165397466", "f277e7e3");
</script>
<div class="video_fav" alt="To favorites" title="To favorites" onclick="Fav.Toggle(this, '-53425992_165397466', '6adab556e398e4602ff6ca8224d02364');"></div>
<div class="video_embed" title="Share" onclick="Embed.show(-53425992, 165397466);"></div>
<div class="main_share_panel">
<div id="addthis_sharing_toolbox">
<div class="share_btn share_btn_re"><span class="re_icon" onclick="return Share.reddit();"></span></div>
<div class="share_btn share_btn_fb"><span class="fb_icon" onclick="return Share.facebook();"></span></div>
<div class="share_btn share_btn_tw"><span class="tw_icon" onclick="return Share.twitter();"></span></div>
<div class="share_btn share_btn_vk"><span class="vk_icon" onclick="return Share.vkontakte();"></span></div>
</div>
</div>
<div class="clear"></div>
<div class="bordertop">
<br>Views: 62 <br>Published: 17 Jul at 04:03 PM </div></div>
<div>Duration: 28:50</div>
</div>
<div class="tabs_wrapper">
<div class="tabs">
<style>.tabs > div.tab-btn{width: 50%;}</style>
<div class="tab-btn tab-show" id="tab_similar">Related</div>
<div class="tab-btn" id="tab_comments">Comments</div>
</div>
</div>
<div class="rightbar tab_similar tab-result tab-show">
<span style="margin: 0px 13px;padding-bottom: 15px;" class="standalonead">
<iframe width="300" height="250" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10002729?time=1555363895445" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10002729"></iframe>
</span>
<div class="video-item video5817_163924401" data-video="WyJXaW50ZXIgWCBHYW1lcyAyMDEyIFtBc3Blbl0gLUZpbmFsIE1lbnMgU3VwZXJQaXBlIiw1MjQ0LDQzLCJodHRwczpcL1wvc3VuOS03My51c2VyYXBpLmNvbVwvYzUxOTYxMVwvdTA1ODE3XC92aWRlb1wvbF83YzRiNjI4Zi5qcGciLCJiNDI1NzY4NmJlYjIxYmE5MjU3NmM5M2NkY2E0YzQxYSJd" data-embedUrl="https://daxab.com/player/KAhmz-GavAUPDmAIUc-dxW0iRzceKb9b5iRkzUgl6a64TgiBLxfTWX_05gLijQy52bs5iuGYkEt_vaEWKbHTdQ" data-playerHash="KAhmz-GavAUPDmAIUc-dxW0iRzceKb9b5iRkzUgl6a64TgiBLxfTWX_05gLijQy52bs5iuGYkEt_vaEWKbHTdQ" data-favHash="24659d3a3191ece1b857645e1fc8a685">
<a href="/watch/5817_163924401" data-id="5817_163924401">
<div class="video-show"></div>
<div class="video-fav" data-hash="24659d3a3191ece1b857645e1fc8a685"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-73.userapi.com/c519611/u05817/video/l_7c4b628f.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">43</span>
<span class="video-time">01:27:24</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Winter X Games 2012 [Aspen] -Final Mens SuperPipe</div>
</a>
</div>
<div class="video-item video-4296410_167364583" data-video="WyJYLWdhbWVzIDIwMTIgYm14IHBhcmsgZmluYWwgKNC90LAg0YDRg9GB0YHQutC+0LwpIiw1MjkzLDM1LCJodHRwczpcL1wvc3VuOS01NS51c2VyYXBpLmNvbVwvYzUzNTUyMlwvdTk3MDA0ODdcL3ZpZGVvXC9sXzgwYzVjOThmLmpwZyIsIjc5YWY1NjFmYzFiYjQ5NmFhZjM5MzEzNzBkNjBkZTQ4Il0=" data-embedUrl="https://daxab.com/player/LM6j7qgXfHebbWrhIvGyWy7I5KdhdvV5hzYiMcS_AutCvWmZoTubmttd38jbwfs7E99csjiJ3b3Bv6SF_szSWQ" data-playerHash="LM6j7qgXfHebbWrhIvGyWy7I5KdhdvV5hzYiMcS_AutCvWmZoTubmttd38jbwfs7E99csjiJ3b3Bv6SF_szSWQ" data-favHash="24c1b3a842c081128ac6b11cf664db03">
<a href="/watch/-4296410_167364583" data-id="-4296410_167364583">
<div class="video-show"></div>
<div class="video-fav" data-hash="24c1b3a842c081128ac6b11cf664db03"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-55.userapi.com/c535522/u9700487/video/l_80c5c98f.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">8 years ago</span>
<span class="video-view">35</span>
<span class="video-time">01:28:13</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012 bmx park final (на русском)</div>
</a>
</div>
<div class="video-item video-20774214_163747625" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxKSBIRCIsNjY5MiwxNzUsImh0dHBzOlwvXC9zdW45LTgwLnVzZXJhcGkuY29tXC9jNTE0MjE4XC91MTE0MDIzMTJcL3ZpZGVvXC9sXzE3NWVhYzgxLmpwZyIsIjFmNTlmYmM5NTc0MjY4N2JhNmEwOGQ5ZWY3ZjUzMmZlIl0=" data-embedUrl="https://daxab.com/player/W89OX2P1Wl7P2Snbw4J5XhMZIvl1XUwlVjgXJrbeGe8eP1dffA3Pf45KQQPFW_4uDRkf1lXdKfEbKzVrH9wypg" data-playerHash="W89OX2P1Wl7P2Snbw4J5XhMZIvl1XUwlVjgXJrbeGe8eP1dffA3Pf45KQQPFW_4uDRkf1lXdKfEbKzVrH9wypg" data-favHash="50bb3585ce2c07e4f89225006b652011">
<a href="/watch/-20774214_163747625" data-id="-20774214_163747625">
<div class="video-show"></div>
<div class="video-fav" data-hash="50bb3585ce2c07e4f89225006b652011"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c514218/u11402312/video/l_175eac81.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">175</span>
<span class="video-time">01:51:32</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1) HD</div>
</a>
</div>
<div class="video-item video-127946280_456239036" data-video="WyJFcm9nZSEgSCBtbyBHYW1lIG1vIEthaWhhdHN1IFphbm1haSDQrdGA0L7Qs9C1ISAwMlwv0JfQsNCx0YvRgtGM0YHRjyDQsiDRgdC+0LfQtNCw0L3QuNC4INGF0LXQvdGC0LDQuSDQuNCz0YDRiyAoKzE4IEhlbnRhaSBIRCkiLDE4NDYsNDA0NSwiaHR0cHM6XC9cL3N1bjYtMjIudXNlcmFwaS5jb21cL2M2MjY2MTlcL3Y2MjY2MTkyODBcLzI2MTExXC9TSmhKdExNNDBYNC5qcGciLCJhNmI1NmM0M2NkNmQxNTJiMzljYWExMzNhZmQzNGUwZSJd" data-embedUrl="https://daxab.com/player/DLB1LguKjOrw4nzhoDbLm68vjmcOlsW7WnmN1l17bSqgd6LkodN-0rlJa0fPnNRk6bFNeSJNqadzRvpLoxlp336B_Zc3CPLAUZzaugnWPmM" data-playerHash="DLB1LguKjOrw4nzhoDbLm68vjmcOlsW7WnmN1l17bSqgd6LkodN-0rlJa0fPnNRk6bFNeSJNqadzRvpLoxlp336B_Zc3CPLAUZzaugnWPmM" data-favHash="3a7c012b7e3a9ade5274c30b1f34afa5">
<a href="/watch/-127946280_456239036" data-id="-127946280_456239036">
<div class="video-show"></div>
<div class="video-fav" data-hash="3a7c012b7e3a9ade5274c30b1f34afa5"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c626619/v626619280/26111/SJhJtLM40X4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">5 years ago</span>
<span class="video-view">4k</span>
<span class="video-time">30:46</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Eroge! H mo Game mo Kaihatsu Zanmai Эроге! 02/Забыться в создании хентай игры (+18 Hentai HD)</div>
</a>
</div>
<div class="video-item video-20774214_163586566" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxKSIsNjc4NCwyMDQsImh0dHBzOlwvXC9zdW45LTg2LnVzZXJhcGkuY29tXC9jNTMyMzE1XC91OTcyODAxMVwvdmlkZW9cL2xfNTYzNzMyMmMuanBnIiwiYWQxZjRiYzE1ODQ0M2VlODE2MDVlZjcyNTE0YzczMDIiXQ==" data-embedUrl="https://daxab.com/player/rbbtM2xLqoA92LcCgASsAfFWlpWIu29Mijqp3TDQn211BAcmGTfkEPyJEHXB3Ajt4qRdK2WDr9ohaOhxuXr2Fg" data-playerHash="rbbtM2xLqoA92LcCgASsAfFWlpWIu29Mijqp3TDQn211BAcmGTfkEPyJEHXB3Ajt4qRdK2WDr9ohaOhxuXr2Fg" data-favHash="255bdad6a740955e40ef7e3cc4be92d1">
<a href="/watch/-20774214_163586566" data-id="-20774214_163586566">
<div class="video-show"></div>
<div class="video-fav" data-hash="255bdad6a740955e40ef7e3cc4be92d1"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-86.userapi.com/c532315/u9728011/video/l_5637322c.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">204</span>
<span class="video-time">01:53:04</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1)</div>
</a>
</div>
<div class="video-item video-147765253_456239519" data-video="WyJHeW1uYXN0aWNzIC0gQXJ0aXN0aWMgLSBNZW4mIzM5O3MgVGVhbSBGaW5hbCB8IExvbmRvbiAyMDEyIE9seW1waWMgR2FtZXMiLDExNjQwLDEsImh0dHBzOlwvXC9zdW42LTIwLnVzZXJhcGkuY29tXC93Y2t5eHgwLTdCQjlFa05BRDJYbVpfeDN1ZjZVemUtcnVxVl94d1wvYXZpSDc4Vk1OZEkuanBnIiwiN2FmNjY0MDdiNDQwMGM3NTc2NWRmYTYzZGJmZWEwMTQiXQ==" data-embedUrl="https://daxab.com/player/4SySaG-Lt9YPsggkWAJfQjpJLRsY2HAmDLKIVmAQyZwZ6lUxmrD43RDvRBf29GfJ8i9Iw88Rhnc4SqCzri_L6FzsD9_c4LpC-6RKHbpU6yU" data-playerHash="4SySaG-Lt9YPsggkWAJfQjpJLRsY2HAmDLKIVmAQyZwZ6lUxmrD43RDvRBf29GfJ8i9Iw88Rhnc4SqCzri_L6FzsD9_c4LpC-6RKHbpU6yU" data-favHash="21df7febeb200b63d0e7d35d0cdd5e05">
<a href="/watch/-147765253_456239519" data-id="-147765253_456239519">
<div class="video-show"></div>
<div class="video-fav" data-hash="21df7febeb200b63d0e7d35d0cdd5e05"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/wckyxx0-7BB9EkNAD2XmZ_x3uf6Uze-ruqV_xw/aviH78VMNdI.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 months ago</span>
<span class="video-view">1</span>
<span class="video-time">03:14:00</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Gymnastics - Artistic - Men&amp;#39;s Team Final | London 2012 Olympic Games</div>
</a>
</div>
<div class="video-item video-188576299_456239468" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAxIFs3MjBwXSIsMTg2OSwxOTEyLCJodHRwczpcL1wvc3VuNi0yMC51c2VyYXBpLmNvbVwvYzg1NzQyMFwvdjg1NzQyMDM5NVwvMjJhZTBmXC9xV0dMVjlycDgyRS5qcGciLCI2NzM2ZGM1NzgzOWRhYTFjMGI3M2YwN2UxZDhhM2ZkNSJd" data-embedUrl="https://daxab.com/player/YbglyNtDrKeCwIEiBnUwZzfkoQXjIzlvt8ji04raYM6aVfx4evR3sUt_3STNP5MWlP-gToe2hPROC7bz9mgk8Xhg4nHMtYbc4aqkvSQUGKE" data-playerHash="YbglyNtDrKeCwIEiBnUwZzfkoQXjIzlvt8ji04raYM6aVfx4evR3sUt_3STNP5MWlP-gToe2hPROC7bz9mgk8Xhg4nHMtYbc4aqkvSQUGKE" data-favHash="cc2ea25fe6368387cd0d23e5d82299d1">
<a href="/watch/-188576299_456239468" data-id="-188576299_456239468">
<div class="video-show"></div>
<div class="video-fav" data-hash="cc2ea25fe6368387cd0d23e5d82299d1"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/c857420/v857420395/22ae0f/qWGLV9rp82E.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,9k</span>
<span class="video-time">31:09</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 01 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239466" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAzIFs3MjBwXSIsMTgwNiwxNzMwLCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NTcyOFwvdjg1NTcyODM5NVwvMjRmMjY0XC9lcHl0TURrMFJkSS5qcGciLCI3Y2RiMDhjOTFhNmMwOWZiNjQ3YzBkNDhmZTllMzZlMyJd" data-embedUrl="https://daxab.com/player/PytnTg6L-mXJCofRPP9VfNIkOenEgp6ig5ybmlYmPhMbh5G_wuAbbxVeDDLel3YEHRuI6zpALwmCn2H6JRR_JeQDsrDLZlavovDllNtI-OE" data-playerHash="PytnTg6L-mXJCofRPP9VfNIkOenEgp6ig5ybmlYmPhMbh5G_wuAbbxVeDDLel3YEHRuI6zpALwmCn2H6JRR_JeQDsrDLZlavovDllNtI-OE" data-favHash="ac89a8b41ee3f0f4315343b2c6f6a9fa">
<a href="/watch/-188576299_456239466" data-id="-188576299_456239466">
<div class="video-show"></div>
<div class="video-fav" data-hash="ac89a8b41ee3f0f4315343b2c6f6a9fa"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c855728/v855728395/24f264/epytMDk0RdI.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
 <div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,7k</span>
<span class="video-time">30:06</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 03 [720p]</div>
</a>
</div>
<div class="video-item video-20774214_163763516" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTQgKExpdmUgU2hvdyAyKSBIRCIsNjAzNCwxMzYsImh0dHBzOlwvXC9zdW45LTEzLnVzZXJhcGkuY29tXC9jNTI3NjEwXC91MTE0MDIzMTJcL3ZpZGVvXC9sXzg1MzBmODBiLmpwZyIsIjNjNjFjYzYyMDY2YmI5ZWJhM2RhNjZkNTU3Y2M5MWMyIl0=" data-embedUrl="https://daxab.com/player/06di4O4gWUb4ztDorb9YoJs-gl8Is7nz4_rO7LEO85FG00DQTyNMamQWflE9YUMiUtK543Y4b7bQGcVQnlDgdA" data-playerHash="06di4O4gWUb4ztDorb9YoJs-gl8Is7nz4_rO7LEO85FG00DQTyNMamQWflE9YUMiUtK543Y4b7bQGcVQnlDgdA" data-favHash="0938a89746b3a3d4fa400d6265b36a06">
<a href="/watch/-20774214_163763516" data-id="-20774214_163763516">
<div class="video-show"></div>
<div class="video-fav" data-hash="0938a89746b3a3d4fa400d6265b36a06"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-13.userapi.com/c527610/u11402312/video/l_8530f80b.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">136</span>
<span class="video-time">01:40:34</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x14 (Live Show 2) HD</div>
</a>
</div>
<div class="video-item video-901607_162943067" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIE1hbmBzIE1vdG8gWCBFbmR1cm8gWCIsNDc5OSw4MCwiaHR0cHM6XC9cL3N1bjktNDkudXNlcmFwaS5jb21cL2M1Mjc1MDdcL3UzMDY5OTMxN1wvdmlkZW9cL2xfYTZmNDJkMDkuanBnIiwiYmNmN2I5NjA1ZmNmZDI3OTFhNzUzY2FkYjNjZDViYTAiXQ==" data-embedUrl="https://daxab.com/player/1dBlXJc7h_n4ws6IsDMFhHuTEMd6Urxhc6wprlmVYV4vWPBTE242G3xPvGRwxLryD8fhrtyzBJw4vOHl2YFg9A" data-playerHash="1dBlXJc7h_n4ws6IsDMFhHuTEMd6Urxhc6wprlmVYV4vWPBTE242G3xPvGRwxLryD8fhrtyzBJw4vOHl2YFg9A" data-favHash="c3778c5de8bcf23b7d197e025de7fd81">
<a href="/watch/-901607_162943067" data-id="-901607_162943067">
<div class="video-show"></div>
<div class="video-fav" data-hash="c3778c5de8bcf23b7d197e025de7fd81"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-49.userapi.com/c527507/u30699317/video/l_a6f42d09.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">80</span>
<span class="video-time">01:19:59</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Man`s Moto X Enduro X</div>
</a>
</div>
<div class="video-item video-188576299_456239467" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDA0IFs3MjBwXSIsMTgwNSwxMzgwLCJodHRwczpcL1wvc3VuNi0yMS51c2VyYXBpLmNvbVwvYzg1NTIzNlwvdjg1NTIzNjM5NVwvMjU0ODc1XC9VYUFDbnNmTUNGWS5qcGciLCI0ZTg2ZDE5M2I1ZWExZmQ0ODhkNGZlNWQ0ZGMzZTkzNCJd" data-embedUrl="https://daxab.com/player/O7aN8kUVYhTu1rboNERijtBZDX_5O9ujdHysjyh_Fxi_jcDk7BjZu-0xt3X-P_0TCvs3M9vPkI9C7FB-xgbQ1VYjrQUp_h7uDGeaS31BcDg" data-playerHash="O7aN8kUVYhTu1rboNERijtBZDX_5O9ujdHysjyh_Fxi_jcDk7BjZu-0xt3X-P_0TCvs3M9vPkI9C7FB-xgbQ1VYjrQUp_h7uDGeaS31BcDg" data-favHash="78bdcfd180ecfe4a5ad2e93a0d744515">
<a href="/watch/-188576299_456239467" data-id="-188576299_456239467">
<div class="video-show"></div>
<div class="video-fav" data-hash="78bdcfd180ecfe4a5ad2e93a0d744515"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-21.userapi.com/c855236/v855236395/254875/UaACnsfMCFY.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,4k</span>
<span class="video-time">30:05</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 04 [720p]</div>
</a>
</div>
<div class="video-item video-200135128_456239373" data-video="WyLQkNC80LXRgNC40LrQsNC90YHQutC40Lkg0LrQvtGI0LzQsNGAIC0gQWxhbiBXYWtlJiMzOTtzIEFtZXJpY2FuIE5pZ2h0bWFyZSAjMSIsMjUwMSwxLCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvVHp4NTZSQnR4YXpuUV81X2lrVFpmZmUzcmhmSWgwZWVhQ2dra1FcLy0tZWFOSkdrTnhnLmpwZyIsIjMzMzM4NzZkMTczMjkzMzU4NmVhYjQxYzBmOWZlNGJjIl0=" data-embedUrl="https://daxab.com/player/yIqPPmoFCDuKdHbJ27SjBKfVAfCU_06dggaTgReEzj735EZWUK57EeQNHJwXoHU1l_L6yKFxkEC86PWl1STohbug76cRFldM9DEZ8TmXXSQ" data-playerHash="yIqPPmoFCDuKdHbJ27SjBKfVAfCU_06dggaTgReEzj735EZWUK57EeQNHJwXoHU1l_L6yKFxkEC86PWl1STohbug76cRFldM9DEZ8TmXXSQ" data-favHash="268336d8383e223acd548ce94a4c1928">
<a href="/watch/-200135128_456239373" data-id="-200135128_456239373">
<div class="video-show"></div>
<div class="video-fav" data-hash="268336d8383e223acd548ce94a4c1928"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/Tzx56RBtxaznQ_5_ikTZffe3rhfIh0eeaCgkkQ/--eaNJGkNxg.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 months ago</span>
<span class="video-view">1</span>
<span class="video-time">41:41</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Американский кошмар - Alan Wake&amp;#39;s American Nightmare #1</div>
</a>
</div>
<div class="video-item video195091338_456239074" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIEJlc3QgVHJpY2siLDI0NTcsMiwiaHR0cHM6XC9cL3N1bjYtMjEudXNlcmFwaS5jb21cL2M2MzY5MjVcL3Y2MzY5MjUzMzhcLzJmMWRcLzlKbm9iQ1R0ZjlnLmpwZyIsImUwNWQwZmYwMWJiNDE5MWFhYzRkOWQwNDNkNjk1OWMyIl0=" data-embedUrl="https://daxab.com/player/dFeHheddl9Pcl1iYqPJ9V9QMfa0M6XKxMjRB4u-Frjh8ScO1gUmRgZv6qDuvD3x-K-Vx4DedsLnIRE8EMzQYew" data-playerHash="dFeHheddl9Pcl1iYqPJ9V9QMfa0M6XKxMjRB4u-Frjh8ScO1gUmRgZv6qDuvD3x-K-Vx4DedsLnIRE8EMzQYew" data-favHash="1393143e615e35dcd69cbdbe2d1e56f3">
<a href="/watch/195091338_456239074" data-id="195091338_456239074">
<div class="video-show"></div>
<div class="video-fav" data-hash="1393143e615e35dcd69cbdbe2d1e56f3"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-21.userapi.com/c636925/v636925338/2f1d/9JnobCTtf9g.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">6 years ago</span>
<span class="video-view">2</span>
<span class="video-time">40:57</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Best Trick</div>
</a>
</div>
<div class="video-item video-188576299_456239333" data-video="WyLQl9Cw0YHRgtC10L3Rh9C40LLQvi3QoNCw0YHQv9GD0YLQvdCw0Y8g0LTQtdCy0YPRiNC60LAg0KHRg9C60YPQvNC4IFRzdW5kZXJlIElucmFuIFNob3VqbyBTdWt1bWkgLSAwMiBbNzIwcF0iLDExNTEsNTY2LCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NzIxNlwvdjg1NzIxNjE1NFwvMWE5Yzc5XC9pemdpTm9pTmZLby5qcGciLCIxMTg1ZjdjMWFlNDQyZjc5OGJjM2RlMGUzODQ5NjM1OCJd" data-embedUrl="https://daxab.com/player/fBBTvZ_ddMGkf1e4tTUfgojfZfJwqB_oeKCfGqwJDlWD1Z_fFwVOV0_sE6zK_MLPnYbeP1dLOq-y5uEcVwBPXni3a4AlM2g3Hke2h8hti4g" data-playerHash="fBBTvZ_ddMGkf1e4tTUfgojfZfJwqB_oeKCfGqwJDlWD1Z_fFwVOV0_sE6zK_MLPnYbeP1dLOq-y5uEcVwBPXni3a4AlM2g3Hke2h8hti4g" data-favHash="1059f8f2c8602da44c308a4c7842b98c">
<a href="/watch/-188576299_456239333" data-id="-188576299_456239333">
<div class="video-show"></div>
<div class="video-fav" data-hash="1059f8f2c8602da44c308a4c7842b98c"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c857216/v857216154/1a9c79/izgiNoiNfKo.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">566</span>
<span class="video-time">19:11</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Застенчиво-Распутная девушка Сукуми Tsundere Inran Shoujo Sukumi - 02 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239465" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAyIFs3MjBwXSIsMTgwNiwxMjAyLCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NTYyMFwvdjg1NTYyMDM5NVwvMjQ5ZDg3XC9CcGI5Q2tBUWFBZy5qcGciLCIzNjYyZDFjMWE5NjQ5OTkzZGExMGI3NWYyOTc0MGRjNyJd" data-embedUrl="https://daxab.com/player/qFa85TTx6Q0TwLeD8BDo-epZj3C4Fq3WGObIq5hubHjCurUcz_z2ICOYvnB722SLGFEJfC5oOwN-2SsLSV4g8IFaaAS4ADdta7F9igiJTuA" data-playerHash="qFa85TTx6Q0TwLeD8BDo-epZj3C4Fq3WGObIq5hubHjCurUcz_z2ICOYvnB722SLGFEJfC5oOwN-2SsLSV4g8IFaaAS4ADdta7F9igiJTuA" data-favHash="ff491e850a72875ffb536f7e4fe9c880">
<a href="/watch/-188576299_456239465" data-id="-188576299_456239465">
<div class="video-show"></div>
<div class="video-fav" data-hash="ff491e850a72875ffb536f7e4fe9c880"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c855620/v855620395/249d87/Bpb9CkAQaAg.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,2k</span>
<span class="video-time">30:06</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 02 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239332" data-video="WyLQl9Cw0YHRgtC10L3Rh9C40LLQvi3QoNCw0YHQv9GD0YLQvdCw0Y8g0LTQtdCy0YPRiNC60LAg0KHRg9C60YPQvNC4IFRzdW5kZXJlIElucmFuIFNob3VqbyBTdWt1bWkgLSAwMSBbNzIwcF0iLDEwODIsOTE4LCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvVVJDWFp3aHhjT285TVR2QWtzWjZpUTd6TXFZXzR5alZiWFByandcL3ZIQnB6RFRmLVNjLmpwZyIsIjY1MTEyODU3MmUzNTkzMGRhYjEyYmQ2MWRkNWNiMWMyIl0=" data-embedUrl="https://daxab.com/player/E1EBnqaX0JLjfCy8R8s6M7evmWFnseZZmMaHgWqavbs1N5d2riSmbY_bOTmCuCjFusPMD2nPx-b1Tstigjqw9JdIdQ1z1xH0dyOSMvWUpNk" data-playerHash="E1EBnqaX0JLjfCy8R8s6M7evmWFnseZZmMaHgWqavbs1N5d2riSmbY_bOTmCuCjFusPMD2nPx-b1Tstigjqw9JdIdQ1z1xH0dyOSMvWUpNk" data-favHash="a3947601bc8cd944b48dba7d4c2d585a">
<a href="/watch/-188576299_456239332" data-id="-188576299_456239332">
<div class="video-show"></div>
<div class="video-fav" data-hash="a3947601bc8cd944b48dba7d4c2d585a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/URCXZwhxcOo9MTvAksZ6iQ7zMqY_4yjVbXPrjw/vHBpzDTf-Sc.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">918</span>
<span class="video-time">18:02</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Застенчиво-Распутная девушка Сукуми Tsundere Inran Shoujo Sukumi - 01 [720p]</div>
</a>
</div>
<div class="video-item video-901607_162904584" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIFN0ZXAgVXAiLDMyMzAsNzgsImh0dHBzOlwvXC9zdW45LTY3LnVzZXJhcGkuY29tXC9jNTI2NjE2XC91MzA2OTkzMTdcL3ZpZGVvXC9sXzI0NTEyZWQ0LmpwZyIsIjVhYjZhYjY2MzllMGFiYjI4Y2FkMTA1NzlkNGU0Y2Y4Il0=" data-embedUrl="https://daxab.com/player/-Ip3GA0yDGyfWUGhIQNjDeIEfVXNAoQ6L_qvm3pLFGuJeVBr0p49O-HZttOrVvklxYTFgeklbEawmR26AhOLWA" data-playerHash="-Ip3GA0yDGyfWUGhIQNjDeIEfVXNAoQ6L_qvm3pLFGuJeVBr0p49O-HZttOrVvklxYTFgeklbEawmR26AhOLWA" data-favHash="7c5221b338e8f47aef78cdfd04b38762">
<a href="/watch/-901607_162904584" data-id="-901607_162904584">
<div class="video-show"></div>
<div class="video-fav" data-hash="7c5221b338e8f47aef78cdfd04b38762"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-67.userapi.com/c526616/u30699317/video/l_24512ed4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">78</span>
<span class="video-time">53:50</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Step Up</div>
</a>
</div>
<div class="video-item video-150082602_456239164" data-video="WyLQn9GD0YLQtdGI0LXRgdGC0LLQuNC1INCa0LDRgdGB0LDQvdC00YDRiyAyLiDQmtC+0L3QtdGGINGB0LLQtdGC0LAgMjAxMi4gIyAxIC0g0J/QvtC40YHQutC4INC90LDRh9C40L3QsNGO0YLRgdGPIiwxODY3LDEsImh0dHBzOlwvXC9zdW42LTIwLnVzZXJhcGkuY29tXC9jODMwMzA4XC92ODMwMzA4MTI1XC8xN2E1OWRcL1JUV1Rtb2QxOEJ3LmpwZyIsImFkNDI4MTQyOGMzMmEzZTdkMjRmZTczNjhlOTc5YzExIl0=" data-embedUrl="https://daxab.com/player/fg-cWKW20pbvnT5jkjnmCh8tEzr0Tkx391WRHiEcPQz5zIqlcTc3GK3s1dwhoc3OkHuYrBmZsdR5iwIPEFbtr2zMWJ64xEdPg7eyPsh-pNw" data-playerHash="fg-cWKW20pbvnT5jkjnmCh8tEzr0Tkx391WRHiEcPQz5zIqlcTc3GK3s1dwhoc3OkHuYrBmZsdR5iwIPEFbtr2zMWJ64xEdPg7eyPsh-pNw" data-favHash="f3e0f335d61cf72c6f3c24bb83dcaaaf">
<a href="/watch/-150082602_456239164" data-id="-150082602_456239164">
<div class="video-show"></div>
<div class="video-fav" data-hash="f3e0f335d61cf72c6f3c24bb83dcaaaf"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/c830308/v830308125/17a59d/RTWTmod18Bw.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">3 years ago</span>
<span class="video-view">1</span>
<span class="video-time">31:07</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Путешествие Кассандры 2. Конец света 2012. # 1 - Поиски начинаются</div>
</a>
</div>
<div class="video-item video-20774214_163637013" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTQgKExpdmUgU2hvdyAyKSIsNjA1OCwxMzcsImh0dHBzOlwvXC9zdW45LTgwLnVzZXJhcGkuY29tXC9jNTI3MzE3XC91OTcyODAxMVwvdmlkZW9cL2xfMjZlOWZhMmEuanBnIiwiNzM1NDA1ZjA5YjcwMTcyMDI3M2U3MDRiNDU5NjJhZTAiXQ==" data-embedUrl="https://daxab.com/player/p1TE8S7zWBba-lbR1g5yFWEpHrFY1qTP_A0it6wVDBhVkk4nKwST1FhavO-r5zeeCpWvGKnVOORm6m06PzKsjw" data-playerHash="p1TE8S7zWBba-lbR1g5yFWEpHrFY1qTP_A0it6wVDBhVkk4nKwST1FhavO-r5zeeCpWvGKnVOORm6m06PzKsjw" data-favHash="d462fb2019aed8ca25f27225aac6c177">
<a href="/watch/-20774214_163637013" data-id="-20774214_163637013">
<div class="video-show"></div>
<div class="video-fav" data-hash="d462fb2019aed8ca25f27225aac6c177"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c527317/u9728011/video/l_26e9fa2a.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">137</span>
<span class="video-time">01:40:58</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x14 (Live Show 2)</div>
</a>
</div>
<div class="video-item video-19767527_165512903" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gRm91cnRoIGRheS4gQk1YIEJpZyBBaXIiLDI5NTUsNDcsImh0dHBzOlwvXC9zdW45LTEwLnVzZXJhcGkuY29tXC9jNTA2MTE4XC91ODE5ODkyMDhcL3ZpZGVvXC9sX2VkZmYxMGU4LmpwZyIsIjQ1ZWExY2M0YWQ1MDNkNTQ4ZDI1ZWY3ODk4MTVkODJkIl0=" data-embedUrl="https://daxab.com/player/9-CmGh_RFn2cA7M2OYF7O_Vp-A-RXki5P-EsC-xF5qRHAeDcUZ1jDgIKf5ORmxEB3RLKbcCLFGbzUdVwPD-czA" data-playerHash="9-CmGh_RFn2cA7M2OYF7O_Vp-A-RXki5P-EsC-xF5qRHAeDcUZ1jDgIKf5ORmxEB3RLKbcCLFGbzUdVwPD-czA" data-favHash="dde051d566857522cb8860d118466d73">
<a href="/watch/-19767527_165512903" data-id="-19767527_165512903">
<div class="video-show"></div>
<div class="video-fav" data-hash="dde051d566857522cb8860d118466d73"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-10.userapi.com/c506118/u81989208/video/l_edff10e8.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">47</span>
<span class="video-time">49:15</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Fourth day. BMX Big Air</div>
</a>
</div>
<div class="video-item video-53908459_456239661" data-video="WyJYLdC40LPRgNCwIDIgXC8gWCBHYW1lIDIiLDYxOTUsODEsImh0dHBzOlwvXC9zdW42LTIyLnVzZXJhcGkuY29tXC9jNjM2MjE4XC92NjM2MjE4NDU5XC80ODY0M1wvazFoWEpFWk1SNzQuanBnIiwiYjYzN2IzNzFmNjIxNTUxMThlMzYxZWI2NTE1OGNmMTMiXQ==" data-embedUrl="https://daxab.com/player/V0oy3VArQm_UnwMLcwlksxCLzT_2IZV2wmfb3p6TDn1HocwzoXEzP6wvexJLrVtxRK-DunPJe0LMHNR6IX3nYw" data-playerHash="V0oy3VArQm_UnwMLcwlksxCLzT_2IZV2wmfb3p6TDn1HocwzoXEzP6wvexJLrVtxRK-DunPJe0LMHNR6IX3nYw" data-favHash="fcc330c464b617c167c9bfb94242a787">
<a href="/watch/-53908459_456239661" data-id="-53908459_456239661">
<div class="video-show"></div>
<div class="video-fav" data-hash="fcc330c464b617c167c9bfb94242a787"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c636218/v636218459/48643/k1hXJEZMR74.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">5 years ago</span>
<span class="video-view">81</span>
<span class="video-time">01:43:15</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-игра 2 / X Game 2</div>
</a>
</div>
<div class="video-item video-901607_162940653" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIE1vdG8gRnJlZXN0eWxlIiwzNzg1LDcwLCJodHRwczpcL1wvc3VuOS00NS51c2VyYXBpLmNvbVwvYzUxODYxNlwvdTMwNjk5MzE3XC92aWRlb1wvbF82MmE2MTVmOS5qcGciLCI4NWYzNjI0MjMxYjJkOTVkZTE1OGQ4MjM5Yzg5OWUxYiJd" data-embedUrl="https://daxab.com/player/-v9FTaEm7juDMSfbLQEuMzPtMqocVh9p_lNSgCg0v-8H_LU6_NIx5iCovd5avzvUxFXcpWPgFPAnj3hKLYwnAw" data-playerHash="-v9FTaEm7juDMSfbLQEuMzPtMqocVh9p_lNSgCg0v-8H_LU6_NIx5iCovd5avzvUxFXcpWPgFPAnj3hKLYwnAw" data-favHash="fccdce282eb9ecdf8bd54f3045922e3c">
<a href="/watch/-901607_162940653" data-id="-901607_162940653">
<div class="video-show"></div>
<div class="video-fav" data-hash="fccdce282eb9ecdf8bd54f3045922e3c"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-45.userapi.com/c518616/u30699317/video/l_62a615f9.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">70</span>
<span class="video-time">01:03:05</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Moto Freestyle</div>
</a>
</div>
<div class="video-item video-20774214_163592952" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxXC8gUGFydCAxKSBIRCIsMzM3OSw3MywiaHR0cHM6XC9cL3N1bjktNTQudXNlcmFwaS5jb21cL2M1MjkyMDJcL3UzMDg0OTU1XC92aWRlb1wvbF85Mjc0NDRkMC5qcGciLCJjM2ZmZGZhY2RhNDU5OGJiOTc1MTYwNzc3YTU4ZWRmMSJd" data-embedUrl="https://daxab.com/player/z-rRRU1q6BIcWx7Xko4Jl0HOaOIUoHbsmhqrXtXLFvxx7GKMU32nlFCnwQ27enzqSTNykctf19eaG66eav_I5g" data-playerHash="z-rRRU1q6BIcWx7Xko4Jl0HOaOIUoHbsmhqrXtXLFvxx7GKMU32nlFCnwQ27enzqSTNykctf19eaG66eav_I5g" data-favHash="3576c9c62c9f70156112ddb8ea7f498e">
<a href="/watch/-20774214_163592952" data-id="-20774214_163592952">
<div class="video-show"></div>
<div class="video-fav" data-hash="3576c9c62c9f70156112ddb8ea7f498e"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-54.userapi.com/c529202/u3084955/video/l_927444d0.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">73</span>
<span class="video-time">56:19</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1/ Part 1) HD</div>
</a>
</div>
<div class="video-item video-190464408_456239270" data-video="WyJTbGVlcGluZyBEb2dzIOKeqCDQn9GA0L7RhdC+0LbQtNC10L3QuNC1IDEyIOKeqCDQntC00L7Qu9C20LXQvdC40Y8g4oSWIDIgI1NsZWVwaW5nRG9ncyAjU3RhcnBlcml5YSIsMjU3NiwxLCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvNzBJZ0FONW1tclFjajFjR0FEZ2RaZkNPSkhVejJPeHFDdVJmUWdcL2w4Y0JfTXpwcVJFLmpwZyIsIjg5M2Y3YmJlMWQ3MjBmNDdjNzRmODg0NGQ0ZjI1NDhlIl0=" data-embedUrl="https://daxab.com/player/hFIJAS92o5dvE7WD2_kr2YZLHLkuJQJeYYBjPAFf5aU8MOs86R_kuiRVA38s0zLm-qklHG0MEa0MtHAad3LUTeLxy_XIUjzb8k0-FO4y3ms" data-playerHash="hFIJAS92o5dvE7WD2_kr2YZLHLkuJQJeYYBjPAFf5aU8MOs86R_kuiRVA38s0zLm-qklHG0MEa0MtHAad3LUTeLxy_XIUjzb8k0-FO4y3ms" data-favHash="dfaa105378c66240f1bf25aa8494588d">
<a href="/watch/-190464408_456239270" data-id="-190464408_456239270">
<div class="video-show"></div>
<div class="video-fav" data-hash="dfaa105378c66240f1bf25aa8494588d"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/70IgAN5mmrQcj1cGADgdZfCOJHUz2OxqCuRfQg/l8cB_MzpqRE.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1</span>
<span class="video-time">42:56</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Sleeping Dogs ➨ Прохождение 12 ➨ Одолжения № 2 #SleepingDogs #Starperiya</div>
</a>
</div>
<div class="video-item video384329_164980719" data-video="WyJYLWdhbWVzLCBBc2lhLCBWZXJ0LCAyMDEyIHllYXIiLDI4MzQsMTUsImh0dHBzOlwvXC9zdW45LTIyLnVzZXJhcGkuY29tXC9jNTE4MjEzXC91Mzg0MzI5XC92aWRlb1wvbF82MjEyODI1ZC5qcGciLCJlNzMzZGY4NTdhMGE3ZTczN2FmN2Y1ZDAwMDA4MGI2MCJd" data-embedUrl="https://daxab.com/player/3xADH2OZOtPFg2U83I9JY3mm5wEbURo8fFKneA8n8Oz3idGRJyz52Fjaa2A0bk68tvh_fXGdGzb2DYCWs7KdVw" data-playerHash="3xADH2OZOtPFg2U83I9JY3mm5wEbURo8fFKneA8n8Oz3idGRJyz52Fjaa2A0bk68tvh_fXGdGzb2DYCWs7KdVw" data-favHash="e5a881faca6da5f106ae9f0095b97d3a">
<a href="/watch/384329_164980719" data-id="384329_164980719">
<div class="video-show"></div>
<div class="video-fav" data-hash="e5a881faca6da5f106ae9f0095b97d3a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-22.userapi.com/c518213/u384329/video/l_6212825d.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">15</span>
<span class="video-time">47:14</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games, Asia, Vert, 2012 year</div>
</a>
</div>
<div class="video-item video-12783167_170084732" data-video="WyJYLUdhbWVzIDIwMTIgUmFsbHktQ3Jvc3MiLDU3MDAsMzYsImh0dHBzOlwvXC9zdW45LTM1LnVzZXJhcGkuY29tXC9jNTEzMzEyXC91MTQzOTcyOTQ5XC92aWRlb1wvbF8xMmM1MzFmNi5qcGciLCI5MDQ1MDMyMmY4MjA1OWQzMmNhNjFmNWFkMDUzNzMyYiJd" data-embedUrl="https://daxab.com/player/swn9PAIXVwJFMxA3l4_gstJc9anxHnv6AKx5-5d-Cj_W8uAT2GlbW0zmEVibupSvNRpPiPlTD8NHVZ2lj2WQBg" data-playerHash="swn9PAIXVwJFMxA3l4_gstJc9anxHnv6AKx5-5d-Cj_W8uAT2GlbW0zmEVibupSvNRpPiPlTD8NHVZ2lj2WQBg" data-favHash="a6401941b889bf3e78bc1df3df07fa72">
<a href="/watch/-12783167_170084732" data-id="-12783167_170084732">
<div class="video-show"></div>
<div class="video-fav" data-hash="a6401941b889bf3e78bc1df3df07fa72"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-35.userapi.com/c513312/u143972949/video/l_12c531f6.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 years ago</span>
<span class="video-view">36</span>
<span class="video-time">01:35:00</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games 2012 Rally-Cross</div>
</a>
</div>
<div class="video-item video-19767527_165206109" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gRmlyc3QiLDMxNDYsMjgsImh0dHBzOlwvXC9zdW45LTgyLnVzZXJhcGkuY29tXC9jNTMyMzA0XC91ODE5ODkyMDhcL3ZpZGVvXC9sXzYwNWY0ZmQ0LmpwZyIsIjdkZmU1MDE3MjJhNzMzZGFiZGZmZjMyM2M2Njk3NDUwIl0=" data-embedUrl="https://daxab.com/player/srVxpj7qCJkkBtbGdAGlclumt20k48GVmBPtsJZZf8ZK-6n_9V2_xu3GEVf88CG_fAwEgsOhcZN4cIpcyNo3Ag" data-playerHash="srVxpj7qCJkkBtbGdAGlclumt20k48GVmBPtsJZZf8ZK-6n_9V2_xu3GEVf88CG_fAwEgsOhcZN4cIpcyNo3Ag" data-favHash="cb12f1c853d023943344fa576e1d169a">
<a href="/watch/-19767527_165206109" data-id="-19767527_165206109">
<div class="video-show"></div>
<div class="video-fav" data-hash="cb12f1c853d023943344fa576e1d169a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-82.userapi.com/c532304/u81989208/video/l_605f4fd4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">28</span>
<span class="video-time">52:26</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. First</div>
</a>
</div>
<div class="video-item video-19767527_165214922" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gVGhyaWQgZGF5LiBCTVggU3RyZWV0IEZpbmFsIiwxMjcwLDExLCJodHRwczpcL1wvc3VuOS00Ni51c2VyYXBpLmNvbVwvYzUzNTUxMlwvdTgxOTg5MjA4XC92aWRlb1wvbF82NmRkZGZmYy5qcGciLCI5MGU1YjNiZGU4MjNhMjhkNTFlMzRiOGU3NmU2MzYzNiJd" data-embedUrl="https://daxab.com/player/BbH8SqfvQ2irtv8mdFOhKtexI-rO7G7FJFEhgFVuI7Bt60TAYXWtudlnnT-YLrVnprJGfhMYchrLP-_f7pcNUw" data-playerHash="BbH8SqfvQ2irtv8mdFOhKtexI-rO7G7FJFEhgFVuI7Bt60TAYXWtudlnnT-YLrVnprJGfhMYchrLP-_f7pcNUw" data-favHash="a89d88f3a3ec1b0d933a35869c8a565a">
<a href="/watch/-19767527_165214922" data-id="-19767527_165214922">
<div class="video-show"></div>
<div class="video-fav" data-hash="a89d88f3a3ec1b0d933a35869c8a565a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-46.userapi.com/c535512/u81989208/video/l_66dddffc.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">11</span>
<span class="video-time">21:10</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Thrid day. BMX Street Final</div>
</a>
</div>
<div class="video-item video-901607_162943115" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIFJldmlldyIsNTM4MSwxNiwiaHR0cHM6XC9cL3N1bjktODcudXNlcmFwaS5jb21cL2M1MjcyMTZcL3UzMDY5OTMxN1wvdmlkZW9cL2xfZDhiOTA2ZjQuanBnIiwiM2MyY2QzNDFhMTM4ODlkNjcxNjJmNmFlMTU5ZDBlMTIiXQ==" data-embedUrl="https://daxab.com/player/IzduTe80H_dMj1KCCJksJ5xn5oNKws1MV8pe7UKjtuiyg0chzmMKO1VScDB8nZVPOXIf6eapb_yfnT5CLnMryg" data-playerHash="IzduTe80H_dMj1KCCJksJ5xn5oNKws1MV8pe7UKjtuiyg0chzmMKO1VScDB8nZVPOXIf6eapb_yfnT5CLnMryg" data-favHash="f9d2d9c63926cc6aedd852f38b190488">
<a href="/watch/-901607_162943115" data-id="-901607_162943115">
<div class="video-show"></div>
<div class="video-fav" data-hash="f9d2d9c63926cc6aedd852f38b190488"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-87.userapi.com/c527216/u30699317/video/l_d8b906f4.jpg"></div>
 
<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">16</span>
<span class="video-time">01:29:41</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Review</div>
</a>
</div>
<div class="video-item video-19767527_165214946" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gVGhyaWQgZGF5LiBCTVggVmVydCIsOTkzLDg0LCJodHRwczpcL1wvc3VuOS03OS51c2VyYXBpLmNvbVwvYzUxNDUwNFwvdTgxOTg5MjA4XC92aWRlb1wvbF9jZDk1NjI3Ny5qcGciLCIwZjVhMDQ4NzkwYTM2NWYxYzNlNzUyNWI3NGIxYjQ1ZCJd" data-embedUrl="https://daxab.com/player/g_znYRdupGV8FaI6AyRkKiNvfkpFEKR6du3zES5mcikPZ6_skSLYUUqO4R-JdWDsjxpaiP9pO_0tHQJkVl8ZZQ" data-playerHash="g_znYRdupGV8FaI6AyRkKiNvfkpFEKR6du3zES5mcikPZ6_skSLYUUqO4R-JdWDsjxpaiP9pO_0tHQJkVl8ZZQ" data-favHash="8af51ac89ba5920efcbea5001540648a">
<a href="/watch/-19767527_165214946" data-id="-19767527_165214946">
<div class="video-show"></div>
<div class="video-fav" data-hash="8af51ac89ba5920efcbea5001540648a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-79.userapi.com/c514504/u81989208/video/l_cd956277.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">84</span>
<span class="video-time">16:33</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Thrid day. BMX Vert</div>
</a>
</div>
<div class="video-item video581608605_456239155" data-video="WyJSb2NrIHRoZSBWb3RlISAtIC0gUHJvcGFnYW5kYVdhdGNoICggMTQ0IFggMTQ0ICkubXA0IiwxMzE1LDE4NCwiaHR0cHM6XC9cL3N1bjYtMjMudXNlcmFwaS5jb21cLzFwdVZmMjRaVlMyd0VvNldVb3lJdV9EM2hQY1I1S1BoMWd0Uk93XC9WY2txVzB6WEVlUS5qcGciLCI3OGJlNzFlOTdmZWU0ODgwZmUxNzE5NWM1YTkxMGRjYyJd" data-embedUrl="https://daxab.com/player/yXw0P83DLgr3gUlFpuZLshXWtJv6qa1-MCzQUWDwMM_ol2rQLUYtPH0iltad2OG6wewxJWMu3IktlOHkIf2l0w" data-playerHash="yXw0P83DLgr3gUlFpuZLshXWtJv6qa1-MCzQUWDwMM_ol2rQLUYtPH0iltad2OG6wewxJWMu3IktlOHkIf2l0w" data-favHash="77b9ed060e5f68bd61219753639c34cf">
<a href="/watch/581608605_456239155" data-id="581608605_456239155">
<div class="video-show"></div>
<div class="video-fav" data-hash="77b9ed060e5f68bd61219753639c34cf"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/1puVf24ZVS2wEo6WUoyIu_D3hPcR5KPh1gtROw/VckqW0zXEeQ.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">184</span>
<span class="video-time">21:55</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Rock the Vote! - - PropagandaWatch ( 144 X 144 ).mp4</div>
</a>
</div>
<div class="video-item video-20774214_163592955" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxXC8gUGFydCAyKSBIRCIsMzMxMyw0MCwiaHR0cHM6XC9cL3N1bjktNjEudXNlcmFwaS5jb21cL2M1MTg2MDJcL3UzMDg0OTU1XC92aWRlb1wvbF83MmE0MTdiMi5qcGciLCI2MTM1NjNhMzdkZTY3NThjY2FiYzk2Njc5NzljZDg0OSJd" data-embedUrl="https://daxab.com/player/tQehSOCvsA7KX3nlq0Fpc-Ct7rU4iZxYw0OUpThLtq0013VjVC5pYeu2h1Nf2AF0GA7HdYPT8gOhGiC0jvYzNg" data-playerHash="tQehSOCvsA7KX3nlq0Fpc-Ct7rU4iZxYw0OUpThLtq0013VjVC5pYeu2h1Nf2AF0GA7HdYPT8gOhGiC0jvYzNg" data-favHash="d8daed6fac8e218db9f4e78de3e73b29">
<a href="/watch/-20774214_163592955" data-id="-20774214_163592955">
<div class="video-show"></div>
<div class="video-fav" data-hash="d8daed6fac8e218db9f4e78de3e73b29"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-61.userapi.com/c518602/u3084955/video/l_72a417b2.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">40</span>
<span class="video-time">55:13</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1/ Part 2) HD</div>
</a>
</div>
<div class="video-item video-4296410_167364578" data-video="WyJYLWdhbWVzIDIwMTIgYm14IHZlcnQgZmluYWwgKCDQvdCwINGA0YPRgdGB0LrQvtC8KSIsOTkzLDYyLCJodHRwczpcL1wvc3VuOS0zMy51c2VyYXBpLmNvbVwvYzUyMzYwMFwvdTk3MDA0ODdcL3ZpZGVvXC9sX2NkYmZhMWUyLmpwZyIsIjE5NmVhYWE3NzE2MDhiYmJkYzc4ZjE5YTg3MDk5NDVjIl0=" data-embedUrl="https://daxab.com/player/7tlTXv3u17NOAdDsfQNVhlEkwYiD30Lfd30aE2ydZIk1CqVfN9YqLdqi-SLx16v8XFJPERHhAUeYmaBJHXRrnA" data-playerHash="7tlTXv3u17NOAdDsfQNVhlEkwYiD30Lfd30aE2ydZIk1CqVfN9YqLdqi-SLx16v8XFJPERHhAUeYmaBJHXRrnA" data-favHash="ed545ebb89984148892cfa8ee7c45ef8">
<a href="/watch/-4296410_167364578" data-id="-4296410_167364578">
<div class="video-show"></div>
<div class="video-fav" data-hash="ed545ebb89984148892cfa8ee7c45ef8"></div>
 <div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-33.userapi.com/c523600/u9700487/video/l_cdbfa1e2.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">8 years ago</span>
<span class="video-view">62</span>
<span class="video-time">16:33</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012 bmx vert final ( на русском)</div>
</a>
</div>
<div class="video-item video-145081708_456242402" data-video="WyLQuNC1IEJvcmRlcmxhbmRzIEdPVFkgWzIwMDldIFBDXC9QUzNcL1hib3gzNjAuINCf0YDQvtGF0L7QttC00LXQvdC40LUg0LfQsCDQm9C40LvQuNGCINGHLjggKzE4IFtkYXIiLDI2NDcsMTE4LCJodHRwczpcL1wvaS5teWNkbi5tZVwvZ2V0VmlkZW9QcmV2aWV3P2lkPTEzMjIzNzY4MjU1NjMmaWR4PTEmdHlwZT0zOSZ0a249U3VPTUVURDh2R1dRMTNIaHdhdzI0WmhoX0xRJmZuPXZpZF9sIiwiZmRkNjMwM2Y3YTczNzUxYjlkZGQzZGFiYjE5NGEwNzYiXQ==" data-embedUrl="https://daxab.com/player/W6-mh7iy8TfHOcigZyk1u84R_1G0li8EZBA3EgsJZHLJwTpPMQdTdQXwz37dK9O_skSPYUa3PmXMA5pjN21s8tnYqjg1SlUMInYxDCncIX4" data-playerHash="W6-mh7iy8TfHOcigZyk1u84R_1G0li8EZBA3EgsJZHLJwTpPMQdTdQXwz37dK9O_skSPYUa3PmXMA5pjN21s8tnYqjg1SlUMInYxDCncIX4" data-favHash="697a4941bacc445326c3e7e84ee48753">
<a href="/watch/-145081708_456242402" data-id="-145081708_456242402">
<div class="video-show"></div>
<div class="video-fav" data-hash="697a4941bacc445326c3e7e84ee48753"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://i.mycdn.me/getVideoPreview?id=1322376825563&idx=1&type=39&tkn=SuOMETD8vGWQ13Hhwaw24Zhh_LQ&fn=vid_l"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">118</span>
<span class="video-time">44:07</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">ие Borderlands GOTY [2009] PC/PS3/Xbox360. Прохождение за Лилит ч.8 +18 [dar</div>
</a>
</div>
<div class="video-item video104849236_456277453" data-video="WyJNQVggUEFZTkUgMyBOUEMgV2FycyAyMyAoVUZFIHZzIENyYWNoYSBQcmV0bykiLDE4ODMsMSwiaHR0cHM6XC9cL3N1bjYtMjIudXNlcmFwaS5jb21cL1d4d08yUXhIVUk4Qm5qaks1R3U2TGQxMGlLVWZDYkFpQTVMRjlnXC9zTGdBSEQ4U0lwOC5qcGciLCI5ZTJkMjU5Mzk5YmZhYzBhNzU1YWVmOGMxNDE1ZjhhZSJd" data-embedUrl="https://daxab.com/player/PtlMoMBvo7ZIPKWIpbSoGc1dpAi56zWKGHcwYf39lknxuSeHi0cJOmDL_y-Mqymi21oBctvi-zHGJRDeXaRmww" data-playerHash="PtlMoMBvo7ZIPKWIpbSoGc1dpAi56zWKGHcwYf39lknxuSeHi0cJOmDL_y-Mqymi21oBctvi-zHGJRDeXaRmww" data-favHash="daff3e6ec27aa95eabcde1e74d2bdced">
<a href="/watch/104849236_456277453" data-id="104849236_456277453">
<div class="video-show"></div>
<div class="video-fav" data-hash="daff3e6ec27aa95eabcde1e74d2bdced"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/WxwO2QxHUI8BnjjK5Gu6Ld10iKUfCbAiA5LF9g/sLgAHD8SIp8.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 months ago</span>
<span class="video-view">1</span>
<span class="video-time">31:23</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">MAX PAYNE 3 NPC Wars 23 (UFE vs Cracha Preto)</div>
</a>
</div>
<div class="video-item video-901607_162902722" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuINCe0LHQt9C+0YAg0L/QtdGA0LLQvtCz0L4g0LTQvdGPIiwyNjkyLDE0LCJodHRwczpcL1wvc3VuOS0zMy51c2VyYXBpLmNvbVwvYzUyNjUwN1wvdTMwNjk5MzE3XC92aWRlb1wvbF8xYWFlYTc5ZC5qcGciLCJjNjU2ODIyYzQ3YzY3MTEyM2U5Y2QxYzYxMWZhOTQzMyJd" data-embedUrl="https://daxab.com/player/8BPcRbDcJMx7lLwuUfrafL6fsMRw6o4wH6qr7nsoVLVsTTJpHXxFgBMOjh4Ne8cDe2dvaASgsL6-8p-S-91G_g" data-playerHash="8BPcRbDcJMx7lLwuUfrafL6fsMRw6o4wH6qr7nsoVLVsTTJpHXxFgBMOjh4Ne8cDe2dvaASgsL6-8p-S-91G_g" data-favHash="ad3b2031e76f2b45329c183030b3b5e2">
<a href="/watch/-901607_162902722" data-id="-901607_162902722">
<div class="video-show"></div>
<div class="video-fav" data-hash="ad3b2031e76f2b45329c183030b3b5e2"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-33.userapi.com/c526507/u30699317/video/l_1aaea79d.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">14</span>
<span class="video-time">44:52</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Обзор первого дня</div>
</a>
</div>
<div class="video-item video593543_162951285" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIEJlc3QgVHJpY2siLDI0NTcsMjUsImh0dHBzOlwvXC9zdW45LTY5LnVzZXJhcGkuY29tXC9jNTI3MTA3XC91MzA2OTkzMTdcL3ZpZGVvXC9sXzk5ZGM1MjljLmpwZyIsIjBmODBkOWU2YWYzNTQ2NDY0YTdmZTE3OTRkYjk1YjVmIl0=" data-embedUrl="https://daxab.com/player/BdNpgYUYWIpPdq4ULS9XgR3P6GOb1UFItzSMr59lMQJ-1CINSnukPoKdfV0VZBXVoaH4_0pvJ1aV_dA3Q6AQUg" data-playerHash="BdNpgYUYWIpPdq4ULS9XgR3P6GOb1UFItzSMr59lMQJ-1CINSnukPoKdfV0VZBXVoaH4_0pvJ1aV_dA3Q6AQUg" data-favHash="d6ab0848c96f33c96ad3032c4a0d0637">
<a href="/watch/593543_162951285" data-id="593543_162951285">
<div class="video-show"></div>
<div class="video-fav" data-hash="d6ab0848c96f33c96ad3032c4a0d0637"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-69.userapi.com/c527107/u30699317/video/l_99dc529c.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">25</span>
<span class="video-time">40:57</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Best Trick</div>
</a>
</div>
<div class="video-item video-197946026_456239614" data-video="WyLQn9GA0L7RhdC+0LbQtNC10L3QuNC1IFNsZWVwaW5nIERvZ3MgKDIwMTMpICMyIC0g0JrQu9GD0LEgXCLQkdCw0Lwt0JHQsNC8XCIiLDczMDgsNDMsImh0dHBzOlwvXC9zdW42LTIzLnVzZXJhcGkuY29tXC9xMGxBdlpKd21wZE9samtzd3BZYW5MeFpKUl9aTDF6YjB1RXdfZ1wvYllXZE50M05xSDAuanBnIiwiYmU2NjkwN2I3ZGJjNzY1YjVkNzE5MjU5ZWQ0NTZmMDEiXQ==" data-embedUrl="https://daxab.com/player/lrwYGcjjoKFQsTJ1aQbZ9uMA4kj-VPCUiCLnwGV4aC2JTdfRIrotK0h4pPp_YEDq1vcIlFliBu7NX9_c_PLsjw4De2mZ7Z70hcQnzRXk9bI" data-playerHash="lrwYGcjjoKFQsTJ1aQbZ9uMA4kj-VPCUiCLnwGV4aC2JTdfRIrotK0h4pPp_YEDq1vcIlFliBu7NX9_c_PLsjw4De2mZ7Z70hcQnzRXk9bI" data-favHash="004476a96ec3d868483e77418cf244f2">
<a href="/watch/-197946026_456239614" data-id="-197946026_456239614">
<div class="video-show"></div>
<div class="video-fav" data-hash="004476a96ec3d868483e77418cf244f2"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/q0lAvZJwmpdOljkswpYanLxZJR_ZL1zb0uEw_g/bYWdNt3NqH0.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">4 months ago</span>
<span class="video-view">43</span>
<span class="video-time">02:01:48</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Прохождение Sleeping Dogs (2013) #2 - Клуб &quot;Бам-Бам&quot;</div>
</a>
</div>
<div class="video-item video-27575945_163557091" data-video="WyJYLWdhbWVzIDIwMTIs0KTRgNC40YHRgtCw0LnQuyzQn9C+0LvQvdC+0YHRgtGM0Y4hIiwzNzg1LDEzLCJodHRwczpcL1wvc3VuOS04MC51c2VyYXBpLmNvbVwvYzUyNjIxNVwvdTU5NDM1OTcxXC92aWRlb1wvbF84YThkNGI0NS5qcGciLCI3OTMwYzNkY2Y5NzdkZDhiYWM1ZWQ1NDJiYTkwZGIyOCJd" data-embedUrl="https://daxab.com/player/O9xzCum8cxiDci-Fo_LrpB9wMEu8okF3zyJfAR334WETE0OXDCgfxtR-SfmU8tbCU6gCAXKseDPOFDPgGWM0Cw" data-playerHash="O9xzCum8cxiDci-Fo_LrpB9wMEu8okF3zyJfAR334WETE0OXDCgfxtR-SfmU8tbCU6gCAXKseDPOFDPgGWM0Cw" data-favHash="9795118eba65377777021ab858d2594b">
<a href="/watch/-27575945_163557091" data-id="-27575945_163557091">
<div class="video-show"></div>
<div class="video-fav" data-hash="9795118eba65377777021ab858d2594b"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c526215/u59435971/video/l_8a8d4b45.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">13</span>
<span class="video-time">01:03:05</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012,Фристайл,Полностью!</div>
</a>
</div>
<span class="standalonead" style="margin: 0px 13px;padding-bottom: 15px;">
<iframe width="300" height="250" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10002729?time=1555363895445" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10002729"></iframe>
</span>
<div class="more-similar" onclick="event.cancelBubble = true; loadMoreSimilar(this);" data-page="1" data-video="-53425992_165397466" data-hash="84d987ceaf814a97d050f983234f2315d5157387">Show more</div>
</div>

<script>
$('.tabs > div.tab-btn').on('click', function() {
  if ($(this).hasClass('tab-show')) {
    return;
  }

  $(this).parent().children('div').removeClass('tab-show');
  $(this).addClass('tab-show');

  $('.tab-result').removeClass('tab-show');
  $('.tab-result.' + this.id).addClass('tab-show');

  // if ($('.width').width() < 1020 && this.id == 'tab_comments') {
  //   loadComments($('#video_hash').val().trim());
  // }
});
</script>
</div>
</div>
</div>
<div class="footer">
<div class="width">
<div class="now">
<b>You might be interested</b>
<span><a href="/video/Mia Malkova">Mia Malkova</a> <a href="/video/Jade Venus">Jade Venus</a> <a href="/video/Scarlett Mae">Scarlett Mae</a> <a href="/video/Miriama Kunkelova">Miriama Kunkelova</a> <a href="/video/Paulina Soul">Paulina Soul</a> <a href="/video/Excogi">Excogi</a> <a href="/video/Barbara Angel Bbw">Barbara Angel Bbw</a> <a href="/video/Danish">Danish</a> <a href="/video/My Pickup Girls">My Pickup Girls</a> <a href="/video/Celestina Blooms">Celestina Blooms</a> <a href="/video/Payton Preslee">Payton Preslee</a> <a href="/video/Interracial">Interracial</a> <a href="/video/Arteya">Arteya</a> <a href="/video/Secret Crush">Secret Crush</a> <a href="/video/Korra Del Rio">Korra Del Rio</a> <a href="/video/Adriana Chechik Squirt">Adriana Chechik Squirt</a> <a href="/video/Femdom Estonia">Femdom Estonia</a> <a href="/video/Legalporno">Legalporno</a> <a href="/video/Audrey Bitoni">Audrey Bitoni</a> <a href="/video/Oiled">Oiled</a></span>
</div>
<div class="clear"></div>
<div class="copy">
<a href="/">DaftSex</a>
<a href="/browse">Browse</a>
<a href="/hottest">Hottest</a>
<a href="/categories">Category</a>
<a href="/pornstars">Pornstars</a>
<a href="/holders">DMCA</a>
<a href="/privacy">Privacy</a>
<a href="/faq">FAQ</a>
<a href="/contact">Contact Us</a>
<a href="https://daft.sex/" target="_blank" title="Start search Page for DaftSex">DaftSex Search</a>
<a href="https://theporndude.com/" target="_blank" rel="nofollow" style="font-weight: bold;">Best Porn Sites</a>
<a href="https://twitter.com/DaftPost" target="_blank" class="twitter font-weight-bold" style="background: #01abf1 url(/img/twitter.png) no-repeat scroll center center;border-radius: 25px;color: transparent;background-size: 27px;"><i class="icon-twistter"></i>Twitter</a>
</div>
</div>
</div>
</div>
<div class="bg_layer"></div>

<script>
onPageLoaded();
$(window).load(onPageReady);
previewEvents();
</script>
<script type='text/javascript' src='//sufficientretiredbunker.com/a5/96/32/a59632dda777535e591fa2e7dde66a93.js'></script>
<script type="text/javascript" async src="/js/app.ve1c0c4626c025980.js"></script>
<script type="text/javascript">
!function(){var w,n=window.outerWidth-window.innerWidth,o=window.outerHeight-window.innerHeight,t=$(window).width(),e=$(window).height();function i(i){w&&clearTimeout(w),w=setTimeout(function(){var i=window.outerWidth-window.innerWidth,w=window.outerHeight-window.innerHeight;n!==i&&o!==w||n==i&&o==w||t==$(window).width()&&e==$(window).height()||t!==$(window).width()&&e!==$(window).height()||!winFullscreen&&(400<i&&n<i&&400<Math.abs(i-n)||200<w&&o<w&&200<Math.abs(w-o))&&(setCookie("_dt",1,{path:"/",expires:1800,domain:"."+location.hostname}),dt()),n=window.outerWidth-window.innerWidth,o=window.outerHeight-window.innerHeight,t=$(window).width(),e=$(window).height()},500)}browser.mobile||$(window).off("resize",i).on("resize",i)}();
</script>

<div style="display:none;">
<script type="text/javascript">
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
</script>
</div>


</body>
</html>
<!DOCTYPE html>
<html>
<head data-extra="aIX2DL3JIGnO7i8k6HvdT4">
<meta charset="UTF-8">
<title>2012 X-Games 18 motocross best trick — DaftSex</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="/index.js?6" defer></script>
<link rel="manifest" href="/manifest.webmanifest">
<meta name="robots" content="all" />
<meta name="robots" content="noarchive" />
<meta name="description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality">
<meta name="keywords" content="Watch, video, daftsex, motocross, best, trick, daftsex">
<meta property="og:url" content="https://daftsex.com/watch/-53425992_165397466" />
<meta property="og:type" content="video.movie" />
<meta property="og:title" content="2012 X-Games 18 motocross best trick" />
<meta property="og:description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality" />
<meta property="og:image" content="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg" />
<meta property="og:video:type" content="text/html" />
<link href="/css/robotocondensed.v4b6944ca5bbf3c8b.css" rel="stylesheet" type="text/css">
<link rel="icon" type="image/png" sizes="196x196" href="/img/daftlogo196x196.png?2">
<link rel="shortcut icon" href="/img/favicon.ico?5" />
<meta name="theme-color" content="#C00">
<link rel="stylesheet" href="/css/common.v112268244c82433e.css" />
<link rel="stylesheet" href="/css/icons.v38e1854b6d41caec.css" />
<link rel="stylesheet" href="/css/dark.ve32b1eba318e418e.css" />
<script type="text/javascript">
window.globEmbedUrl = 'https://daxab.com/player/';
window.timeNow = 1659012240;
setInterval(function() {
  window.timeNow++;
}, 1000);
window.liteopen = false;
window.is_logged = false;
</script>
<script type="text/javascript" src="/js/jquery-2.1.1.min.v18b7e87c91d98481.js"></script>
<script type="text/javascript" src="/js/history.v955089448af5a0c8.js"></script>
<script type="text/javascript" src="/js/nprogress.v3410974b8841b4f3.js"></script>
<script type="text/javascript" src="/js/nouislider.v2192f61dc764023a.js"></script>
<script type="text/javascript" src="/js/select.ve363dc0076d2c78b.js"></script>
<script type="text/javascript" src="/js/common.vb1f31c4b59a9e4d1.js"></script>
<script type="text/javascript" src="/js/auth.vfa3c32a15fba2304.js"></script>
<script type="text/javascript" src="/js/jquery.mutations.min.v4b147b138a5b1019.js"></script>
<script type="text/javascript" src="/js/fav.v764365b62392eb58.js"></script>
<script type="text/javascript" src="/js/share.vdf8ddf291dc2f417.js"></script>
<script type="text/javascript" src="/js/likes.v5e342c5feda70804.js"></script>
<script type="text/javascript">
window._stv = '67c5c441c9e2';
window.log_version = 'fbcb78248ac55029';
</script>
</head>
<body>
<a class="up" onclick="toTop();">&uarr;</a>
<div class="page_wrapper">
<div class="header">
<div class="width">
<div class="header_inner">
<a onclick="window.history.go(-1); return false;" title="Back" class="back-logo"><i class="icon-left"></i></a>
<a href="/" title="DaftSex" class="logo">
<svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="55.000000pt" height="50.000000pt" viewBox="0 0 311.000000 127.000000" preserveAspectRatio="xMidYMid meet">
<g transform="translate(0.000000,180.000000) scale(0.100000,-0.100000)" fill="#dd3333" stroke="none">
<path d="M895 1713 c-142 -78 -362 -148 -559 -178 -54 -9 -102 -18 -107 -21
-14 -8 -10 -228 5 -354 36 -284 110 -501 235 -692 110 -167 280 -320 442 -398
l46 -23 54 27 c149 75 311 216 408 356 59 84 131 221 168 320 35 92 79 287 93
410 12 101 18 346 8 355 -2 2 -49 11 -104 19 -215 33 -424 99 -578 184 l-51
29 -60 -34z m15 -818 l0 -636 -45 31 c-128 88 -246 229 -327 390 -86 171 -148
427 -148 612 0 72 2 78 23 83 209 46 339 85 447 134 25 11 46 21 48 21 1 0 2
-286 2 -635z m197 592 c99 -40 258 -87 343 -102 79 -14 74 -4 67 -133 -4 -63
-11 -120 -16 -125 -5 -5 -38 -6 -83 -1 l-73 9 3 46 3 45 -63 13 c-35 6 -75 14
-90 18 l-28 5 0 -95 c0 -81 3 -96 18 -102 9 -3 81 -9 160 -12 78 -3 142 -10
142 -15 0 -14 -39 -155 -61 -221 -35 -101 -118 -253 -188 -340 -65 -79 -226
-221 -242 -212 -8 5 -12 501 -5 514 3 4 43 2 90 -3 l85 -9 3 -41 3 -41 37 65
c59 105 64 96 -61 104 -61 3 -122 11 -136 16 l-25 10 0 325 c0 179 2 325 4
325 2 0 53 -20 113 -43z" />
<path d="M628 1242 c-79 -19 -78 -15 -53 -153 18 -102 51 -201 98 -294 69
-137 66 -145 67 178 l0 287 -22 -1 c-13 -1 -53 -9 -90 -17z" />
</g>
</svg>
</a>
<div class="clear-form"><i class="icon-cancel"></i></div>
<form class="search-form" onsubmit="return search(true);">
<input type="text" id="q" autocomplete="off" value="" placeholder="Search millions of videos...">
<a title="RandomTV" href="/video/Tyler Nixon" class="randomQuery"><span class="deskrandom"></span></a>
<button type="submit" title="Search button">Search</button>
<div class="head-menu"></div>
</form>
<div class="head-menu module-browse"><a href="/browse">Browse</a></div>
<div class="head-menu module-hottest"><a href="/hottest">Hottest</a></div>
<div class="head-menu module-categories"><a href="/categories">Category</a></div>
<div class="head-menu module-pornstars"><a href="/pornstars">Pornstars</a></div>
<div class="head-menu"><a href="https://theporndude.com/" target="_blank" rel="nofollow" style="color: #000;">Best Porn Sites</a></div>
<button class="btn-search" type="button"><i class="icon-left"></i></button>
<span class="hide-item">

<a href="/pornstars"><span class="btn-pornstars"></span></a>
<a href="/categories"><span class="btn-catt"></span></a>
<a href="/hottest"><span class="btn-hottest"></span></a>
<a href="/browse"><span class="btn-browse"></span></a>
</span>
<span class="auth login hide-item" title="Login" onclick="Auth.Login();"></span>
</div>
</div>
<div id="progress_content"></div>
<div class="suggest_search"><div class="suggest_inner width"><ul class="suggest_items"></ul></div></div>
</div>
<div class="bg_width">
<div class="width">

 <div class="aboveVideo standalonead" style="text-align: center;padding-top: 75px;margin-bottom: -70px;">

<span style="
    text-align: center;
"> <a style="
    text-align: left;
    color: #fff;
    font-size: 15px;
    text-decoration: none;
    padding: 5px;
    top: 61px;
    margin: 5px;
    border-radius: 25px;
    display: block;
    background-color: #5e62ff;
    " href="https://artsporn.com" target="_blank">
ℹ: ARTSPORN.COM - Another new Alternative, faster website without ads. 👊⚡️⚡️⚡️</a></span>


<iframe width="300" height="150" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10009021?time=1575323689465" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10012984"></iframe>




</div>
<div class="aboveVideoPC standalonead" style="text-align: center;padding-top: 75px;margin-bottom: -70px;">








</div>
<div class="content-wrapper" id="content">
<div itemscope itemtype="http://schema.org/VideoObject">
<link itemprop="thumbnailUrl" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
<span itemprop="thumbnail" itemscope itemtype="http://schema.org/ImageObject">
<link itemprop="url" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
<link itemprop="contentUrl" href="https://sun9-46.userapi.com/c535304/u6252465/video/l_dd930e8f.jpg">
</span>
<meta itemprop="name" content="2012 X-Games 18 motocross best trick" />
<meta itemprop="uploadDate" content="2013-07-17T16:03:39+04:00">
<meta itemprop="description" content="2012 X-Games 18 motocross best trick – Watch video Watch video in high quality" />
<meta itemprop="duration" content="T28M50S" />
</div>

<div class="video">
<a class="standalonead" style="font-size: 14px; font-weight: bold;color:#0a0a0a; text-align: center;" href="https://landing.brazzersnetwork.com/?ats=eyJhIjoyOTIyOTcsImMiOjU3NzAzNTQ4LCJuIjoxNCwicyI6OTAsImUiOjg4MDMsInAiOjU3fQ==" target="_blank" rel="nofollow"><div class="message-container" style="background-color:#ffd001;padding: 7px;font-size: 17px;font-weight: 700;"><span class="nomobile">EXCLUSIVE DaftSex OFFER - Join BRAZZERS Only 1$ Today ! [PROMO]</span> <span class="nomobile"><span style="color:rgba(255, 208, 1)"></span><span style="color:rgba(255, 208, 1)"><span></span></span></span></div></a>
<div style="background:#000000;outline: 0px solid #fff;" class="frame_wrapper">
<script id="data-embed-video-QT5DeEsqGn3yZ_mJFNtCxdo65AvvbMi0DtQXCWT-m22QgZlhGUMW5-nS1XjASfeLa1V8ikwimIXcWAPi38GQS5UeiS3ETtGcyYhgFz-xV1A">
if (window.DaxabPlayer && window.DaxabPlayer.Init) {
  DaxabPlayer.Init({
    id: 'video-53425992_165397466',
    origin: window.globEmbedUrl.replace('/player/', ''),
    embedUrl: window.globEmbedUrl,
    hash: "QT5DeEsqGn3yZ_mJFNtCxdo65AvvbMi0DtQXCWT-m22QgZlhGUMW5-nS1XjASfeLa1V8ikwimIXcWAPi38GQS5UeiS3ETtGcyYhgFz-xV1A",
    color: "f12b24",
    styles: {
      border: 0,
      overflow: 'hidden',
      marginBottom: '-5px'
    },
    attrs: {
      frameborder: 0,
      allowFullScreen: ''
    },
    events: function(data) {
      if (data.event == 'player_show') {
        dt();

        HistoryWatch.set("-53425992_165397466", "WyIyMDEyIFgtR2FtZXMgMTggbW90b2Nyb3NzIGJlc3QgdHJpY2siLDE3MzAsNjIsImh0dHBzOlwvXC9zdW45LTQ2LnVzZXJhcGkuY29tXC9jNTM1MzA0XC91NjI1MjQ2NVwvdmlkZW9cL2xfZGQ5MzBlOGYuanBnIiwiODc4OGE3MDgzNDAzZWNkOTg0ZWNiOGExNThhMmJjN2IiXQ==");
      }
    }
  });
}
</script>
</div>
<div class="video_info_wrapper">
<h1 class="heading">2012 X-Games 18 motocross best trick</h1>
<div class="voting-module video-watch">
<span class="icon-container">
<span class="icon icon1"></span>
<span class="icon icon2 hidden-vote"></span>
</span>
<span class="text-container">
<span class="text text1"></span>
<span class="text text2"></span>
</span>
</div>
<script>
var videoLikes = 0;

_likes(videoLikes, "-53425992_165397466", "f277e7e3");

logVideoId("-53425992_165397466", "f277e7e3");
</script>
<div class="video_fav" alt="To favorites" title="To favorites" onclick="Fav.Toggle(this, '-53425992_165397466', '6adab556e398e4602ff6ca8224d02364');"></div>
<div class="video_embed" title="Share" onclick="Embed.show(-53425992, 165397466);"></div>
<div class="main_share_panel">
<div id="addthis_sharing_toolbox">
<div class="share_btn share_btn_re"><span class="re_icon" onclick="return Share.reddit();"></span></div>
<div class="share_btn share_btn_fb"><span class="fb_icon" onclick="return Share.facebook();"></span></div>
<div class="share_btn share_btn_tw"><span class="tw_icon" onclick="return Share.twitter();"></span></div>
<div class="share_btn share_btn_vk"><span class="vk_icon" onclick="return Share.vkontakte();"></span></div>
</div>
</div>
<div class="clear"></div>
<div class="bordertop">
<br>Views: 62 <br>Published: 17 Jul at 04:03 PM </div></div>
<div>Duration: 28:50</div>
</div>
<div class="tabs_wrapper">
<div class="tabs">
<style>.tabs > div.tab-btn{width: 50%;}</style>
<div class="tab-btn tab-show" id="tab_similar">Related</div>
<div class="tab-btn" id="tab_comments">Comments</div>
</div>
</div>
<div class="rightbar tab_similar tab-result tab-show">
<span style="margin: 0px 13px;padding-bottom: 15px;" class="standalonead">
<iframe width="300" height="250" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10002729?time=1555363895445" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10002729"></iframe>
</span>
<div class="video-item video5817_163924401" data-video="WyJXaW50ZXIgWCBHYW1lcyAyMDEyIFtBc3Blbl0gLUZpbmFsIE1lbnMgU3VwZXJQaXBlIiw1MjQ0LDQzLCJodHRwczpcL1wvc3VuOS03My51c2VyYXBpLmNvbVwvYzUxOTYxMVwvdTA1ODE3XC92aWRlb1wvbF83YzRiNjI4Zi5qcGciLCJiNDI1NzY4NmJlYjIxYmE5MjU3NmM5M2NkY2E0YzQxYSJd" data-embedUrl="https://daxab.com/player/q_dyprwjQ6uUvmM7ExYw5_7thrWGg85ISn2pmFCoS0ZSbwdtHtKDZcXzMV-w9fNVuRNhUMdMeZxGUuWlUQC1Hg" data-playerHash="q_dyprwjQ6uUvmM7ExYw5_7thrWGg85ISn2pmFCoS0ZSbwdtHtKDZcXzMV-w9fNVuRNhUMdMeZxGUuWlUQC1Hg" data-favHash="24659d3a3191ece1b857645e1fc8a685">
<a href="/watch/5817_163924401" data-id="5817_163924401">
<div class="video-show"></div>
<div class="video-fav" data-hash="24659d3a3191ece1b857645e1fc8a685"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-73.userapi.com/c519611/u05817/video/l_7c4b628f.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
 <span class="video-date">9 years ago</span>
<span class="video-view">43</span>
<span class="video-time">01:27:24</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Winter X Games 2012 [Aspen] -Final Mens SuperPipe</div>
</a>
</div>
<div class="video-item video-4296410_167364583" data-video="WyJYLWdhbWVzIDIwMTIgYm14IHBhcmsgZmluYWwgKNC90LAg0YDRg9GB0YHQutC+0LwpIiw1MjkzLDM1LCJodHRwczpcL1wvc3VuOS01NS51c2VyYXBpLmNvbVwvYzUzNTUyMlwvdTk3MDA0ODdcL3ZpZGVvXC9sXzgwYzVjOThmLmpwZyIsIjc5YWY1NjFmYzFiYjQ5NmFhZjM5MzEzNzBkNjBkZTQ4Il0=" data-embedUrl="https://daxab.com/player/evpdU3UJ6fZ2hpkbeYRf2RtisZAe8FPMERs05etFkpWjkhFqiobtNbaFMfctirVdg0obmhNyvocpyTJSqBUujA" data-playerHash="evpdU3UJ6fZ2hpkbeYRf2RtisZAe8FPMERs05etFkpWjkhFqiobtNbaFMfctirVdg0obmhNyvocpyTJSqBUujA" data-favHash="24c1b3a842c081128ac6b11cf664db03">
<a href="/watch/-4296410_167364583" data-id="-4296410_167364583">
<div class="video-show"></div>
<div class="video-fav" data-hash="24c1b3a842c081128ac6b11cf664db03"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-55.userapi.com/c535522/u9700487/video/l_80c5c98f.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">8 years ago</span>
<span class="video-view">35</span>
<span class="video-time">01:28:13</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012 bmx park final (на русском)</div>
</a>
</div>
<div class="video-item video-20774214_163747625" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxKSBIRCIsNjY5MiwxNzUsImh0dHBzOlwvXC9zdW45LTgwLnVzZXJhcGkuY29tXC9jNTE0MjE4XC91MTE0MDIzMTJcL3ZpZGVvXC9sXzE3NWVhYzgxLmpwZyIsIjFmNTlmYmM5NTc0MjY4N2JhNmEwOGQ5ZWY3ZjUzMmZlIl0=" data-embedUrl="https://daxab.com/player/1XIjMoMdnvpM5vdR0qWA7pGe9n_CIs7ukN0N45ShgGiXsFfqzveAfCH80dKevOO7qGieb9F6lmoGaC1G6mLJ7A" data-playerHash="1XIjMoMdnvpM5vdR0qWA7pGe9n_CIs7ukN0N45ShgGiXsFfqzveAfCH80dKevOO7qGieb9F6lmoGaC1G6mLJ7A" data-favHash="50bb3585ce2c07e4f89225006b652011">
<a href="/watch/-20774214_163747625" data-id="-20774214_163747625">
<div class="video-show"></div>
<div class="video-fav" data-hash="50bb3585ce2c07e4f89225006b652011"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c514218/u11402312/video/l_175eac81.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">175</span>
<span class="video-time">01:51:32</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1) HD</div>
</a>
</div>
<div class="video-item video-127946280_456239036" data-video="WyJFcm9nZSEgSCBtbyBHYW1lIG1vIEthaWhhdHN1IFphbm1haSDQrdGA0L7Qs9C1ISAwMlwv0JfQsNCx0YvRgtGM0YHRjyDQsiDRgdC+0LfQtNCw0L3QuNC4INGF0LXQvdGC0LDQuSDQuNCz0YDRiyAoKzE4IEhlbnRhaSBIRCkiLDE4NDYsNDA0NSwiaHR0cHM6XC9cL3N1bjYtMjIudXNlcmFwaS5jb21cL2M2MjY2MTlcL3Y2MjY2MTkyODBcLzI2MTExXC9TSmhKdExNNDBYNC5qcGciLCJhNmI1NmM0M2NkNmQxNTJiMzljYWExMzNhZmQzNGUwZSJd" data-embedUrl="https://daxab.com/player/0T6JDMp7HGG3cLCxqELeQOtDm7o8YjCRQCu8G4kIXqUPrHvKSUTgMaVeAnESlGTynAYRqtbD7GWB4ut1AEUQ0nDc7uUnUI1NfcLFJn0sv0k" data-playerHash="0T6JDMp7HGG3cLCxqELeQOtDm7o8YjCRQCu8G4kIXqUPrHvKSUTgMaVeAnESlGTynAYRqtbD7GWB4ut1AEUQ0nDc7uUnUI1NfcLFJn0sv0k" data-favHash="3a7c012b7e3a9ade5274c30b1f34afa5">
<a href="/watch/-127946280_456239036" data-id="-127946280_456239036">
<div class="video-show"></div>
<div class="video-fav" data-hash="3a7c012b7e3a9ade5274c30b1f34afa5"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c626619/v626619280/26111/SJhJtLM40X4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">5 years ago</span>
<span class="video-view">4k</span>
<span class="video-time">30:46</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Eroge! H mo Game mo Kaihatsu Zanmai Эроге! 02/Забыться в создании хентай игры (+18 Hentai HD)</div>
</a>
</div>
<div class="video-item video-20774214_163586566" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxKSIsNjc4NCwyMDQsImh0dHBzOlwvXC9zdW45LTg2LnVzZXJhcGkuY29tXC9jNTMyMzE1XC91OTcyODAxMVwvdmlkZW9cL2xfNTYzNzMyMmMuanBnIiwiYWQxZjRiYzE1ODQ0M2VlODE2MDVlZjcyNTE0YzczMDIiXQ==" data-embedUrl="https://daxab.com/player/2OaU6NhkUZTFLRw24M3WvXu5eUlQe7hFN9rB46PxFYx4SeXRFyo-XqmW2g_khkMsuNpqXP6zp0agaM-8HhRA-Q" data-playerHash="2OaU6NhkUZTFLRw24M3WvXu5eUlQe7hFN9rB46PxFYx4SeXRFyo-XqmW2g_khkMsuNpqXP6zp0agaM-8HhRA-Q" data-favHash="255bdad6a740955e40ef7e3cc4be92d1">
<a href="/watch/-20774214_163586566" data-id="-20774214_163586566">
<div class="video-show"></div>
<div class="video-fav" data-hash="255bdad6a740955e40ef7e3cc4be92d1"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-86.userapi.com/c532315/u9728011/video/l_5637322c.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">204</span>
<span class="video-time">01:53:04</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1)</div>
</a>
</div>
<div class="video-item video-147765253_456239519" data-video="WyJHeW1uYXN0aWNzIC0gQXJ0aXN0aWMgLSBNZW4mIzM5O3MgVGVhbSBGaW5hbCB8IExvbmRvbiAyMDEyIE9seW1waWMgR2FtZXMiLDExNjQwLDEsImh0dHBzOlwvXC9zdW42LTIwLnVzZXJhcGkuY29tXC93Y2t5eHgwLTdCQjlFa05BRDJYbVpfeDN1ZjZVemUtcnVxVl94d1wvYXZpSDc4Vk1OZEkuanBnIiwiN2FmNjY0MDdiNDQwMGM3NTc2NWRmYTYzZGJmZWEwMTQiXQ==" data-embedUrl="https://daxab.com/player/ZHJBWwJe25WA5AJMJy-Sz-1E2ruRLlbSShoRLEqPFcPlzAFhSPDfAWkCUaSrDJjFPmXpn8BNPcL59qNk9pnLGH4IVlFgpux2qGtah2gKjBE" data-playerHash="ZHJBWwJe25WA5AJMJy-Sz-1E2ruRLlbSShoRLEqPFcPlzAFhSPDfAWkCUaSrDJjFPmXpn8BNPcL59qNk9pnLGH4IVlFgpux2qGtah2gKjBE" data-favHash="21df7febeb200b63d0e7d35d0cdd5e05">
<a href="/watch/-147765253_456239519" data-id="-147765253_456239519">
<div class="video-show"></div>
<div class="video-fav" data-hash="21df7febeb200b63d0e7d35d0cdd5e05"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/wckyxx0-7BB9EkNAD2XmZ_x3uf6Uze-ruqV_xw/aviH78VMNdI.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 months ago</span>
<span class="video-view">1</span>
<span class="video-time">03:14:00</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Gymnastics - Artistic - Men&amp;#39;s Team Final | London 2012 Olympic Games</div>
</a>
</div>
<div class="video-item video-188576299_456239468" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAxIFs3MjBwXSIsMTg2OSwxOTEyLCJodHRwczpcL1wvc3VuNi0yMC51c2VyYXBpLmNvbVwvYzg1NzQyMFwvdjg1NzQyMDM5NVwvMjJhZTBmXC9xV0dMVjlycDgyRS5qcGciLCI2NzM2ZGM1NzgzOWRhYTFjMGI3M2YwN2UxZDhhM2ZkNSJd" data-embedUrl="https://daxab.com/player/zwzr6UZT69JferK8X_vKCOxMO4w1e8vP00JuN7yLlhkA0P24pPzAGPpjeHG6O2Fmd_PULSpwCHqCOLEI5Uy6GwvE7quBfB9AOTVOZl1CjFM" data-playerHash="zwzr6UZT69JferK8X_vKCOxMO4w1e8vP00JuN7yLlhkA0P24pPzAGPpjeHG6O2Fmd_PULSpwCHqCOLEI5Uy6GwvE7quBfB9AOTVOZl1CjFM" data-favHash="cc2ea25fe6368387cd0d23e5d82299d1">
<a href="/watch/-188576299_456239468" data-id="-188576299_456239468">
<div class="video-show"></div>
<div class="video-fav" data-hash="cc2ea25fe6368387cd0d23e5d82299d1"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/c857420/v857420395/22ae0f/qWGLV9rp82E.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,9k</span>
<span class="video-time">31:09</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 01 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239466" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAzIFs3MjBwXSIsMTgwNiwxNzMwLCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NTcyOFwvdjg1NTcyODM5NVwvMjRmMjY0XC9lcHl0TURrMFJkSS5qcGciLCI3Y2RiMDhjOTFhNmMwOWZiNjQ3YzBkNDhmZTllMzZlMyJd" data-embedUrl="https://daxab.com/player/_ytl6-x270mRyhF_TpnAj8ytaRpWdX7SsYpHlKenjynaD75HmGw3zl9vsT8sJ1C2E2nwoLJq34gHz4xRnny6cy_5lyrOypHeSPFGOixcG2c" data-playerHash="_ytl6-x270mRyhF_TpnAj8ytaRpWdX7SsYpHlKenjynaD75HmGw3zl9vsT8sJ1C2E2nwoLJq34gHz4xRnny6cy_5lyrOypHeSPFGOixcG2c" data-favHash="ac89a8b41ee3f0f4315343b2c6f6a9fa">
<a href="/watch/-188576299_456239466" data-id="-188576299_456239466">
<div class="video-show"></div>
<div class="video-fav" data-hash="ac89a8b41ee3f0f4315343b2c6f6a9fa"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c855728/v855728395/24f264/epytMDk0RdI.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
 <span class="video-date">1 year ago</span>
<span class="video-view">1,7k</span>
<span class="video-time">30:06</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 03 [720p]</div>
</a>
</div>
<div class="video-item video-20774214_163763516" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTQgKExpdmUgU2hvdyAyKSBIRCIsNjAzNCwxMzYsImh0dHBzOlwvXC9zdW45LTEzLnVzZXJhcGkuY29tXC9jNTI3NjEwXC91MTE0MDIzMTJcL3ZpZGVvXC9sXzg1MzBmODBiLmpwZyIsIjNjNjFjYzYyMDY2YmI5ZWJhM2RhNjZkNTU3Y2M5MWMyIl0=" data-embedUrl="https://daxab.com/player/56R1VYZoTe3AbOF98jd_CzFFS8-AvTXmxapWoi0jThLwxBRpD9pbnlPCJ27OnqxkWTOWNptwBlZXzp_mrcsKhw" data-playerHash="56R1VYZoTe3AbOF98jd_CzFFS8-AvTXmxapWoi0jThLwxBRpD9pbnlPCJ27OnqxkWTOWNptwBlZXzp_mrcsKhw" data-favHash="0938a89746b3a3d4fa400d6265b36a06">
<a href="/watch/-20774214_163763516" data-id="-20774214_163763516">
<div class="video-show"></div>
<div class="video-fav" data-hash="0938a89746b3a3d4fa400d6265b36a06"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-13.userapi.com/c527610/u11402312/video/l_8530f80b.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">136</span>
<span class="video-time">01:40:34</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x14 (Live Show 2) HD</div>
</a>
</div>
<div class="video-item video-901607_162943067" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIE1hbmBzIE1vdG8gWCBFbmR1cm8gWCIsNDc5OSw4MCwiaHR0cHM6XC9cL3N1bjktNDkudXNlcmFwaS5jb21cL2M1Mjc1MDdcL3UzMDY5OTMxN1wvdmlkZW9cL2xfYTZmNDJkMDkuanBnIiwiYmNmN2I5NjA1ZmNmZDI3OTFhNzUzY2FkYjNjZDViYTAiXQ==" data-embedUrl="https://daxab.com/player/L4_-3x2MdPn4_BJ0geFkmj-fW03Ded-mXeoOQQnlPPTJ3xUO9mAybrVj7S5ohorszMs6cu7SZxkxllsOgbcZUw" data-playerHash="L4_-3x2MdPn4_BJ0geFkmj-fW03Ded-mXeoOQQnlPPTJ3xUO9mAybrVj7S5ohorszMs6cu7SZxkxllsOgbcZUw" data-favHash="c3778c5de8bcf23b7d197e025de7fd81">
<a href="/watch/-901607_162943067" data-id="-901607_162943067">
<div class="video-show"></div>
<div class="video-fav" data-hash="c3778c5de8bcf23b7d197e025de7fd81"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-49.userapi.com/c527507/u30699317/video/l_a6f42d09.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">80</span>
<span class="video-time">01:19:59</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Man`s Moto X Enduro X</div>
</a>
</div>
<div class="video-item video-188576299_456239467" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDA0IFs3MjBwXSIsMTgwNSwxMzgwLCJodHRwczpcL1wvc3VuNi0yMS51c2VyYXBpLmNvbVwvYzg1NTIzNlwvdjg1NTIzNjM5NVwvMjU0ODc1XC9VYUFDbnNmTUNGWS5qcGciLCI0ZTg2ZDE5M2I1ZWExZmQ0ODhkNGZlNWQ0ZGMzZTkzNCJd" data-embedUrl="https://daxab.com/player/dYFxrA29t871jyJsVL8WtcM4KOOxLtWB4YlgWhGuuhnhdNZHBtRqlpHFxo95NEHfxini_e8pk38OXV05WKHWlzL46yU8QTNHuF616T0aEys" data-playerHash="dYFxrA29t871jyJsVL8WtcM4KOOxLtWB4YlgWhGuuhnhdNZHBtRqlpHFxo95NEHfxini_e8pk38OXV05WKHWlzL46yU8QTNHuF616T0aEys" data-favHash="78bdcfd180ecfe4a5ad2e93a0d744515">
<a href="/watch/-188576299_456239467" data-id="-188576299_456239467">
<div class="video-show"></div>
<div class="video-fav" data-hash="78bdcfd180ecfe4a5ad2e93a0d744515"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-21.userapi.com/c855236/v855236395/254875/UaACnsfMCFY.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,4k</span>
<span class="video-time">30:05</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 04 [720p]</div>
</a>
</div>
<div class="video-item video-200135128_456239373" data-video="WyLQkNC80LXRgNC40LrQsNC90YHQutC40Lkg0LrQvtGI0LzQsNGAIC0gQWxhbiBXYWtlJiMzOTtzIEFtZXJpY2FuIE5pZ2h0bWFyZSAjMSIsMjUwMSwxLCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvVHp4NTZSQnR4YXpuUV81X2lrVFpmZmUzcmhmSWgwZWVhQ2dra1FcLy0tZWFOSkdrTnhnLmpwZyIsIjMzMzM4NzZkMTczMjkzMzU4NmVhYjQxYzBmOWZlNGJjIl0=" data-embedUrl="https://daxab.com/player/c4yK1gm7tQPmqJJnuYidEUZkSZ1oDiJrP4bItFhiCcAQfgO2EDWpms5bGt0LwZTbCCEoSq76A3TtpnQF00m7_YxDNuO71qJz8ganc6NaJSg" data-playerHash="c4yK1gm7tQPmqJJnuYidEUZkSZ1oDiJrP4bItFhiCcAQfgO2EDWpms5bGt0LwZTbCCEoSq76A3TtpnQF00m7_YxDNuO71qJz8ganc6NaJSg" data-favHash="268336d8383e223acd548ce94a4c1928">
<a href="/watch/-200135128_456239373" data-id="-200135128_456239373">
<div class="video-show"></div>
<div class="video-fav" data-hash="268336d8383e223acd548ce94a4c1928"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/Tzx56RBtxaznQ_5_ikTZffe3rhfIh0eeaCgkkQ/--eaNJGkNxg.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 months ago</span>
<span class="video-view">1</span>
<span class="video-time">41:41</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Американский кошмар - Alan Wake&amp;#39;s American Nightmare #1</div>
</a>
</div>
<div class="video-item video195091338_456239074" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIEJlc3QgVHJpY2siLDI0NTcsMiwiaHR0cHM6XC9cL3N1bjYtMjEudXNlcmFwaS5jb21cL2M2MzY5MjVcL3Y2MzY5MjUzMzhcLzJmMWRcLzlKbm9iQ1R0ZjlnLmpwZyIsImUwNWQwZmYwMWJiNDE5MWFhYzRkOWQwNDNkNjk1OWMyIl0=" data-embedUrl="https://daxab.com/player/u1ivW13XnGsVlW3_V6JyaO02W2qM0lZRjXC6TxhYUU23okrl-xZib9ic4rOkBrhnJ8WhOxDi-266FtCkPrZqyg" data-playerHash="u1ivW13XnGsVlW3_V6JyaO02W2qM0lZRjXC6TxhYUU23okrl-xZib9ic4rOkBrhnJ8WhOxDi-266FtCkPrZqyg" data-favHash="1393143e615e35dcd69cbdbe2d1e56f3">
<a href="/watch/195091338_456239074" data-id="195091338_456239074">
<div class="video-show"></div>
<div class="video-fav" data-hash="1393143e615e35dcd69cbdbe2d1e56f3"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-21.userapi.com/c636925/v636925338/2f1d/9JnobCTtf9g.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">6 years ago</span>
<span class="video-view">2</span>
<span class="video-time">40:57</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Best Trick</div>
</a>
</div>
<div class="video-item video-188576299_456239333" data-video="WyLQl9Cw0YHRgtC10L3Rh9C40LLQvi3QoNCw0YHQv9GD0YLQvdCw0Y8g0LTQtdCy0YPRiNC60LAg0KHRg9C60YPQvNC4IFRzdW5kZXJlIElucmFuIFNob3VqbyBTdWt1bWkgLSAwMiBbNzIwcF0iLDExNTEsNTY2LCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NzIxNlwvdjg1NzIxNjE1NFwvMWE5Yzc5XC9pemdpTm9pTmZLby5qcGciLCIxMTg1ZjdjMWFlNDQyZjc5OGJjM2RlMGUzODQ5NjM1OCJd" data-embedUrl="https://daxab.com/player/_xrocTvqI9kHT3x3U7wzowtXwVlezUDoj5Hq5G3hKrwDyR1Is6uI55BD6hNsRuyoj6Tem8Xe_CHvUTxefHRPRsZIb-JQ7ieHn0TRsHIDhcQ" data-playerHash="_xrocTvqI9kHT3x3U7wzowtXwVlezUDoj5Hq5G3hKrwDyR1Is6uI55BD6hNsRuyoj6Tem8Xe_CHvUTxefHRPRsZIb-JQ7ieHn0TRsHIDhcQ" data-favHash="1059f8f2c8602da44c308a4c7842b98c">
<a href="/watch/-188576299_456239333" data-id="-188576299_456239333">
<div class="video-show"></div>
<div class="video-fav" data-hash="1059f8f2c8602da44c308a4c7842b98c"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c857216/v857216154/1a9c79/izgiNoiNfKo.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">566</span>
<span class="video-time">19:11</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Застенчиво-Распутная девушка Сукуми Tsundere Inran Shoujo Sukumi - 02 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239465" data-video="WyLQodC10LnRh9Cw0YEg0YEg0JzQsNC60LggTWFraS1jaGFuIHRvIE5vdyAtIDAyIFs3MjBwXSIsMTgwNiwxMjAyLCJodHRwczpcL1wvc3VuNi0yMi51c2VyYXBpLmNvbVwvYzg1NTYyMFwvdjg1NTYyMDM5NVwvMjQ5ZDg3XC9CcGI5Q2tBUWFBZy5qcGciLCIzNjYyZDFjMWE5NjQ5OTkzZGExMGI3NWYyOTc0MGRjNyJd" data-embedUrl="https://daxab.com/player/4_NgxMzGy4exGtMu_eJegjTDMjBgXgNnWllyPYop6pT2tlsvXJ2gE7QsA5xasyyefKTawXHuFi7PTYkpm3impLqpWIJvF8AQdM67DN3dvPY" data-playerHash="4_NgxMzGy4exGtMu_eJegjTDMjBgXgNnWllyPYop6pT2tlsvXJ2gE7QsA5xasyyefKTawXHuFi7PTYkpm3impLqpWIJvF8AQdM67DN3dvPY" data-favHash="ff491e850a72875ffb536f7e4fe9c880">
<a href="/watch/-188576299_456239465" data-id="-188576299_456239465">
<div class="video-show"></div>
<div class="video-fav" data-hash="ff491e850a72875ffb536f7e4fe9c880"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c855620/v855620395/249d87/Bpb9CkAQaAg.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1,2k</span>
<span class="video-time">30:06</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Сейчас с Маки Maki-chan to Now - 02 [720p]</div>
</a>
</div>
<div class="video-item video-188576299_456239332" data-video="WyLQl9Cw0YHRgtC10L3Rh9C40LLQvi3QoNCw0YHQv9GD0YLQvdCw0Y8g0LTQtdCy0YPRiNC60LAg0KHRg9C60YPQvNC4IFRzdW5kZXJlIElucmFuIFNob3VqbyBTdWt1bWkgLSAwMSBbNzIwcF0iLDEwODIsOTE4LCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvVVJDWFp3aHhjT285TVR2QWtzWjZpUTd6TXFZXzR5alZiWFByandcL3ZIQnB6RFRmLVNjLmpwZyIsIjY1MTEyODU3MmUzNTkzMGRhYjEyYmQ2MWRkNWNiMWMyIl0=" data-embedUrl="https://daxab.com/player/IETvncF8DoCSMbGzj3sRTzOt_75qcb3XjjwiHxc9t5St2n_72xtVMH0GQ23G4QYwVyjDw5VbCaHDMM1NLX4Wm8nTCGyj6MwlNc23ebIR9ps" data-playerHash="IETvncF8DoCSMbGzj3sRTzOt_75qcb3XjjwiHxc9t5St2n_72xtVMH0GQ23G4QYwVyjDw5VbCaHDMM1NLX4Wm8nTCGyj6MwlNc23ebIR9ps" data-favHash="a3947601bc8cd944b48dba7d4c2d585a">
<a href="/watch/-188576299_456239332" data-id="-188576299_456239332">
<div class="video-show"></div>
<div class="video-fav" data-hash="a3947601bc8cd944b48dba7d4c2d585a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/URCXZwhxcOo9MTvAksZ6iQ7zMqY_4yjVbXPrjw/vHBpzDTf-Sc.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">918</span>
<span class="video-time">18:02</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Застенчиво-Распутная девушка Сукуми Tsundere Inran Shoujo Sukumi - 01 [720p]</div>
</a>
</div>
<div class="video-item video-901607_162904584" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIFN0ZXAgVXAiLDMyMzAsNzgsImh0dHBzOlwvXC9zdW45LTY3LnVzZXJhcGkuY29tXC9jNTI2NjE2XC91MzA2OTkzMTdcL3ZpZGVvXC9sXzI0NTEyZWQ0LmpwZyIsIjVhYjZhYjY2MzllMGFiYjI4Y2FkMTA1NzlkNGU0Y2Y4Il0=" data-embedUrl="https://daxab.com/player/4kngrqLh-K6l9NX5PEzEdAcKIZdL4eD_Y5-C5DmZT2r9kHdbiDbumRIjj5M6L9fYWQcCEBwRPGUXd04NU4Z5tQ" data-playerHash="4kngrqLh-K6l9NX5PEzEdAcKIZdL4eD_Y5-C5DmZT2r9kHdbiDbumRIjj5M6L9fYWQcCEBwRPGUXd04NU4Z5tQ" data-favHash="7c5221b338e8f47aef78cdfd04b38762">
<a href="/watch/-901607_162904584" data-id="-901607_162904584">
<div class="video-show"></div>
<div class="video-fav" data-hash="7c5221b338e8f47aef78cdfd04b38762"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-67.userapi.com/c526616/u30699317/video/l_24512ed4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">78</span>
<span class="video-time">53:50</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Step Up</div>
</a>
</div>
<div class="video-item video-150082602_456239164" data-video="WyLQn9GD0YLQtdGI0LXRgdGC0LLQuNC1INCa0LDRgdGB0LDQvdC00YDRiyAyLiDQmtC+0L3QtdGGINGB0LLQtdGC0LAgMjAxMi4gIyAxIC0g0J/QvtC40YHQutC4INC90LDRh9C40L3QsNGO0YLRgdGPIiwxODY3LDEsImh0dHBzOlwvXC9zdW42LTIwLnVzZXJhcGkuY29tXC9jODMwMzA4XC92ODMwMzA4MTI1XC8xN2E1OWRcL1JUV1Rtb2QxOEJ3LmpwZyIsImFkNDI4MTQyOGMzMmEzZTdkMjRmZTczNjhlOTc5YzExIl0=" data-embedUrl="https://daxab.com/player/5eB8I06oNTQ55zM0Y6ZkKSVqwoYhgdR7TNQrEtvgcrJFdyRnMLOGn8xQ00B23WVKfcxcoUOeECQkwnRxKjDliFgl-z4WTM6xr_Ef3lScLIM" data-playerHash="5eB8I06oNTQ55zM0Y6ZkKSVqwoYhgdR7TNQrEtvgcrJFdyRnMLOGn8xQ00B23WVKfcxcoUOeECQkwnRxKjDliFgl-z4WTM6xr_Ef3lScLIM" data-favHash="f3e0f335d61cf72c6f3c24bb83dcaaaf">
<a href="/watch/-150082602_456239164" data-id="-150082602_456239164">
<div class="video-show"></div>
<div class="video-fav" data-hash="f3e0f335d61cf72c6f3c24bb83dcaaaf"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-20.userapi.com/c830308/v830308125/17a59d/RTWTmod18Bw.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">3 years ago</span>
<span class="video-view">1</span>
<span class="video-time">31:07</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Путешествие Кассандры 2. Конец света 2012. # 1 - Поиски начинаются</div>
</a>
</div>
<div class="video-item video-20774214_163637013" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTQgKExpdmUgU2hvdyAyKSIsNjA1OCwxMzcsImh0dHBzOlwvXC9zdW45LTgwLnVzZXJhcGkuY29tXC9jNTI3MzE3XC91OTcyODAxMVwvdmlkZW9cL2xfMjZlOWZhMmEuanBnIiwiNzM1NDA1ZjA5YjcwMTcyMDI3M2U3MDRiNDU5NjJhZTAiXQ==" data-embedUrl="https://daxab.com/player/Gi8uJe0IWHd7KuFQKXv3ePzd5HC22buiyGJ68IxIamRbZ9RzBHKpgglYjuZTm1YZDmkD9MU8zjAxyFX5kYGPIw" data-playerHash="Gi8uJe0IWHd7KuFQKXv3ePzd5HC22buiyGJ68IxIamRbZ9RzBHKpgglYjuZTm1YZDmkD9MU8zjAxyFX5kYGPIw" data-favHash="d462fb2019aed8ca25f27225aac6c177">
<a href="/watch/-20774214_163637013" data-id="-20774214_163637013">
<div class="video-show"></div>
<div class="video-fav" data-hash="d462fb2019aed8ca25f27225aac6c177"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c527317/u9728011/video/l_26e9fa2a.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">137</span>
<span class="video-time">01:40:58</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x14 (Live Show 2)</div>
</a>
</div>
<div class="video-item video-19767527_165512903" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gRm91cnRoIGRheS4gQk1YIEJpZyBBaXIiLDI5NTUsNDcsImh0dHBzOlwvXC9zdW45LTEwLnVzZXJhcGkuY29tXC9jNTA2MTE4XC91ODE5ODkyMDhcL3ZpZGVvXC9sX2VkZmYxMGU4LmpwZyIsIjQ1ZWExY2M0YWQ1MDNkNTQ4ZDI1ZWY3ODk4MTVkODJkIl0=" data-embedUrl="https://daxab.com/player/VNW_zsDt4g9iwPBpePWpVIiT8oxQdLx3Gi_N3AILSHrxLZWm-s0ZCWYke8oW6ymUqst-Te21Fur2BkwSBXysAA" data-playerHash="VNW_zsDt4g9iwPBpePWpVIiT8oxQdLx3Gi_N3AILSHrxLZWm-s0ZCWYke8oW6ymUqst-Te21Fur2BkwSBXysAA" data-favHash="dde051d566857522cb8860d118466d73">
<a href="/watch/-19767527_165512903" data-id="-19767527_165512903">
 <div class="video-show"></div>
<div class="video-fav" data-hash="dde051d566857522cb8860d118466d73"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-10.userapi.com/c506118/u81989208/video/l_edff10e8.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">47</span>
<span class="video-time">49:15</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Fourth day. BMX Big Air</div>
</a>
</div>
<div class="video-item video-53908459_456239661" data-video="WyJYLdC40LPRgNCwIDIgXC8gWCBHYW1lIDIiLDYxOTUsODEsImh0dHBzOlwvXC9zdW42LTIyLnVzZXJhcGkuY29tXC9jNjM2MjE4XC92NjM2MjE4NDU5XC80ODY0M1wvazFoWEpFWk1SNzQuanBnIiwiYjYzN2IzNzFmNjIxNTUxMThlMzYxZWI2NTE1OGNmMTMiXQ==" data-embedUrl="https://daxab.com/player/aFo4TL0aHEIxnkXt5ZAptk8hze46askVtz_BV4NXKybMu-sxdBI2UkDrNaOYzdsIkWx8KRn8cMqkU3COGLA8Qg" data-playerHash="aFo4TL0aHEIxnkXt5ZAptk8hze46askVtz_BV4NXKybMu-sxdBI2UkDrNaOYzdsIkWx8KRn8cMqkU3COGLA8Qg" data-favHash="fcc330c464b617c167c9bfb94242a787">
<a href="/watch/-53908459_456239661" data-id="-53908459_456239661">
<div class="video-show"></div>
<div class="video-fav" data-hash="fcc330c464b617c167c9bfb94242a787"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/c636218/v636218459/48643/k1hXJEZMR74.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">5 years ago</span>
<span class="video-view">81</span>
<span class="video-time">01:43:15</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-игра 2 / X Game 2</div>
</a>
</div>
<div class="video-item video-901607_162940653" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIE1vdG8gRnJlZXN0eWxlIiwzNzg1LDcwLCJodHRwczpcL1wvc3VuOS00NS51c2VyYXBpLmNvbVwvYzUxODYxNlwvdTMwNjk5MzE3XC92aWRlb1wvbF82MmE2MTVmOS5qcGciLCI4NWYzNjI0MjMxYjJkOTVkZTE1OGQ4MjM5Yzg5OWUxYiJd" data-embedUrl="https://daxab.com/player/cXdSoxzvkVSwtDlDYgPKr8JY9lfTUHoqno33nACd8IxPJKNIqFgQ44WNUSPDHHp1pqcZpuWhEytM84NID2VN1Q" data-playerHash="cXdSoxzvkVSwtDlDYgPKr8JY9lfTUHoqno33nACd8IxPJKNIqFgQ44WNUSPDHHp1pqcZpuWhEytM84NID2VN1Q" data-favHash="fccdce282eb9ecdf8bd54f3045922e3c">
<a href="/watch/-901607_162940653" data-id="-901607_162940653">
<div class="video-show"></div>
<div class="video-fav" data-hash="fccdce282eb9ecdf8bd54f3045922e3c"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-45.userapi.com/c518616/u30699317/video/l_62a615f9.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">70</span>
<span class="video-time">01:03:05</span>
</div>
</div>
 <div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Moto Freestyle</div>
</a>
</div>
<div class="video-item video-20774214_163592952" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxXC8gUGFydCAxKSBIRCIsMzM3OSw3MywiaHR0cHM6XC9cL3N1bjktNTQudXNlcmFwaS5jb21cL2M1MjkyMDJcL3UzMDg0OTU1XC92aWRlb1wvbF85Mjc0NDRkMC5qcGciLCJjM2ZmZGZhY2RhNDU5OGJiOTc1MTYwNzc3YTU4ZWRmMSJd" data-embedUrl="https://daxab.com/player/qjKfyoXa931Zrs7Qv7rXOGrLk_aL5l-irizYxpXTcyJTIdHsFGTGYH3TB1HJc1nkCmGUDTLXISFNHgkDJN8ZCg" data-playerHash="qjKfyoXa931Zrs7Qv7rXOGrLk_aL5l-irizYxpXTcyJTIdHsFGTGYH3TB1HJc1nkCmGUDTLXISFNHgkDJN8ZCg" data-favHash="3576c9c62c9f70156112ddb8ea7f498e">
<a href="/watch/-20774214_163592952" data-id="-20774214_163592952">
<div class="video-show"></div>
<div class="video-fav" data-hash="3576c9c62c9f70156112ddb8ea7f498e"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-54.userapi.com/c529202/u3084955/video/l_927444d0.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">73</span>
<span class="video-time">56:19</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1/ Part 1) HD</div>
</a>
</div>
<div class="video-item video-190464408_456239270" data-video="WyJTbGVlcGluZyBEb2dzIOKeqCDQn9GA0L7RhdC+0LbQtNC10L3QuNC1IDEyIOKeqCDQntC00L7Qu9C20LXQvdC40Y8g4oSWIDIgI1NsZWVwaW5nRG9ncyAjU3RhcnBlcml5YSIsMjU3NiwxLCJodHRwczpcL1wvc3VuNi0yMy51c2VyYXBpLmNvbVwvNzBJZ0FONW1tclFjajFjR0FEZ2RaZkNPSkhVejJPeHFDdVJmUWdcL2w4Y0JfTXpwcVJFLmpwZyIsIjg5M2Y3YmJlMWQ3MjBmNDdjNzRmODg0NGQ0ZjI1NDhlIl0=" data-embedUrl="https://daxab.com/player/_VkU8ct0ZHBZVIPSjUQmLS7piek5zigx2gDbu1yAj0CKd3dm4wBc9doH4e-sFbPEVObil5GzxOYmEt0rYbB0w5m0McPGBn3a3WISiUsVaQc" data-playerHash="_VkU8ct0ZHBZVIPSjUQmLS7piek5zigx2gDbu1yAj0CKd3dm4wBc9doH4e-sFbPEVObil5GzxOYmEt0rYbB0w5m0McPGBn3a3WISiUsVaQc" data-favHash="dfaa105378c66240f1bf25aa8494588d">
<a href="/watch/-190464408_456239270" data-id="-190464408_456239270">
<div class="video-show"></div>
<div class="video-fav" data-hash="dfaa105378c66240f1bf25aa8494588d"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/70IgAN5mmrQcj1cGADgdZfCOJHUz2OxqCuRfQg/l8cB_MzpqRE.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">1</span>
<span class="video-time">42:56</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Sleeping Dogs ➨ Прохождение 12 ➨ Одолжения № 2 #SleepingDogs #Starperiya</div>
</a>
</div>
<div class="video-item video384329_164980719" data-video="WyJYLWdhbWVzLCBBc2lhLCBWZXJ0LCAyMDEyIHllYXIiLDI4MzQsMTUsImh0dHBzOlwvXC9zdW45LTIyLnVzZXJhcGkuY29tXC9jNTE4MjEzXC91Mzg0MzI5XC92aWRlb1wvbF82MjEyODI1ZC5qcGciLCJlNzMzZGY4NTdhMGE3ZTczN2FmN2Y1ZDAwMDA4MGI2MCJd" data-embedUrl="https://daxab.com/player/c4mOsg2rPBBS6OSUt_vX89exWhwkH3cSkyptgdKLICQ2_A1JIFhAhavjx_oEfSCt7Ap8M0NDRWsVvHzCWvZe8A" data-playerHash="c4mOsg2rPBBS6OSUt_vX89exWhwkH3cSkyptgdKLICQ2_A1JIFhAhavjx_oEfSCt7Ap8M0NDRWsVvHzCWvZe8A" data-favHash="e5a881faca6da5f106ae9f0095b97d3a">
<a href="/watch/384329_164980719" data-id="384329_164980719">
<div class="video-show"></div>
<div class="video-fav" data-hash="e5a881faca6da5f106ae9f0095b97d3a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-22.userapi.com/c518213/u384329/video/l_6212825d.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">15</span>
<span class="video-time">47:14</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games, Asia, Vert, 2012 year</div>
</a>
</div>
<div class="video-item video-12783167_170084732" data-video="WyJYLUdhbWVzIDIwMTIgUmFsbHktQ3Jvc3MiLDU3MDAsMzYsImh0dHBzOlwvXC9zdW45LTM1LnVzZXJhcGkuY29tXC9jNTEzMzEyXC91MTQzOTcyOTQ5XC92aWRlb1wvbF8xMmM1MzFmNi5qcGciLCI5MDQ1MDMyMmY4MjA1OWQzMmNhNjFmNWFkMDUzNzMyYiJd" data-embedUrl="https://daxab.com/player/-b4kPqC_HOo2DJtsG1luhAPLsNVjCB6oU4YgW6VviZYji7DH6oiHv6ldRpFuCeU-0IO_GJCVXYuYn3WUNCFVXw" data-playerHash="-b4kPqC_HOo2DJtsG1luhAPLsNVjCB6oU4YgW6VviZYji7DH6oiHv6ldRpFuCeU-0IO_GJCVXYuYn3WUNCFVXw" data-favHash="a6401941b889bf3e78bc1df3df07fa72">
<a href="/watch/-12783167_170084732" data-id="-12783167_170084732">
<div class="video-show"></div>
<div class="video-fav" data-hash="a6401941b889bf3e78bc1df3df07fa72"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-35.userapi.com/c513312/u143972949/video/l_12c531f6.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 years ago</span>
<span class="video-view">36</span>
<span class="video-time">01:35:00</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games 2012 Rally-Cross</div>
</a>
</div>
<div class="video-item video-19767527_165206109" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gRmlyc3QiLDMxNDYsMjgsImh0dHBzOlwvXC9zdW45LTgyLnVzZXJhcGkuY29tXC9jNTMyMzA0XC91ODE5ODkyMDhcL3ZpZGVvXC9sXzYwNWY0ZmQ0LmpwZyIsIjdkZmU1MDE3MjJhNzMzZGFiZGZmZjMyM2M2Njk3NDUwIl0=" data-embedUrl="https://daxab.com/player/msbm-nnZ8xwcd-IyCt-AbdkL-07sOGHCs4UxmX6Nn7-3m3wF90QYd7k48XQj4tzjXzezTe8sreavy6xaqvecDA" data-playerHash="msbm-nnZ8xwcd-IyCt-AbdkL-07sOGHCs4UxmX6Nn7-3m3wF90QYd7k48XQj4tzjXzezTe8sreavy6xaqvecDA" data-favHash="cb12f1c853d023943344fa576e1d169a">
<a href="/watch/-19767527_165206109" data-id="-19767527_165206109">
<div class="video-show"></div>
<div class="video-fav" data-hash="cb12f1c853d023943344fa576e1d169a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-82.userapi.com/c532304/u81989208/video/l_605f4fd4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
 <span class="video-date">9 years ago</span>
<span class="video-view">28</span>
<span class="video-time">52:26</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. First</div>
</a>
</div>
<div class="video-item video-19767527_165214922" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gVGhyaWQgZGF5LiBCTVggU3RyZWV0IEZpbmFsIiwxMjcwLDExLCJodHRwczpcL1wvc3VuOS00Ni51c2VyYXBpLmNvbVwvYzUzNTUxMlwvdTgxOTg5MjA4XC92aWRlb1wvbF82NmRkZGZmYy5qcGciLCI5MGU1YjNiZGU4MjNhMjhkNTFlMzRiOGU3NmU2MzYzNiJd" data-embedUrl="https://daxab.com/player/DXZkOMTwPpT4j8mx-zQMsl9FMxSnRtXc0aBq9CF5yFeZHGR0i_LkLVGQwRsksuQ19KDovRg56qY3l5HuVnmicw" data-playerHash="DXZkOMTwPpT4j8mx-zQMsl9FMxSnRtXc0aBq9CF5yFeZHGR0i_LkLVGQwRsksuQ19KDovRg56qY3l5HuVnmicw" data-favHash="a89d88f3a3ec1b0d933a35869c8a565a">
<a href="/watch/-19767527_165214922" data-id="-19767527_165214922">
<div class="video-show"></div>
<div class="video-fav" data-hash="a89d88f3a3ec1b0d933a35869c8a565a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-46.userapi.com/c535512/u81989208/video/l_66dddffc.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">11</span>
<span class="video-time">21:10</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Thrid day. BMX Street Final</div>
</a>
</div>
<div class="video-item video-901607_162943115" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIFJldmlldyIsNTM4MSwxNiwiaHR0cHM6XC9cL3N1bjktODcudXNlcmFwaS5jb21cL2M1MjcyMTZcL3UzMDY5OTMxN1wvdmlkZW9cL2xfZDhiOTA2ZjQuanBnIiwiM2MyY2QzNDFhMTM4ODlkNjcxNjJmNmFlMTU5ZDBlMTIiXQ==" data-embedUrl="https://daxab.com/player/nUKqmDQK6fXo41dyeuw80POggLtKdtaRGCspEvSf2jvcxJPlma3FGcFp-prY0HDNMZGUzcmFDtk-N2nAxLWCmw" data-playerHash="nUKqmDQK6fXo41dyeuw80POggLtKdtaRGCspEvSf2jvcxJPlma3FGcFp-prY0HDNMZGUzcmFDtk-N2nAxLWCmw" data-favHash="f9d2d9c63926cc6aedd852f38b190488">
<a href="/watch/-901607_162943115" data-id="-901607_162943115">
<div class="video-show"></div>
<div class="video-fav" data-hash="f9d2d9c63926cc6aedd852f38b190488"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-87.userapi.com/c527216/u30699317/video/l_d8b906f4.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">16</span>
<span class="video-time">01:29:41</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Review</div>
</a>
</div>
<div class="video-item video-19767527_165214946" data-video="WyJYIEdhbWUgTG9zIEFuZ2VsZXMgMjAxMi4gVGhyaWQgZGF5LiBCTVggVmVydCIsOTkzLDg0LCJodHRwczpcL1wvc3VuOS03OS51c2VyYXBpLmNvbVwvYzUxNDUwNFwvdTgxOTg5MjA4XC92aWRlb1wvbF9jZDk1NjI3Ny5qcGciLCIwZjVhMDQ4NzkwYTM2NWYxYzNlNzUyNWI3NGIxYjQ1ZCJd" data-embedUrl="https://daxab.com/player/2Hnw7Q8vNKUsO_tvn3c1R14trnFAYDaW1re16ULXpDOIIk3_inEjscys9zIGbzwTuRrPaC-Mw2lqPTB7gXn6Jg" data-playerHash="2Hnw7Q8vNKUsO_tvn3c1R14trnFAYDaW1re16ULXpDOIIk3_inEjscys9zIGbzwTuRrPaC-Mw2lqPTB7gXn6Jg" data-favHash="8af51ac89ba5920efcbea5001540648a">
<a href="/watch/-19767527_165214946" data-id="-19767527_165214946">
<div class="video-show"></div>
<div class="video-fav" data-hash="8af51ac89ba5920efcbea5001540648a"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-79.userapi.com/c514504/u81989208/video/l_cd956277.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">84</span>
<span class="video-time">16:33</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X Game Los Angeles 2012. Thrid day. BMX Vert</div>
</a>
</div>
<div class="video-item video581608605_456239155" data-video="WyJSb2NrIHRoZSBWb3RlISAtIC0gUHJvcGFnYW5kYVdhdGNoICggMTQ0IFggMTQ0ICkubXA0IiwxMzE1LDE4NCwiaHR0cHM6XC9cL3N1bjYtMjMudXNlcmFwaS5jb21cLzFwdVZmMjRaVlMyd0VvNldVb3lJdV9EM2hQY1I1S1BoMWd0Uk93XC9WY2txVzB6WEVlUS5qcGciLCI3OGJlNzFlOTdmZWU0ODgwZmUxNzE5NWM1YTkxMGRjYyJd" data-embedUrl="https://daxab.com/player/3gPBxoXuvqciFxZWa5_qI_xllbMf-apJRvcw1eBRLibuSTBww-Rl9vq3Jzl6B0AlCZ0C_kBLSowU3H4JqvThdw" data-playerHash="3gPBxoXuvqciFxZWa5_qI_xllbMf-apJRvcw1eBRLibuSTBww-Rl9vq3Jzl6B0AlCZ0C_kBLSowU3H4JqvThdw" data-favHash="77b9ed060e5f68bd61219753639c34cf">
<a href="/watch/581608605_456239155" data-id="581608605_456239155">
<div class="video-show"></div>
<div class="video-fav" data-hash="77b9ed060e5f68bd61219753639c34cf"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/1puVf24ZVS2wEo6WUoyIu_D3hPcR5KPh1gtROw/VckqW0zXEeQ.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">2 years ago</span>
<span class="video-view">184</span>
<span class="video-time">21:55</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Rock the Vote! - - PropagandaWatch ( 144 X 144 ).mp4</div>
</a>
</div>
<div class="video-item video-20774214_163592955" data-video="WyJUaGUgWCBGYWN0b3IgMjAxMiAtIDl4MTIgKExpdmUgU2hvdyAxXC8gUGFydCAyKSBIRCIsMzMxMyw0MCwiaHR0cHM6XC9cL3N1bjktNjEudXNlcmFwaS5jb21cL2M1MTg2MDJcL3UzMDg0OTU1XC92aWRlb1wvbF83MmE0MTdiMi5qcGciLCI2MTM1NjNhMzdkZTY3NThjY2FiYzk2Njc5NzljZDg0OSJd" data-embedUrl="https://daxab.com/player/4DaDOegcZbN70nyI1fxNaMtgiHChFGZFRItVBNYnLXwJDSkLf3issehzlZ3elcfUnPeoB2zrRWZQmh2vQxEtzw" data-playerHash="4DaDOegcZbN70nyI1fxNaMtgiHChFGZFRItVBNYnLXwJDSkLf3issehzlZ3elcfUnPeoB2zrRWZQmh2vQxEtzw" data-favHash="d8daed6fac8e218db9f4e78de3e73b29">
<a href="/watch/-20774214_163592955" data-id="-20774214_163592955">
<div class="video-show"></div>
<div class="video-fav" data-hash="d8daed6fac8e218db9f4e78de3e73b29"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-61.userapi.com/c518602/u3084955/video/l_72a417b2.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">9 years ago</span>
<span class="video-view">40</span>
<span class="video-time">55:13</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">The X Factor 2012 - 9x12 (Live Show 1/ Part 2) HD</div>
</a>
</div>
<div class="video-item video-4296410_167364578" data-video="WyJYLWdhbWVzIDIwMTIgYm14IHZlcnQgZmluYWwgKCDQvdCwINGA0YPRgdGB0LrQvtC8KSIsOTkzLDYyLCJodHRwczpcL1wvc3VuOS0zMy51c2VyYXBpLmNvbVwvYzUyMzYwMFwvdTk3MDA0ODdcL3ZpZGVvXC9sX2NkYmZhMWUyLmpwZyIsIjE5NmVhYWE3NzE2MDhiYmJkYzc4ZjE5YTg3MDk5NDVjIl0=" data-embedUrl="https://daxab.com/player/lInWgovnw6tJLld0CDvDKThptl-anz6eiWy29-Y-KzfQYvtDt9iAUdw_f06izTX-tZSevV02_Lm6_X8Det7PwA" data-playerHash="lInWgovnw6tJLld0CDvDKThptl-anz6eiWy29-Y-KzfQYvtDt9iAUdw_f06izTX-tZSevV02_Lm6_X8Det7PwA" data-favHash="ed545ebb89984148892cfa8ee7c45ef8">
<a href="/watch/-4296410_167364578" data-id="-4296410_167364578">
<div class="video-show"></div>
<div class="video-fav" data-hash="ed545ebb89984148892cfa8ee7c45ef8"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-33.userapi.com/c523600/u9700487/video/l_cdbfa1e2.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">8 years ago</span>
<span class="video-view">62</span>
<span class="video-time">16:33</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012 bmx vert final ( на русском)</div>
</a>
</div>
<div class="video-item video-145081708_456242402" data-video="WyLQuNC1IEJvcmRlcmxhbmRzIEdPVFkgWzIwMDldIFBDXC9QUzNcL1hib3gzNjAuINCf0YDQvtGF0L7QttC00LXQvdC40LUg0LfQsCDQm9C40LvQuNGCINGHLjggKzE4IFtkYXIiLDI2NDcsMTE4LCJodHRwczpcL1wvaS5teWNkbi5tZVwvZ2V0VmlkZW9QcmV2aWV3P2lkPTEzMjIzNzY4MjU1NjMmaWR4PTEmdHlwZT0zOSZ0a249U3VPTUVURDh2R1dRMTNIaHdhdzI0WmhoX0xRJmZuPXZpZF9sIiwiZmRkNjMwM2Y3YTczNzUxYjlkZGQzZGFiYjE5NGEwNzYiXQ==" data-embedUrl="https://daxab.com/player/YK3dzGl8e0x-Z-na65bdKre5O4tx23-6c7nLSzRlh_ZznPzwIa6EARINkVdtmWIpqZqTMJWKELZ9fZhmjql1_7xqFbQpqTaIA3agHLWNJ6Q" data-playerHash="YK3dzGl8e0x-Z-na65bdKre5O4tx23-6c7nLSzRlh_ZznPzwIa6EARINkVdtmWIpqZqTMJWKELZ9fZhmjql1_7xqFbQpqTaIA3agHLWNJ6Q" data-favHash="697a4941bacc445326c3e7e84ee48753">
<a href="/watch/-145081708_456242402" data-id="-145081708_456242402">
<div class="video-show"></div>
<div class="video-fav" data-hash="697a4941bacc445326c3e7e84ee48753"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://i.mycdn.me/getVideoPreview?id=1322376825563&idx=1&type=39&tkn=SuOMETD8vGWQ13Hhwaw24Zhh_LQ&fn=vid_l"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">1 year ago</span>
<span class="video-view">118</span>
<span class="video-time">44:07</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">ие Borderlands GOTY [2009] PC/PS3/Xbox360. Прохождение за Лилит ч.8 +18 [dar</div>
</a>
</div>
<div class="video-item video104849236_456277453" data-video="WyJNQVggUEFZTkUgMyBOUEMgV2FycyAyMyAoVUZFIHZzIENyYWNoYSBQcmV0bykiLDE4ODMsMSwiaHR0cHM6XC9cL3N1bjYtMjIudXNlcmFwaS5jb21cL1d4d08yUXhIVUk4Qm5qaks1R3U2TGQxMGlLVWZDYkFpQTVMRjlnXC9zTGdBSEQ4U0lwOC5qcGciLCI5ZTJkMjU5Mzk5YmZhYzBhNzU1YWVmOGMxNDE1ZjhhZSJd" data-embedUrl="https://daxab.com/player/pxZm3-54LzW2kdpx0O2y5wvfqRXODe2W9ECzQQJr6bbzDUguoSXn-Q4yoVExeam98AtNx-q8LyTAUdGQPh_l3A" data-playerHash="pxZm3-54LzW2kdpx0O2y5wvfqRXODe2W9ECzQQJr6bbzDUguoSXn-Q4yoVExeam98AtNx-q8LyTAUdGQPh_l3A" data-favHash="daff3e6ec27aa95eabcde1e74d2bdced">
<a href="/watch/104849236_456277453" data-id="104849236_456277453">
<div class="video-show"></div>
<div class="video-fav" data-hash="daff3e6ec27aa95eabcde1e74d2bdced"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-22.userapi.com/WxwO2QxHUI8BnjjK5Gu6Ld10iKUfCbAiA5LF9g/sLgAHD8SIp8.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">7 months ago</span>
<span class="video-view">1</span>
<span class="video-time">31:23</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">MAX PAYNE 3 NPC Wars 23 (UFE vs Cracha Preto)</div>
</a>
</div>
<div class="video-item video-901607_162902722" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuINCe0LHQt9C+0YAg0L/QtdGA0LLQvtCz0L4g0LTQvdGPIiwyNjkyLDE0LCJodHRwczpcL1wvc3VuOS0zMy51c2VyYXBpLmNvbVwvYzUyNjUwN1wvdTMwNjk5MzE3XC92aWRlb1wvbF8xYWFlYTc5ZC5qcGciLCJjNjU2ODIyYzQ3YzY3MTEyM2U5Y2QxYzYxMWZhOTQzMyJd" data-embedUrl="https://daxab.com/player/byofn9dbZvM9yA4sWXfSkCqQNybA2_bgMfIhZzMgBI9-u-xskdFPRQWB_Q4VRhzSfN9250cGON1uo_MfuTMVtw" data-playerHash="byofn9dbZvM9yA4sWXfSkCqQNybA2_bgMfIhZzMgBI9-u-xskdFPRQWB_Q4VRhzSfN9250cGON1uo_MfuTMVtw" data-favHash="ad3b2031e76f2b45329c183030b3b5e2">
<a href="/watch/-901607_162902722" data-id="-901607_162902722">
<div class="video-show"></div>
<div class="video-fav" data-hash="ad3b2031e76f2b45329c183030b3b5e2"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-33.userapi.com/c526507/u30699317/video/l_1aaea79d.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">14</span>
<span class="video-time">44:52</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Обзор первого дня</div>
</a>
</div>
<div class="video-item video593543_162951285" data-video="WyJYLUdhbWVzIExvcyBBbmdlbGVzIDIwMTIuIEJlc3QgVHJpY2siLDI0NTcsMjUsImh0dHBzOlwvXC9zdW45LTY5LnVzZXJhcGkuY29tXC9jNTI3MTA3XC91MzA2OTkzMTdcL3ZpZGVvXC9sXzk5ZGM1MjljLmpwZyIsIjBmODBkOWU2YWYzNTQ2NDY0YTdmZTE3OTRkYjk1YjVmIl0=" data-embedUrl="https://daxab.com/player/3m77zpb0QWJ6b4O0AgEFIeBdcVQV4-TMC-oAGJuTfQ31opQ_L6CUkTXVy9XAlwbY_jH6XcbXzSGFFLsGwQP5lA" data-playerHash="3m77zpb0QWJ6b4O0AgEFIeBdcVQV4-TMC-oAGJuTfQ31opQ_L6CUkTXVy9XAlwbY_jH6XcbXzSGFFLsGwQP5lA" data-favHash="d6ab0848c96f33c96ad3032c4a0d0637">
<a href="/watch/593543_162951285" data-id="593543_162951285">
<div class="video-show"></div>
<div class="video-fav" data-hash="d6ab0848c96f33c96ad3032c4a0d0637"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-69.userapi.com/c527107/u30699317/video/l_99dc529c.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">25</span>
<span class="video-time">40:57</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-Games Los Angeles 2012. Best Trick</div>
</a>
</div>
<div class="video-item video-197946026_456239614" data-video="WyLQn9GA0L7RhdC+0LbQtNC10L3QuNC1IFNsZWVwaW5nIERvZ3MgKDIwMTMpICMyIC0g0JrQu9GD0LEgXCLQkdCw0Lwt0JHQsNC8XCIiLDczMDgsNDMsImh0dHBzOlwvXC9zdW42LTIzLnVzZXJhcGkuY29tXC9xMGxBdlpKd21wZE9samtzd3BZYW5MeFpKUl9aTDF6YjB1RXdfZ1wvYllXZE50M05xSDAuanBnIiwiYmU2NjkwN2I3ZGJjNzY1YjVkNzE5MjU5ZWQ0NTZmMDEiXQ==" data-embedUrl="https://daxab.com/player/4amxwrbkool321ixQkyeCZVC1eexjKuL1wEZQ3zcd9h7o5EXDWaGVPMLJL1wrqWnKnG_R50xfz9Q2DurgG419J6rijz30IcekIqdbnJipwM" data-playerHash="4amxwrbkool321ixQkyeCZVC1eexjKuL1wEZQ3zcd9h7o5EXDWaGVPMLJL1wrqWnKnG_R50xfz9Q2DurgG419J6rijz30IcekIqdbnJipwM" data-favHash="004476a96ec3d868483e77418cf244f2">
<a href="/watch/-197946026_456239614" data-id="-197946026_456239614">
<div class="video-show"></div>
<div class="video-fav" data-hash="004476a96ec3d868483e77418cf244f2"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun6-23.userapi.com/q0lAvZJwmpdOljkswpYanLxZJR_ZL1zb0uEw_g/bYWdNt3NqH0.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">4 months ago</span>
<span class="video-view">43</span>
<span class="video-time">02:01:48</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">Прохождение Sleeping Dogs (2013) #2 - Клуб &quot;Бам-Бам&quot;</div>
</a>
</div>
<div class="video-item video-27575945_163557091" data-video="WyJYLWdhbWVzIDIwMTIs0KTRgNC40YHRgtCw0LnQuyzQn9C+0LvQvdC+0YHRgtGM0Y4hIiwzNzg1LDEzLCJodHRwczpcL1wvc3VuOS04MC51c2VyYXBpLmNvbVwvYzUyNjIxNVwvdTU5NDM1OTcxXC92aWRlb1wvbF84YThkNGI0NS5qcGciLCI3OTMwYzNkY2Y5NzdkZDhiYWM1ZWQ1NDJiYTkwZGIyOCJd" data-embedUrl="https://daxab.com/player/TYHExj49tfrzT-o9xgsoeRsZSwVnSdV67eU-nHUEMfzPRD5osa-sbY2ynw6GM6_PHcZPm1u4-JfGwrCe93m4fA" data-playerHash="TYHExj49tfrzT-o9xgsoeRsZSwVnSdV67eU-nHUEMfzPRD5osa-sbY2ynw6GM6_PHcZPm1u4-JfGwrCe93m4fA" data-favHash="9795118eba65377777021ab858d2594b">
<a href="/watch/-27575945_163557091" data-id="-27575945_163557091">
<div class="video-show"></div>
<div class="video-fav" data-hash="9795118eba65377777021ab858d2594b"></div>
<div class="video-img">
<div class="video-thumb lazy" data-thumb="https://sun9-80.userapi.com/c526215/u59435971/video/l_8a8d4b45.jpg"></div>

<div class="video-play"></div>
<div oncontextmenu="return false" class="video-preview"></div>
<div class="video-info">
<span class="video-date">10 years ago</span>
<span class="video-view">13</span>
<span class="video-time">01:03:05</span>
</div>
</div>
<div class="video-title" onmouseover="setTitle(this);">X-games 2012,Фристайл,Полностью!</div>
</a>
</div>
<span class="standalonead" style="margin: 0px 13px;padding-bottom: 15px;">
<iframe width="300" height="250" scrolling="no" frameborder="0" src="https://a.adtng.com/get/10002729?time=1555363895445" allowtransparency="true" marginheight="0" marginwidth="0" name="spot_id_10002729"></iframe>
</span>
<div class="more-similar" onclick="event.cancelBubble = true; loadMoreSimilar(this);" data-page="1" data-video="-53425992_165397466" data-hash="84d987ceaf814a97d050f983234f2315d5157387">Show more</div>
</div>

<script>
$('.tabs > div.tab-btn').on('click', function() {
  if ($(this).hasClass('tab-show')) {
    return;
  }

  $(this).parent().children('div').removeClass('tab-show');
  $(this).addClass('tab-show');

  $('.tab-result').removeClass('tab-show');
  $('.tab-result.' + this.id).addClass('tab-show');

  // if ($('.width').width() < 1020 && this.id == 'tab_comments') {
  //   loadComments($('#video_hash').val().trim());
  // }
});
</script>
</div>
</div>
</div>
<div class="footer">
<div class="width">
<div class="now">
<b>You might be interested</b>
<span><a href="/video/Chloe Temple Seka">Chloe Temple Seka</a> <a href="/video/Bisexual Step">Bisexual Step</a> <a href="/video/Tyler Nixon">Tyler Nixon</a> <a href="/video/Syren De Mer">Syren De Mer</a> <a href="/video/London Keyes">London Keyes</a> <a href="/video/Emily Addison Brazzers">Emily Addison Brazzers</a> <a href="/video/Savannah Watson">Savannah Watson</a> <a href="/video/See Him Fuck">See Him Fuck</a> <a href="/video/Ellie">Ellie</a> <a href="/video/Brooklyn Chase">Brooklyn Chase</a> <a href="/video/Maddie May">Maddie May</a> <a href="/video/Sayuri Sakai">Sayuri Sakai</a> <a href="/video/Lasirena69">Lasirena69</a> <a href="/video/Sneaky">Sneaky</a> <a href="/video/Nicole Love">Nicole Love</a> <a href="/video/Anya">Anya</a> <a href="/video/Sharon White">Sharon White</a> <a href="/video/Ziggy Star">Ziggy Star</a> <a href="/video/Mickey Violet">Mickey Violet</a> <a href="/video/Natural">Natural</a></span>
</div>
<div class="clear"></div>
<div class="copy">
<a href="/">DaftSex</a>
<a href="/browse">Browse</a>
<a href="/hottest">Hottest</a>
<a href="/categories">Category</a>
<a href="/pornstars">Pornstars</a>
<a href="/holders">DMCA</a>
<a href="/privacy">Privacy</a>
<a href="/faq">FAQ</a>
<a href="/contact">Contact Us</a>
<a href="https://daft.sex/" target="_blank" title="Start search Page for DaftSex">DaftSex Search</a>
<a href="https://theporndude.com/" target="_blank" rel="nofollow" style="font-weight: bold;">Best Porn Sites</a>
<a href="https://twitter.com/DaftPost" target="_blank" class="twitter font-weight-bold" style="background: #01abf1 url(/img/twitter.png) no-repeat scroll center center;border-radius: 25px;color: transparent;background-size: 27px;"><i class="icon-twistter"></i>Twitter</a>
</div>
</div>
</div>
</div>
<div class="bg_layer"></div>

<script>
onPageLoaded();
$(window).load(onPageReady);
previewEvents();
</script>
<script type='text/javascript' src='//sufficientretiredbunker.com/a5/96/32/a59632dda777535e591fa2e7dde66a93.js'></script>
<script type="text/javascript" async src="/js/app.ve1c0c4626c025980.js"></script>
<script type="text/javascript">
!function(){var w,n=window.outerWidth-window.innerWidth,o=window.outerHeight-window.innerHeight,t=$(window).width(),e=$(window).height();function i(i){w&&clearTimeout(w),w=setTimeout(function(){var i=window.outerWidth-window.innerWidth,w=window.outerHeight-window.innerHeight;n!==i&&o!==w||n==i&&o==w||t==$(window).width()&&e==$(window).height()||t!==$(window).width()&&e!==$(window).height()||!winFullscreen&&(400<i&&n<i&&400<Math.abs(i-n)||200<w&&o<w&&200<Math.abs(w-o))&&(setCookie("_dt",1,{path:"/",expires:1800,domain:"."+location.hostname}),dt()),n=window.outerWidth-window.innerWidth,o=window.outerHeight-window.innerHeight,t=$(window).width(),e=$(window).height()},500)}browser.mobile||$(window).off("resize",i).on("resize",i)}();
</script>

<div style="display:none;">
<script type="text/javascript">
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t45.6;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";h"+escape(document.title.substring(0,80))+";"+Math.random()+
"' alt='' title='LiveInternet' "+
"border='0' width='31' height='31'><\/a>")
</script>
</div>


</body>
</html>
