#!/usr/bin/python3


import urllib3
import json
import os
from base64 import b64decode
import certifi
import re           # for regular expression
from pudb import set_trace

http = urllib3.PoolManager(cert_reqs='CERT_REQUIRED',ca_certs=certifi.where())

# Light Green Car 240/360/480/1080/720/
# GET VIDEO ID (-2000072782_95072782)
# 2012 X-Games 18 motocross best trick 720/480/360/240 28:50min
url = 'https://daftsex.com/watch/-53425992_165397466'

pattern_id = re.search(r'https?://(?:www\.)?daftsex\.com/watch/(?P<id1>-\d+)_(?P<id2>\d+)',url)
print("ID1: ",pattern_id.group(1))
print("ID2: ",pattern_id.group(2))

# GET VIDEO ID USING SLICE
id = url[26:46]

request = http.request('GET',url)

# FIRST REQUEST - GET PAGE SOURCE
content = request.headers["Content-Type"]

# GET WEBPAGE CONTENT
webpage = request.data.decode('utf-8')

# WRITE FIRST PAGE SCRAPING TO FILE
file = "scraping_DS1.md"
with open(file ,'a') as file_obj:
    file_obj.write(webpage)

# GET TITLE / DURATION / PLAYER_HASH / PLAYER_COLOR
title = re.findall(r'<title>(.*?)</title>',str(webpage))
print("Title: ", title[0])

duration = re.findall(r'Duration: ((?:[0-9]{2}:)[0-9]{2})',str(webpage))
print("Duration: " , duration[0],"\n")

player_hash = re.findall('DaxabPlayer\\.Init\\({[\\s\\S]*hash:\\s*"([0-9a-zA-Z_\\-]+)"[\\s\\S]*}',str(webpage))
print("Hash:", player_hash[0])

player_color = re.findall('DaxabPlayer\\.Init\\({[\\s\\S]*color:\\s*"([0-9a-z]+)"[\\s\\S]*}',str(webpage))
print("Color:" ,player_color[0])

## SECOND PART 
url2 = 'https://daxab.com/player/' + player_hash[0] +'?color=' + player_color[0]
print("URL2: ", url2)

request2 = http.request('GET',url2,headers={'Referer': url})
webpage2 = request2.data.decode('utf-8')

## WRITE SECOND PAGE SCRAPPING
file2 = "scraping_DS2.md"

if(os.path.exists(file2)):
    os.remove("scraping_DS2.md")

with open(file2,'a') as file2_obj:
    file2_obj.write(webpage2)

# GET VIDEO PARAMS - GET QUALITY AND VIDEO ID
video_params=re.findall(r'window\.globParams\s*=\s*({[\S\s]+})\s*;\s*<\/script>',str(webpage2))
print("\n---- [ VIDEO PARAMETERS] ----\n")

server_matches = re.search(r'server:\s"(?P<server>[^"]*)',str(video_params))
#print(type(server_matches.group('server')))

# GET SERVER INFORMATION(DECYPT SERVER URL)
server_name = b64decode(server_matches.groups('server')[0][::-1]).decode('utf-8')
server = 'https://%s' % server_name
print("SERVER_NAME:",server)

# GET TOKKEN INFORMATION
token = re.search(r'access_token:\s"(?P<token>[^"]*)',str(video_params))
print("TOKKEN: ", token.group('token'))

# GET CREDENTIALS
credentials = re.search(r'credentials:\s"(?P<credential>[^"]*)',str(video_params))
print("CREDENTIALS: ",credentials.group('credential'))

# GET C_KEY
c_key = re.search(r'c_key:\s"(?P<ckey>[^"]*)',str(video_params))
print("C_KEY: ", c_key.group('ckey'))

# GET EXTRA_KEY
extra_key = re.search(r'extra_key:\s"(?P<extra_key>[^"]*)',str(video_params))
print("EXTRA_KEY: ", extra_key.group('extra_key'))

height='720'
ext='mp4'

server_url = f'{server}/videos/{pattern_id.group(1)}/{pattern_id.group(2)}/{height}.{ext}'
print("URL:", server_url)

## PART 3 - GET JSON METADATA
print("---- PART 3 GET JSON INFOS ----\n")

url_for_json_parsing = f'{server}/method/video.get/{pattern_id.group(1)}_{pattern_id.group(2)}'
print("URL JSON: ",url_for_json_parsing)

request3 = http.request('GET',url_for_json_parsing,headers={'Referer': url},fields={
    'token': token.group('token'),
    'videos': id,
    'ckey': c_key.group('ckey'),
    'credentials': credentials.group('credential')})

webp3 = request3.data.decode('utf-8')

## WRITE THIRD PAGE SCRAPPING
file3 = "scraping_DS3.md"

if(os.path.exists(file3)):
    os.remove("scraping_DS3.md")

with open(file3,'a') as file3_obj:
    file3_obj.write(webp3)

# GET EXTENTION / QUALITY
ext = re.search(r'(?P<ext>mp4|avi|mkv)',str(webp3))
print("EXT: ", ext.groups())

quality = re.search(r'mp4_(?P<quality>\d+$)',str(webp3))
# FORCE QUALITY
quality='720p'
print("QUALITY: ",quality)

# GET URL FOR 720p
url_720 = re.search(r'mp4_720":"https:\/\/(?P<url>[^"]*)',str(webp3))
print("URL 720: ", url_720.group('url'))

# CHANGE CRAZYCLOUD TO DAXAB URL
daxab_url = re.sub(r'crazycloud.ru','daxab.com',str(server))
print("DAXAB URL:", daxab_url)

# FINAL URL
furl = f'{server}/{url_720.group("url")}&videos={id}&extra_key={extra_key.group("extra_key")}'
print("FINAL URL: ",furl)


